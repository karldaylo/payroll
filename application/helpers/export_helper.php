<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//Used to parse all html value and convert them into pdf platform
//commented out some lower part of script so to make it a stream object where the end-user can view it on browser instead of being downloaded
//commented out some of the upper part of script, this is for debugging incase the pagination is mismatched from the initially calculated target_page count.

function pdf_create($html, $filename='',$target_page = 1, $stream=TRUE) 
{
    require_once __dir__."/dompdf/dompdf_config.inc.php";

    $dompdf = new DOMPDF();
    $dompdf->set_paper('letter', 'portrait');
    // $dompdf->set_paper(array(0, 0, 612.00, 864.00), 'portrait');
    $dompdf->load_html($html);
    $dompdf->render();
    // if($dompdf->get_canvas()->get_page_count() == $target_page){
    $dompdf->stream($filename, array("Attachment" => false));       
    // }
    // else{
    //     return $dompdf->get_canvas()->get_page_count();
    // }

    //to download
    // if ($stream) {
    //     echo $dompdf->output();
    //     // $dompdf->stream($filename.".pdf");
    // } else {
    //     return $dompdf->output();
    // }
}
?>