<?php 
if ( ! function_exists('sss_258'))
{

    //########################### RATES ###############################
    function  rate_313 (){
        $semi_month                 = 7003.38;
        $daily                      = 537;
        $hourly                     = 67.125;
        $undertime_perhour          = 104.032;
        $night_diff_hour            = 6.7125;
        $vl_day_deduct              = 5.58;
        //by percentage
        $ot_hourly                  = 1.25 * $hourly;
        $special_holiday            = 1.30 * $hourly;
        $ot_special_holiday         = 1.69 * $hourly;
        $legal_holiday              = 1.00 * $hourly;
        $ot_legal_holiday           = 2.60 * $hourly;
        $restday_regular_holiday    = 2.60 * $hourly;
        $ot_restday_regular_holiday = 3.38 * $hourly;

        $array = array(
            "semi_month"                => $semi_month,
            "daily_wage"                => $daily,
            "hourly_rate"               => $hourly,
            "ut_rate"                   => $undertime_perhour,
            "ot_rate"                   => $ot_hourly,
            "nd_rate"                   => $night_diff_hour,
            "vl_rate"                   => $vl_day_deduct,
            "special_holiday"           => $special_holiday,
            "ot_special_holiday"        => $ot_special_holiday,
            "legal_holiday"             => $legal_holiday,
            "ot_legal_holiday"          => $ot_legal_holiday,
            "restday_regular_holiday"   => $restday_regular_holiday,
            "ot_restday_regular_holiday"=> $ot_restday_regular_holiday,
            "sss_deduction"             => sss_313(),
            "philhealth_deduction"      => philhealth_313(),
            "pagibig_deduction"         => pagibig_313(),
        );
        return $array;
    }

    function  rate_310 (){
        $semi_month                 = 6936.25;
        $daily                      = 537;
        $hourly                     = 67.125;
        $undertime_perhour          = 103.125;
        $night_diff_hour            = 6.7125;
        $vl_day_deduct              = 8.66;
        //by percentage
        $ot_hourly                  = 1.25 * $hourly;
        $special_holiday            = 1.30 * $hourly;
        $ot_special_holiday         = 1.69 * $hourly;
        $legal_holiday              = 1.00 * $hourly;
        $ot_legal_holiday           = 2.60 * $hourly;
        $restday_regular_holiday    = 2.60 * $hourly;
        $ot_restday_regular_holiday  = 3.38 * $hourly;

        $array = array(
            "semi_month"                => $semi_month,
            "daily_wage"                => $daily,
            "hourly_rate"               => $hourly,
            "ut_rate"                   => $undertime_perhour,
            "ot_rate"                   => $ot_hourly,
            "nd_rate"                   => $night_diff_hour,
            "vl_rate"                   => $vl_day_deduct,
            "special_holiday"           => $special_holiday,
            "ot_special_holiday"        => $ot_special_holiday,
            "legal_holiday"             => $legal_holiday,
            "ot_legal_holiday"          => $ot_legal_holiday,
            "restday_regular_holiday"   => $restday_regular_holiday,
            "ot_restday_regular_holiday" => $ot_restday_regular_holiday,
            "sss_deduction"             => sss_310(),
            "philhealth_deduction"      => philhealth_310(),
            "pagibig_deduction"         => pagibig_310(),
        );
        return $array;
    }

    function  rate_258 (){

        $semi_month                 = 5772.75;
        $daily                      = 537;
        $hourly                     = 67.125;
        $undertime_perhour          = 0;
        $night_diff_hour            = 6.7125;
        $vl_day_deduct              = 10.395;
        //by percentage
        $ot_hourly                  = 1.25 * $hourly;
        $special_holiday            = 1.30 * $hourly;
        $ot_special_holiday         = 1.69 * $hourly;
        $legal_holiday              = 1.00 * $hourly;
        $ot_legal_holiday           = 2.60 * $hourly;
        $restday_regular_holiday    = 2.60 * $hourly;
        $ot_restday_regular_holiday  = 3.38 * $hourly;

        $array = array(
            "semi_month"                => $semi_month,
            "daily_wage"                => $daily,
            "hourly_rate"               => $hourly,
            "ut_rate"                   => $undertime_perhour,
            "ot_rate"                   => $ot_hourly,
            "nd_rate"                   => $night_diff_hour,
            "vl_rate"                   => $vl_day_deduct,
            "special_holiday"           => $special_holiday,
            "ot_special_holiday"        => $ot_special_holiday,
            "legal_holiday"             => $legal_holiday,
            "ot_legal_holiday"          => $ot_legal_holiday,
            "restday_regular_holiday"   => $restday_regular_holiday,
            "ot_restday_regular_holiday" => $ot_restday_regular_holiday,
            "sss_deduction"             => sss_258(),
            "philhealth_deduction"      => philhealth_258(),
            "pagibig_deduction"         => pagibig_258(),
        );
        return $array;
    }


    //########################### DEDUCTIONS ###############################
    function sss_258($target = ""){
        $data = array (
            "0"     => 460,
            "0.5"   => 460,
            "1"     => 420,
            "1.5"   => 420,
            "2"     => 400,
            "2.5"   => 400,
            "3"     => 380,
            "3.5"   => 380,
            "4"     => 360,
            "4.5"   => 360,
            "5"     => 340,
            "5.5"   => 340,
            "6"     => 320,
            "6.5"   => 320,
            "7"     => 300,
            "7.5"   => 280,
            "8"     => 280,
            "8.5"   => 260,
            "9"     => 260,
            "9.5"   => 240,
            "10"    => 240,
            "10.5"  => 220,
            "11"    => 220,
            "11.5"  => 200,
            "12"    => 200,
            "12.5"  => 180,
            "13"    => 180,
            "13.5"  => 160,
            "14"    => 160,
            "14.5"  => 140,
            "15"    => 120,
            "15.5"  => 120,
            "16"    => 100,
            "16.5"  => 100,
        );

        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function sss_310($target = ""){
        $data = array(
            "0"       => 560,
            "0.5"       => 560,
            "1"       => 520,
            "1.5"     => 500,
            "2"       => 500,
            "2.5"     => 480,
            "3"       => 480,
            "3.5"     => 460,
            "4"       => 460,
            "4.5"     => 440,
            "5"       => 440,
            "5.5"     => 420,
            "6"       => 420,
            "6.5"     => 400,
            "7"       => 400,
            "7.5"     => 380,
            "8"       => 380,
            "8.5"     => 360,
            "9"       => 360,
            "9.5"     => 340,
            "10"      => 340,
            "10.5"    => 320,
            "11"      => 320,
            "11.5"    => 280,
            "12"      => 280,
            "12.5"    => 260,
            "13"      => 260,
            "13.5"    => 240,
            "14"      => 240,
            "14.5"    => 220,
            "15"      => 200,
            "15.5"    => 200,
            "16"      => 180,
            "16.5"    => 180,
        );  
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function sss_313($target = ""){
        $data = array(  
            "0"       => 560,
            "0.5"       => 560,
            "1"       => 520,
            "1.5"     => 500,
            "2"       => 500,
            "2.5"     => 480,
            "3"       => 480,
            "3.5"     => 460,
            "4"       => 460,
            "4.5"     => 440,
            "5"       => 440,
            "5.5"     => 420,
            "6"       => 420,
            "6.5"     => 400,
            "7"       => 400,
            "7.5"     => 380,
            "8"       => 380,
            "8.5"     => 360,
            "9"       => 360,
            "9.5"     => 340,
            "10"      => 340,
            "10.5"    => 320,
            "11"      => 320,
            "11.5"    => 280,
            "12"      => 280,
            "12.5"    => 260,
            "13"      => 260,
            "13.5"    => 240,
            "14"      => 240,
            "14.5"    => 220,
            "15"      => 200,
            "15.5"    => 200,
            "16"      => 180,
            "16.5"    => 180,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function pagibig_258($target = ""){
        $data = array(
            "0"       => 100,
            "0.5"       => 100,
            "1"       => 95.35,
            "1.5"     => 93.02,
            "2"       => 90.70,
            "2.5"     => 88.37,
            "3"       => 86.05,
            "3.5"     => 83.72,
            "4"       => 81.40,
            "4.5"     => 79.07,
            "5"       => 76.75,
            "5.5"     => 74.42,
            "6"       => 72.10,
            "6.5"     => 69.77,
            "7"       => 67.45,
            "7.5"     => 65.12,
            "8"       => 62.80,
            "8.5"     => 60.47,
            "9"       => 58.15,
            "9.5"     => 55.82,
            "10"      => 53.5,
            "10.5"    => 51.17,
            "11"      => 48.85,
            "11.5"    => 46.52,
            "12"      => 44.20,
            "12.5"    => 41.87,
            "13"      => 39.55,
            "13.5"    => 37.22,
            "14"      => 34.90,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function pagibig_310($target = ""){
        $data = array(
            "0"       => 100,
            "0.5"       => 100,
            "1"       => 96.13,
            "1.5"     => 94.19,
            "2"       => 92.26,
            "2.5"     => 90.32,
            "3"       => 88.39,
            "3.5"     => 86.45,
            "4"       => 84.52,
            "4.5"     => 82.58,
            "5"       => 80.65,
            "5.5"     => 78.71,
            "6"       => 76.78,
            "6.5"     => 74.84,
            "7"       => 72.91,
            "7.5"     => 70.97,
            "8"       => 69.04,
            "8.5"     => 67.10,
            "9"       => 65.17,
            "9.5"     => 63.23,
            "10"      => 61.30,
            "10.5"    => 59.36,
            "11"      => 57.43,
            "11.5"    => 55.49,
            "12"      => 53.56,
            "12.5"    => 51.62,
            "13"      => 49.69,
            "13.5"    => 47.75,
            "14"      => 45.82,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function pagibig_313($target = ""){
        $data= array(
            "0"       => 100,
            "0.5"       => 100,
            "1"       => 96.17,
            "1.5"     => 94.25,
            "2"       => 92.34,
            "2.5"     => 90.42,
            "3"       => 88.51,
            "3.5"     => 86.59,
            "4"       => 84.68,
            "4.5"     => 82.76,
            "5"       => 80.85,
            "5.5"     => 78.93,
            "6"       => 77.02,
            "6.5"     => 75.10,
            "7"       => 73.19,
            "7.5"     => 71.27,
            "8"       => 69.36,
            "8.5"     => 67.44,
            "9"       => 65.53,
            "9.5"     => 63.61,
            "10"      => 61.70,
            "10.5"    => 59.78,
            "11"      => 57.87,
            "11.5"    => 55.95,
            "12"      => 54.04,
            "12.5"    => 52.12,
            "13"      => 50.21,
            "13.5"    => 48.29,
            "14"      => 46.38,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function philhealth_258($target = ""){
        $data = array(
            "0"     => 173.18,
            "0.5"   => 169.15,
            "1"     => 165.13,
            "1.5"   => 161.10,
            "2"     => 157.07,
            "2.5"   => 153.04,
            "3"     => 150,
            "3.5"   => 150,
            "4"     => 150,
            "4.5"   => 150,
            "5"     => 150,
            "5.5"   => 150,
            "6"     => 150,
            "6.5"   => 150,
            "7"     => 150,
            "7.5"   => 150,
            "8"     => 150,
            "8.5"   => 150,
            "9"     => 150,
            "9.5"   => 150,
            "10"    => 150,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function philhealth_adjustment_258($target = ""){
        $data = array(
            "0"    => 0,
            "0.5"   => 0,
            "1"    => 0,
            "1.5"  => 0,
            "2"    => 0,
            "2.5"  => 0,
            "3"    => 0.99,
            "3.5"  => 5.02,
            "4"    => 9.05,
            "4.5"  => 13.08,
            "5"    => 17.11,
            "5.5"  => 21.14,
            "6"    => 25.17,
            "6.5"  => 29.20,
            "7"    => 33.23,
            "7.5"  => 37.26,
            "8"    => 41.29,
            "8.5"  => 45.32,
            "9"    => 49.35,
            "9.5"  => 53.38,
            "10"   => 57.41,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function philhealth_310($target = ""){
        $data = array(
            "0"       => 208.09,
            "0.5"     => 204.06,
            "1"       => 200.03,
            "1.5"     => 196.00,
            "2"       => 191.98,
            "2.5"     => 187.95,
            "3"       => 183.92,
            "3.5"     => 179.89,
            "4"       => 175.87,
            "4.5"     => 171.84,
            "5"       => 167.81,
            "5.5"     => 163.78,
            "6"       => 159.76,
            "6.5"     => 155.73,
            "7"       => 151.70,
            "7.5"     => 150,
            "8"       => 150,
            "8.5"     => 150,
            "9"       => 150,
            "9.5"     => 150,
            "10"      => 150,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function philhealth_adjustment_310($target = ""){
        $data = array(
            "0"   => 0,
            "0.5" => 0,
            "1"   => 0,
            "1.5" => 0,
            "2"   => 0,
            "2.5" => 0,
            "3"   => 0,
            "3.5" => 0,
            "4"   => 0,
            "4.5" => 0,
            "5"   => 0,
            "5.5" => 0,
            "6"   => 0,
            "6.5" => 0,
            "7"   => 0,
            "7.5" => 2.33,
            "8"   => 6.36,
            "8.5" => 10.39,
            "9"   => 12.35,
            "9.5" => 18.45,
            "10"  => 22.48,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function philhealth_313($target = ""){
        $data = array(
            "0"       => 210.10,
            "0.5"     => 206.07,
            "1"       => 202.04,
            "1.5"     => 198.02,
            "2"       => 193.99,
            "2.5"     => 189.96,
            "3"       => 185.93,
            "3.5"     => 181.91,
            "4"       => 177.88,
            "4.5"     => 173.85,
            "5"       => 169.82,
            "5.5"     => 165.80,
            "6"       => 161.77,
            "6.5"     => 157.74,
            "7"       => 153.71,
            "7.5"     => 150,
            "8"       => 150,
            "8.5"     => 150,
            "9"       => 150,
            "9.5"     => 150,
            "10"      => 150,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }

    function philhealth_adjustment_313($target = ""){
        $data = array(
            "0"       => 0,
            "0.5"     => 0,
            "1"       => 0,
            "1.5"     => 0,
            "2"       => 0,
            "2.5"     => 0,
            "3"       => 0,
            "3.5"     => 0,
            "4"       => 0,
            "4.5"     => 0,
            "5"       => 0,
            "5.5"     => 0,
            "6"       => 0,
            "6.5"     => 0,
            "7"       => 0,
            "7.5"     => 0.32,
            "8"       => 4.35,
            "8.5"     => 8.38,
            "9"       => 12.41,
            "9.5"     => 16.44,
            "10"      => 20.47,
        );
        if(!empty($data[$target])){
            return $data[$target];
        }
        else{
            return $data;
        }
    }
    
}


?>