<?php 
if ( ! function_exists('__js'))
{
    function __js($filedir='') {
        echo '  <script src="'.base_url($filedir).'"></script>';
    }
    function __css($filedir='') {
        echo '  <link rel="stylesheet" href="'.base_url($filedir).'">';
    }
     function __js_external($filedir='') {
        echo '  <script src="'.($filedir).'"></script>';
    }
    function __css_external($filedir='') {
        echo '  <link rel="stylesheet" href="'.($filedir).'">';
    }

    function js_input($id="",$val=""){
        return '$("#'.($id).'").val("'.($val).'");'."\n";
    }

    function js_radio($name="",$val=""){
         return'$("input[name='."'".$name."'".'][value='."'".strtolower($val)."'".']").attr("checked", "checked");'."\n";

        // return '$("input[name='."'".$name."'".']").val("'.__t_raw($val).'");'."\n";
    }

    function js_checkbox($id="",$val="",$display_form = ""){
        $checked = "";
        if( !empty(@$val) || @$val != 0){
            $checked .= '$("#'.__t_raw($id).'").attr("checked","checked");'."\n";         
            if(!empty($display_form)){
                $checked .= '$("#'.__t_raw($display_form).'").fadeIn();'."\n";  
            }
        }
        return $checked;
    }
    function js_html($id="",$val=""){
        return '$("#'.__t_raw($id).'").html("'.__t_raw($val).'");'."\n";
    }

    function js_append($id="",$val=""){
        return '$("#'.__t_raw($id).'").append("'.$val.'");'."\n";
    }   
}


if ( ! function_exists('bootstrapalize'))
{
    function bootstrapalize($label = '',$value = '',$class = 'col-md-6 nomargin nopadding') {
        return '<div class="'.$class.'"><div class="infoLabels col-md-6">'.$label.'</div><div class="col-md-6">'.__t(strtoupper($value)).'</div></div>'."\n";
    }
    function bootstrapalize_2($label = '',$value = '',$class = 'col-md-12 nomargin nopadding') {
        return '<div class="'.$class.'"><div class="infoLabels col-md-2">'.$label.'</div><div class="col-md-10">'.__t(strtoupper($value)).'</div></div>'."
        \n";
    }
    function bootstrapalize_3($label = '',$value = '',$class = 'col-md-12 nomargin nopadding') {
        return '<div class="'.$class.'"><div class="infoLabels col-md-3">'.$label.'</div><div class="col-md-9">'.__t(strtoupper($value)).'</div></div>'."\n";
    }
    function bootstrapalize_4($label = '',$value = '',$class = 'col-md-6 nomargin nopadding') {
        return '<div class="'.$class.'"><div class="infoLabels col-md-4">'.$label.'</div><div class="col-md-8">'.__t(strtoupper($value)).'</div></div>'."\n";
    }
    function bootstrapalize_4_sm($label = '',$value = '',$class = 'col-sm-6 nomargin nopadding') {
        return '<div class="'.$class.'"><div class="infoLabels col-sm-4">'.$label.'</div><div class="col-sm-8">'.__t(strtoupper($value)).'</div></div>'."\n";
    }
    function bootstrapalize_12($label = '',$value = '',$class = 'col-md-12 nomargin nopadding') {
        return '<div class="'.$class.'"><div class="infoLabels col-md-6">'.$label.'</div><div class="col-md-6">'.__t(strtoupper($value)).'</div></div>'."\n";
    }
    function bootstrapalize_center_value($value = '',$class = 'col-md-12 nomargin nopadding') {
        return '<div class="'.$class.'"><div class="col-md-12 text-center">'.__t(strtoupper($value)).'</div></div>'."\n";
    }
    function bootstrapalize_longtext($label = '',$value = '',$class = 'col-md-12 nomargin nopadding') {
        return '<div class="'.$class.'"><div class="infoLabels col-md-12">'.$label.'</div><div class="col-md-12">'.__t(strtoupper($value)).'</div></div>'."\n";
    }

    function bootstrap_subform($id,$label,$value = "",$options = []){

        $req_note       = "";
        $required       = "";
        $readonly       = "";
        $disabled       = "";
        $class          = "";
        $placeholder    = $label;
        $input_type     = "text";
        $input_step     = "";
        if(!empty($options['required'])){
            $req_note   = "<b class='req'>*</b>";
            $required   = "required";
        }
        if(!empty($options['disabled'])){
            $disabled   = "disabled";
            $req_note   = "";
            $required   = "";
        }
        if(!empty($options['readonly'])){
            $readonly   = "readonly";
        }
        if(!empty($options['class'])){
            $class      = $options['class'];
        }
        if(!empty($options['placeholder'])){
            $placeholder= $options['placeholder'];
        }
        if(!empty($options['type'])){
            $input_type  = $options['type'];
            if($input_type == "number"){
                $input_step = "step='.01'";
            }
        }

        $return = "
            <div class='input-group addr-group'>
                <span class='input-group-addon addr-label'> $label $req_note </span>
                <input class='form-control' type='$input_type' $input_step placeholder='$placeholder' name='$id' id='$id' $disabled $readonly $required value='$value'>
            </div>
            ";
        
        return $return;
    }



    function bootstrap_form($id,$label,$value = "",$options = []){
        /*Options
            req(bool)        = if form input is required
            mreq(bool)       = if MODAL input is required
            readonly(bool)   = set to readonly 
            placeholder(str) = if placeholder is custom, otherwise use label's text
            disabled(bool)   = set to disabled,
            class(str)       = add classes to parent element
            name(str)        = if name is empty, use ID's value as name
            min(int)         = minimum character required in input, 0 = infinite
            max(int)         = maximum character required in input, 0 = infinite
            type             = input type, default for text
        Note: ID is also used as form NAME
        */
        $req_note       = "";
        $required       = "";
        $readonly       = "";
        $disabled       = "";
        $class          = "";
        $nospace          = "";
        $input_class    = "";
        $namesuffix     = "";
        $input_type     = "text";
        $input_step     = "";
        $extra_attr     = "";
        $min    = "";
        $max    = "";
        $placeholder    = $label;
        if(!empty($options['req'])){
            $req_note   = "<b class='req'>*</b>";
            $required   = "required";
        }
        if(!empty($options['disabled'])){
            $disabled   = "disabled";
            $req_note   = "";
            $required   = "";
        }
        if(!empty($options['readonly'])){
            $readonly    = "readonly";
        }
        if(!empty($options['class'])){
            $class       = $options['class'];
        }
        if(!empty($options['nospace'])){
            $nospace      = "marginless padless";
        }
        if(!empty($options['placeholder'])){
            $placeholder = $options['placeholder'];
        }
        if(!empty($options['mreq'])){
            $req_note    = "<b class='req'>*</b>";
            $input_class.= " mreq";
        }
        if(!empty($options['smreq'])){
            $req_note    = "<b class='req'>*</b>";
            $input_class.= " smreq";
        }
        if(!empty($options['min'])){
            $min   = "minlength='".$options['min']."'";
        }
        if(!empty($options['max'])){
            $max   = "maxlength='".$options['max']."'";
        }
        if(!empty($options['step'])){
            $input_step = "step='".$options['step']."'";
        }
        if(!empty($options['type'])){
            $input_type = $options['type'];
        }
        if(!empty($options['arrayinput'])){
            $namesuffix  = "[]";
        }
        if(!empty($options['attrib'])){
            $extra_attr  = $options['attrib'];
        }


        $return = "
        <div class='$class $nospace'>
          <div class='form-group col-md-12 $nospace'>
            <label for='$id' class='control-label'>$label $req_note <span id='".$id."_extra'></span></label>
            <input type='$input_type' $input_step class='form-control $input_class' id='$id'  name='".$id.$namesuffix."' placeholder='$placeholder' value='$value' $min $max $required $readonly $disabled $extra_attr>
          </div>
        </div>";
        
        return $return;
    }

    function bootstrap_form_textarea($id,$label,$value = "",$options = []){
        /*Options
            req(bool)        = if form input is required
            mreq(bool)       = if MODAL input is required
            readonly(bool)   = set to readonly 
            placeholder(str) = if placeholder is custom, otherwise use label's text
            disabled(bool)   = set to disabled,
            class(str)       = add classes to parent element
            name(str)        = if name is empty, use ID's value as name
        Note: ID is also used as form NAME
        */
        $req_note       = "";
        $required       = "";
        $readonly       = "";
        $disabled       = "";
        $class          = "";
        $nospace          = "";
        $input_class    = "";
        $placeholder    = $label;
        if(!empty($options['req'])){
            $req_note   = "<b class='req'>*</b>";
            $required   = "required";
        }
        if(!empty($options['disabled'])){
            $disabled   = "disabled";
            $req_note   = "";
            $required   = "";
        }
        if(!empty($options['readonly'])){
            $readonly   = "readonly";
        }
        if(!empty($options['class'])){
            $class      = $options['class'];
        }
        if(!empty($options['nospace'])){
            $nospace      = "marginless padless";
        }
        if(!empty($options['placeholder'])){
            $placeholder= $options['placeholder'];
        }
        if(!empty($options['mreq'])){
            $req_note   = "<b class='req'>*</b>";
            $input_class.= " mreq";
        }
        if(!empty($options['smreq'])){
            $req_note   = "<b class='req'>*</b>";
            $input_class.= " smreq";
        }


        $return = "
        <div class='$class $nospace'>
          <div class='form-group col-md-12 $nospace'>
            <label for='$id' class='control-label'>$label $req_note</label>
            <textarea type='text' class='form-control $input_class' id='$id' name='$id' placeholder='$placeholder' $required $readonly $disabled>$value</textarea>
          </div>
        </div>";
        
        return $return;
    }

    function bootstrapalize_id($label = '',$id = '') {
        return '<p class="col-md-12"><div class="infoLabels col-md-6">'.$label.'</div><div id= "'.$id.'" class="col-md-6">{DISPLAY.ERROR}</div></p>'."\n";
    }
}

if ( ! function_exists('__t'))
{
	function __t($value='') {
        $return = '<i class="unavailable">Not Available</i>';
        if(!empty($value) && $value != ' ')
        {$return = ($value);}
        return $return;
    }

    function __link($link = '', $text = '', $class = '') {
        if(!empty($class) && $class != ' '){
            $class  = 'class="'.htmlentities($class).'"';
        }
        $link   = base_url(htmlentities($link));
        $text   = htmlentities($text);
        if(empty($text) || $text == ' ') {
            $text = "Click Here";
        }
        $return = "<a $class href='$link'>$text</a>";
        return $return;
    }

    function __t_input($value='') {
        $return = '';
        if(!empty($value) && $value != ' ')
        {$return = ($value);}
        return $return;
    }

    function __t_raw($value='') {
        return ($value);
    }


    function add_ellipsis($text = '', $max = 100){
        if(strlen($text) > $max ){
            $text = substr($text,0,$max)."...";
        }
        return $text;
    }

    function __date2($d, $f= 'M j, Y h:i A'){
        if($d){
            $return = new DateTime($d);
            $return = $return->format($f);
        }
        else{
            $return = "";
        }
        return $return;
    }

    function __date($d=null, $f= 'M j, Y h:i A'){
        $d = strtotime($d);
        if($d<"0"){
            return "";
        }
        if($d){
            $return = date($f, $d);
        }
        else{
            $return = "";
        }
        return $return;
    }

    //database entry trimming
    function __dbtrim($text = ""){
        // $text = preg_replace('/\s+/', ' ', $text);
        // $text = trim(preg_replace('/\s+/', ' ', $text));
        return (htmlentities(trim($text)));
    }

    function __datenow(){
        return date("Y-m-d H:i:s");
    }

    function __dateonlynow(){
        return date("Y-m-d");
    }

    function __itemid($id){
        return str_pad($id, 6, 0, STR_PAD_LEFT);
    }

    function __tranno($id){
        return str_pad($id, 10, 0, STR_PAD_LEFT);
    }

    function __brand($brand){
        if(empty($brand)){
            return  "";
        }
        return "- ".$brand;
    }

    function __peso($val){
        //check if numerical first
        if(is_numeric($val)){
            //if numric, format it
            $val = number_format($val,2);
        }
        return $val;
    }

    function __getage($date){
        $date = __date($date,'m/d/Y');
        if(!empty($date)){
           $birthDate = explode("/", $date);
            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                ? ((date("Y") - $birthDate[2]) - 1)
                : (date("Y") - $birthDate[2]));
            return $age; 
        }
        
    }

    function staffonly($session){
        if(empty($session->staff)){
            redirect("/");
        }
    }

    function adminonly($session){
        if(empty($session->admin)){
            redirect("/");
        }
    }

    function checksession($session){
        if(empty($session->uid)){
            redirect("/login");
        }
    }
}

?>