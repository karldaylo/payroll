<?php
class Report_db extends CI_Model {
	//this model is about all CRUD access in account DB
	public function __construct(){
        parent::__construct();
        $this->load->database('default');
    }

    // public function get_company_list(){

    //     $result = [];
    //     $this->db->distinct();
    //     $this->db->select("branch_name as label");
    //     // $this->db->select("branch_name as label");
    //     $this->db->from('lookup_office');
    //     $query   = $this->db->get();
    //     $result  = $query->result_array();
    //     if(!empty($result[0])){
    //         return $result;
    //     }
    //     return $result;
    // }

    public function get_company_list(){

        $result = [];
        $this->db->select("*");
        $this->db->from('lookup_company');
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            return $result;
        }
        return $result;
    }

    public function get_location_list(){

        $result = [];
        $this->db->distinct();
        $this->db->select("*");
        $this->db->from('lookup_location');
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            return $result;
        }
        return $result;
    }

    public function get_branch_list(){

        $result = [];
        $this->db->select("*");
        $this->db->from('lookup_branch');
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            return $result;
        }
        return $result;
    }

    public function get_company_branch_list(){

        $result = [];
        $this->db->select("CONCAT(branch_name, ' - ', branch_location) as label");
        // $this->db->select("CONCAT(branch_name, ' ', branch_location) as label");
        $this->db->from('lookup_office');
        $this->db->order_by('branch_name');
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            return $result;
        }
        return $result;
    }

	public function download_item($options=[]){
        $this->db->select('*');
		$this->db->from('item_list a');
        $this->db->join('item_description b', 'b.item_id = a.item_id and b.disabled=0', 'left');
        $this->db->order_by('b.last_modified', 'DESC');
		$query    = $this->db->get();
        $result   = $query->result_array();
        $return[] = array(
            "unit_name"         =>"INVENTORY NAME",
            "unit_brand"        =>"BRAND",
            "unit_description"  =>"DESCRIPTION",
            "unit_usage"        =>"USAGE",
            "unit_category"     =>"CATEGORY",
            "unit_total"        =>"TOTAL QTY STORED",
            "unit_price"        =>"PRICE (in PHP)",
            "totalprice"        =>"TOTAL PRICE",
            "datecreated"       =>"DATE CREATED",
            "lastmodified"      =>"LAST UPDATE",
        );
        $total    = 0;
        $totalqty = 0;
		
        if(!empty($result[0])){
            foreach ($result as $key => $row) {
                $qty          = $this->getItemTotal($result[$key]['item_id']);
                if(is_numeric($qty) && is_numeric($row['unit_price'])){
                    $price    = $qty * $row['unit_price'];
                }
                $unit_pricing = $row['unit_price'];
                if(is_numeric($row['unit_price'])){
                    $unit_pricing = number_format($row['unit_price'],2);
                }
                $result[$key]['date_created']   = __date($row['date_created']);
                $total                          = $price + $total;
                $totalqty                       = $qty + $totalqty;
                $return[] = array(
                    "unit_name"         => $row['unit_name'],
                    "unit_brand"        => $row['unit_brand'],
                    "unit_description"  => $row['unit_description'],
                    "unit_usage"        => $row['unit_usage'],
                    "unit_category"     => $row['unit_category'],
                    "totalqty"          => $qty,
                    "price"             => $unit_pricing,
                    "totalprice"        => $this->getItemTotal($row['item_id']),
                    "datecreated"       => __date($row['date_created']),
                    "lastmodified"      => __date($row['last_modified']),
                );  
            }

        }
        $return[] = array("str"=>"Total Units: ".number_format($totalqty,2));
        $return[] = array("str"=>"Grand Total: PHP ".number_format($total,2));
        // return $return;
        $this->array_to_csv_download($return,"inventory_".date('Ymd').".csv");
	}

    private function getItemTotal($item_id){
        $this->db->select('SUM(quantity) as total');
        $this->db->from('item_count');
        $this->db->where('item_id',$item_id);
        $this->db->where('disabled',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['total'])){
            return $result[0]['total'];
        }else{
            return 0;
        }
    }

    public function download_users($options=[]){
        $this->db->select('*');
        $this->db->from('user_account a');
        $this->db->join('user_details b', 'b.user_id = a.uid and b.disabled=0', 'left');
        $this->db->order_by('b.last_name',"asc");
        $query    = $this->db->get();
        $result   = $query->result_array();
        $return[] = array(
            "lname"          =>"LAST NAME",
            "fname"          =>"FIRST NAME",
            "mname"          =>"MIDDLE NAME",
            "uname"          =>"USERNAME",
            "desig"          =>"DESIGNATION",
            "deprt"          =>"DEPARTMENT",
            "offc"           =>"BASE OFFICE",
            "email"          =>"EMAIL ADDRESS",
            "phone"          =>"MOBILE #"
        );
        
        if(!empty($result[0])){
            foreach ($result as $key => $row) {
                $return[] = array(
                    "lname"          =>$row['last_name'],
                    "fname"          =>$row['first_name'],
                    "mname"          =>$row['middle_name'],
                    "uname"          =>$row['username'],
                    "desig"          =>$row['designation'],
                    "deprt"          =>$row['department'],
                    "offc"           =>$row['office'],
                    "email"          =>$row['email'],
                    "phone"          =>$row['mobile'],
                );
            }
        }
        // return $return;
        $this->array_to_csv_download($return,"users_".date('Ymd').".csv");
    }

    private function array_to_csv_download($array, $filename = "report.csv", $delimiter=",") {
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen('php://memory', 'w'); 
        // loop over the input array
        foreach ($array as $line) { 
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter); 
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        // tell the browser it's going to be a csv file
        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$filename.'";');
        // make php send the generated csv lines to the browser
        fpassthru($f);
    }

    public function download_employee($options=[]){
        $this->db->select('*');
        $this->db->select('CONCAT(d.branch_name, " ",f.company_name,", ",e.city_name) as branch');
        $this->db->from('user_account a');
        $this->db->join('employee_records b',           'b.user_id      = a.uid and b.disabled=0', 'left');
        $this->db->join('employee_requirements c',      'c.user_id      = a.uid and c.disabled=0', 'left');
        $this->db->join('lookup_branch d',              'd.branch_id    = c.company_assigned'    , 'left');
        $this->db->join('lookup_location e',            'e.city_id      = d.city_id'             , 'left');
        $this->db->join('lookup_company f',             'f.company_id   = d.company_id'          , 'left');
        $this->db->where('a.staff',0);
        $this->db->where('a.disabled',0);
        $query    = $this->db->get();
        $result   = $query->result_array();
        $return[] = array(
            "name"                  =>"FULL NAME",
            "first_name"            =>"FIRST NAME",
            "middle_name"           =>"MIDDLE NAME",
            "last_name"             =>"LAST NAME",
            "employee_code"         =>"EMPLOYEE CODE",
            "employee_type"         =>"EMPLOYEE CODE",
            "branch"                =>"COMPANY ASSIGNED",
            "gender"                =>"GENDER",
            "birth_date"            =>"BIRTH DATE",
            "contact"               =>"CONTACT",
            "home_address"          =>"ADDRESS"
        );
        if(!empty($result[0])){
            $filename="";
            foreach ($result as $key => $row) {
                $nickname = "";
                if(!empty($row['nick_name'])){
                    $nickname = " (".$row['nick_name'].")";
                }

                $name = $row['last_name'].", ".$row['first_name']." ".$row['middle_name'].$nickname;
                $filename=$row['last_name']."_".$row['first_name'];
                $return[] = array(
                     "name"              =>$name,
                     "first_name"        =>$row['first_name'],
                     "middle_name"       =>$row['middle_name'],
                     "last_name"         =>$row['last_name'],
                     "employee_code"     =>$row['employee_code'],
                     "employee_type"     =>$row['employee_type'],
                     "branch"            =>$row['branch'],
                     "gender"            =>$row['gender'],
                     "birth_date"         =>__date($row['birth_date'],"Y-m-d"),
                     "contact"           =>$row['contact'],
                     "home_address"           =>$row['home_address'],
                );
            }
            $this->array_to_csv_download($return,"employeelist_".date('Ymd').".csv");
        }else{
            "no resident list";
        }
        // die(var_dump($return));
        // return $return;
    }

    public function get_transaction($patient_id,$data){
        $page  = 0;
        $limit = 100;
        if(!empty($data['page'])){
            $page = $data['page'];
        }
        if(!empty($data['limit'])){
            $limit = $data['limit'];
        }

        $return['page'] = $page;
        $return['limit'] = $limit;

        $this->db->select('*');
        $this->db->from('patient_description');
        $this->db->where('patient_id',$patient_id);
        $this->db->where('disabled',0);
        $this->db->limit(1);
        $query    = $this->db->get();
        $details   = $query->result_array();
        if(empty($details[0])){
            die("Patient does not exists");
        }

        $return['patient'] = $details[0];

        $return['balance'] = $this->getBalance($patient_id);

        $this->db->select('*');
        $this->db->from('user_credit');
        $this->db->where('patient_id',$patient_id);
        $this->db->where('disabled',0);
        if($limit>0){
            // if($page == 0){
            //  $page = 1;
            // }
            // $offset = ($page - 1) * $limit;
            $offset = $page * $limit;
            $this->db->limit($limit,$offset);
        }
        if(!empty($data['start_date'])){
            $this->db->where('date_created >='   ,$data['start_date']);
        }
        if(!empty($data['end_date'])){
            $this->db->where('date_created <='   ,$data['end_date']);
        }
        $this->db->order_by('date_created','desc');
        $query    = $this->db->get();
        $result   = $query->result_array();

        if(!empty($result[0])){
            $return['transactions'] = $result;
        }else{
            $return['transactions'] = [];
        }

        return $return;
        
    }



    public function get_userlog($uid,$data){
        $page  = 0;
        $limit = 100;
        if(!empty($data['page'])){
            $page = $data['page'];
        }
        if(!empty($data['limit'])){
            $limit = $data['limit'];
        }

        $return['page'] = $page;
        $return['limit'] = $limit;

        $this->db->select('*');
        $this->db->from('user_account');
        $this->db->where('uid',$uid);
        $this->db->where('disabled',0);
        $this->db->limit(1);
        $query    = $this->db->get();
        $details   = $query->result_array();
        if(empty($details[0])){
            die("User does not exists");
        }

        $return['user'] = $details[0];

        $this->db->select('*');
        $this->db->from('audit_logs');
        $this->db->where('user_id',$uid);
        $this->db->where('disabled',0);
        $this->db->order_by('date_created','desc');
        if($limit>0){
            // if($page == 0){
            //  $page = 1;
            // }
            // $offset = ($page - 1) * $limit;
            $offset = $page * $limit;
            $this->db->limit($limit,$offset);
        }
        if(!empty($data['start_date'])){
            $this->db->where('date_created >='   ,$data['start_date']);
        }
        if(!empty($data['end_date'])){
            $this->db->where('date_created <='   ,$data['end_date']);
        }
        $this->db->order_by('date_created','desc');
        $query    = $this->db->get();
        $result   = $query->result_array();

        if(!empty($result[0])){
            $return['logs'] = $result;
        }else{
            $return['logs'] = [];
        }

        return $return;
        
    }

     public function get_alllog($data){
        $page  = 0;
        $limit = 100;
        if(!empty($data['page'])){
            $page = $data['page'];
        }
        if(!empty($data['limit'])){
            $limit = $data['limit'];
        }

        $return['page'] = $page;
        $return['limit'] = $limit;

        $this->db->select('*');
        $this->db->from('audit_logs');
        $this->db->where('disabled',0);
        $this->db->order_by('date_created','desc');
        if($limit>0){
            // if($page == 0){
            //  $page = 1;
            // }
            // $offset = ($page - 1) * $limit;
            $offset = $page * $limit;
            $this->db->limit($limit,$offset);
        }
        if(!empty($data['start_date'])){
            $this->db->where('date_created >='   ,$data['start_date']);
        }
        if(!empty($data['end_date'])){
            $this->db->where('date_created <='   ,$data['end_date']);
        }
        $this->db->order_by('date_created','desc');
        $query    = $this->db->get();
        $result   = $query->result_array();

        if(!empty($result[0])){
            $return['logs'] = $result;
        }else{
            $return['logs'] = [];
        }

        return $return;
        
    }



}