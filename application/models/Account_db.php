<?php
class Account_db extends CI_Model {
	//this model is about all CRUD access in account DB
	
	public function __construct(){
		parent::__construct();
		$this->load->database('default');
	}

	private function randomString($size=5){
	    $c = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $l = strlen($c);
	    $r = '';
	    for ($i = 0; $i < $size; $i++) {
	        $r .= $c[rand(0, $l - 1)];
	    }
	    return $r;
	}

	private function passhash($pass, $salt=''){
		htmlentities($pass);
		if(empty($salt))
		{$salt =  $this->randomString();}
		$return = array(
			"password" => md5(sha1(md5($pass.$salt))),
			"raw" => $pass,
			"salt" => $salt,
		);		
		return $return;
	}

	private function if_db($value,$table = 'user_account',$field = 'username'){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		$this->db->limit(1);
		return ($this->db->count_all_results() > 0)?true:false;
	}

	private function set_session($data){
		$newsession = array(
	        'username'  => $data['username'],
	        'uid'  		=> $data['uid'],
	        'userid'  	=> $data['uid'],
	        'fullname' 	=> $data['fullname'],
	        'admin' 	=> $data['admin'],
	        'staff' 	=> $data['staff'],
	        // 'branch' 	=> $data['office_code'],
		);
		$this->session->set_userdata($newsession);
	}

	public function omni_user_search($filter){
        $this->db->select('*');
        $this->db->from('user_account');
        // $this->db->where('a.ban',0);
        $this->db->order_by('fullname','asc');
        $this->db->like('username', $filter);
        $this->db->or_like('email', $filter);
        $this->db->or_like('mobile', $filter);
        $this->db->or_like('designation', $filter);
        $this->db->or_like('office_code', $filter);
        $this->db->or_like('fullname', $filter);
        
        $this->db->limit(100);
        
        $query   = $this->db->get();
        $result  = $query->result_array();

        if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
    }

	public function get_user_list($limit=0,$page=0){
		$this->db->select('*');
		$this->db->from('user_account');
		$this->db->where('disabled',0);
		$this->db->where('admin',0);
		$this->db->where('staff',0);
		$this->db->order_by('fullname','asc');
		if($limit>0){
			// if($page == 0){
			// 	$page = 1;
			// }
            // $offset = ($page - 1) * $limit;
			$offset = $page * $limit;
			$this->db->limit($limit,$offset);
		}
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function get_employee_as_admin_list($limit=0,$page=0){
		$this->db->select('*');
		// $this->db->select('CONCAT(last_name,", ",first_name," ",middle_name) as fullname');
		$this->db->from('user_account a');
		$this->db->select('CONCAT(d.branch_name," ",d.branch_location) as branch');
    	$this->db->join('employee_records b', 			'b.user_id = a.uid and b.disabled=0', 'left');
    	$this->db->join('employee_requirements c', 		'c.user_id = a.uid and c.disabled=0', 'left');
    	$this->db->join('lookup_office d', 				'd.office_id = c.company_assigned and d.disabled=0', 'left');
		$this->db->where('a.admin',0);
		$this->db->where('a.disabled',0);
		$this->db->order_by('last_name','asc');
		if($limit>0){
			// if($page == 0){
			// 	$page = 1;
			// }
            // $offset = ($page - 1) * $limit;
			$offset = $page * $limit;
			$this->db->limit($limit,$offset);
		}
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function get_employee_list($filter="",$limit=0,$page=0){
		$this->db->select('*');
		$this->db->select('CONCAT(d.branch_name, " ",f.company_name,", ",e.city_name) as branch');
		$this->db->from('user_account a');
    	$this->db->join('employee_records b', 			'b.user_id 		= a.uid and b.disabled=0', 'left');
    	$this->db->join('employee_requirements c', 		'c.user_id 		= a.uid and c.disabled=0', 'left');
    	$this->db->join('lookup_branch d', 				'd.branch_id 	= c.company_assigned' 	 , 'left');
    	$this->db->join('lookup_location e',			'e.city_id  	= d.city_id' 			 , 'left');
    	$this->db->join('lookup_company f',			 	'f.company_id 	= d.company_id' 		 , 'left');
		$this->db->where('a.staff',0);
		$this->db->where('a.disabled',0);
		if(!empty($filter)){
			$this->db->group_start();
			$this->db->like('a.username',$filter,'both');
			$this->db->or_like('a.email',$filter,'both');
			$this->db->or_like('a.mobile',$filter,'both');
			$this->db->or_like('a.fullname',$filter,'both');
			$this->db->or_like('b.first_name',$filter,'both');
			$this->db->or_like('b.middle_name',$filter,'both');
			$this->db->or_like('b.last_name',$filter,'both');
			$this->db->or_like('b.elementary_name',$filter,'both');
			$this->db->or_like('b.highschool_name',$filter,'both');
			$this->db->or_like('b.college_name',$filter,'both');
			$this->db->or_like('c.employee_code',$filter,'both');
			$this->db->or_like('c.bank_account',$filter,'both');
			$this->db->or_like('c.philhealth',$filter,'both');
			$this->db->or_like('c.pagibig',$filter,'both');
			$this->db->or_like('c.sss',$filter,'both');
			$this->db->or_like('b.police_clearance',$filter,'both');
			$this->db->or_like('b.nbi_no',$filter,'both');
			$this->db->or_like('b.med_cert',$filter,'both');
			$this->db->group_end();
		}
		// $this->db->order_by('last_name','asc');
		$this->db->order_by('a.date_created','\desc');
		if($limit>0){
			// if($page == 0){
			// 	$page = 1;
			// }
            // $offset = ($page - 1) * $limit;
			$offset = $page * $limit;
			$this->db->limit($limit,$offset);
		}
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function get_officer_list($filter="",$limit=0,$page=0){
		$this->db->select('*');
		// $this->db->select('CONCAT(last_name,", ",first_name," ",middle_name) as fullname');
		$this->db->from('user_account a');
		$this->db->select('CONCAT(d.branch_name," ",d.branch_location) as branch');
    	$this->db->join('employee_records b', 			'b.user_id = a.uid and b.disabled=0', 'left');
    	$this->db->join('employee_requirements c', 		'c.user_id = a.uid and c.disabled=0', 'left');
    	$this->db->join('lookup_office d', 				'd.office_id = c.company_assigned and d.disabled=0', 'left');
		$this->db->where('a.staff',1);
		$this->db->where('a.admin',0);
		$this->db->where('a.disabled',0);
		if(!empty($filter)){
			$this->db->group_start();
			$this->db->like('a.username',$filter,'both');
			$this->db->or_like('a.email',$filter,'both');
			$this->db->or_like('a.mobile',$filter,'both');
			$this->db->or_like('a.fullname',$filter,'both');
			$this->db->or_like('b.first_name',$filter,'both');
			$this->db->or_like('b.middle_name',$filter,'both');
			$this->db->or_like('b.last_name',$filter,'both');
			$this->db->or_like('b.elementary_name',$filter,'both');
			$this->db->or_like('b.highschool_name',$filter,'both');
			$this->db->or_like('b.college_name',$filter,'both');
			$this->db->or_like('c.employee_code',$filter,'both');
			$this->db->or_like('c.bank_account',$filter,'both');
			$this->db->or_like('c.philhealth',$filter,'both');
			$this->db->or_like('c.pagibig',$filter,'both');
			$this->db->or_like('c.sss',$filter,'both');
			$this->db->or_like('b.police_clearance',$filter,'both');
			$this->db->or_like('b.nbi_no',$filter,'both');
			$this->db->or_like('b.med_cert',$filter,'both');
			$this->db->group_end();
		}
		$this->db->order_by('last_name','asc');
		if($limit>0){
			// if($page == 0){
			// 	$page = 1;
			// }
            // $offset = ($page - 1) * $limit;
			$offset = $page * $limit;
			$this->db->limit($limit,$offset);
		}
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}


	public function get_branches(){
		$this->db->select('*');
		$this->db->from('lookup_office');
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function get_dashboarduser(){
        $this->db->select('COUNT(uid) as total');
        $this->db->from('user_account');
        $this->db->where('disabled',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['total'])){
            return $result[0]['total'];
        }else{
            return 0;
        }
    }

    public function get_payroll_details($id = 0){
        $this->db->select('*');
        $this->db->from('attendance_table');
        $this->db->where('disabled',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        $return  = [];
        if(!empty(@$result[0])){
            $return['payroll'] 	=  $result[0];
            $return['others'] 	=  $this->get_payroll_others($id);
        }
        return $return;

    }

    private function get_payroll_others($id = 0){
        $this->db->select('*');
        $this->db->from('attendance_others');
        $this->db->where('disabled',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0])){
            return $result[0];
        }else{
            return [];
        }
    }

	public function get_employee_details($user_id){
		$return = [];
		if($this->if_db($user_id,'employee_records','user_id')){
			$this->db->select('*');
			$this->db->select('CONCAT(d.branch_name," ",d.branch_location) as branch');
			$this->db->from('user_account a');
	    	$this->db->join('employee_records b', 			'b.user_id = a.uid and b.disabled=0', 'left');
	    	$this->db->join('employee_requirements c', 		'c.user_id = a.uid and c.disabled=0', 'left');
	    	$this->db->join('lookup_office d', 				'd.office_id = c.company_assigned and d.disabled=0', 'left');
			$this->db->where('a.uid',$user_id);

			$this->db->limit(1);

			$query = $this->db->get();  
			$result = $query->result_array();

			// die($this->db->last_query());

			if(!empty($result[0])){
				$return = $result[0];
	        	$return['logs'] 			= $this->getUserLogs($user_id);
	        	$return['attendance'] 		= $this->getUserAttendance($user_id);
	        	$return['attachment'] 		= $this->getUserAttachment($user_id);
	        	$return['dependent']		= $this->getUserDependent($user_id);
	        	$return['work_experience']	= $this->getUserWorkExperience($user_id);
	        	$return['training'] 		= $this->getUserTraining($user_id);
	        	$return['character'] 		= $this->getUserCharacterReference($user_id);
	        	$return['office'] 			= $this->getUserOffice($return['company_assigned']);
			}
		}
		return $return; 
		
	}

	public function get_user_details($user_id){
		$return = [];
		if($this->if_db($user_id,'user_account','uid')){
			$this->db->select('*');
			$this->db->select('CONCAT(d.branch_name," ",d.branch_location) as branch');
			$this->db->from('user_account a');
        	$this->db->join('employee_records b', 'a.uid = b.user_id and b.disabled=0', 'left');
        	$this->db->join('employee_requirements c', 'c.user_id = b.user_id and c.disabled=0', 'left');
    		$this->db->join('lookup_office d',			'd.office_id = c.company_assigned and d.disabled=0', 'left');
			$this->db->where('a.uid',$user_id);
			$this->db->where('b.disabled',0);
			$this->db->limit(1);

			$query = $this->db->get();  
			$result = $query->result_array();
			
			if(!empty($result[0])){
				$return = $result[0];
			}
		}
		return $return; 
	}

	public function get_user_details_calc($user_id){
		$return = [];
		if($this->if_db($user_id,'user_account','uid')){
			$this->db->select('a.uid,a.username,a.fullname,a.img');
			$this->db->select('b.first_name,b.middle_name,b.last_name');
			$this->db->select('c.*');
			$this->db->select('CONCAT(d.branch_name," ",d.branch_location) as branch');
			$this->db->from('user_account a');
        	$this->db->join('employee_records b', 'a.uid = b.user_id and b.disabled=0', 'left');
        	$this->db->join('employee_requirements c', 'c.user_id = b.user_id and c.disabled=0', 'left');
    		$this->db->join('lookup_office d',			'd.office_id = c.company_assigned and d.disabled=0', 'left');
			$this->db->where('a.uid',$user_id);
			$this->db->where('b.disabled',0);
			$this->db->limit(1);

			$query = $this->db->get();  
			$result = $query->result_array();
			
			if(!empty($result[0])){
				$return = $result[0];
			}
		}
		return $return; 
		
	}

	public function getUserAttachment($user_id){
		$this->db->select('*');
		$this->db->from('reference_attachment');
		$this->db->where('user_id',$user_id);
		$this->db->where('disabled',0);
		$this->db->order_by('date_created','desc');
		$query   = $this->db->get();
        $result  = $query->result_array();
        // return $this->db->last_query();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function getUserAttendance($user_id,$m="",$d="",$y=""){
		$this->db->select('*');
		$this->db->from('attendance_table');
		$this->db->where('user_id',$user_id);
		if(!empty($m)){
			$this->db->where('att_month',$m);
		}
		if(!empty($d)){
			$this->db->where('att_date',$d);
		}
		if(!empty($y)){
			$this->db->where('att_year',$y);
		}
		$this->db->where('disabled',0);
		$this->db->order_by('date_created','desc');
		$query   = $this->db->get();
        $result  = $query->result_array();
        // return $this->db->last_query();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function getUserDependent($user_id){
		$this->db->select('*');
		$this->db->from('reference_dependents');
		$this->db->where('user_id',$user_id);
		$this->db->where('disabled',0);
		$this->db->order_by('dependent_name','desc');
		$query   = $this->db->get();
        $result  = $query->result_array();
        // return $this->db->last_query();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function getUserWorkExperience($user_id){
		$this->db->select('*');
		$this->db->from('reference_work_experience');
		$this->db->where('user_id',$user_id);
		$this->db->where('disabled',0);
		$this->db->order_by('date_created','desc');
		$query   = $this->db->get();
        $result  = $query->result_array();
        // return $this->db->last_query();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function getUserTraining($user_id){
		$this->db->select('*');
		$this->db->from('reference_trainings');
		$this->db->where('user_id',$user_id);
		$this->db->where('disabled',0);
		$this->db->order_by('date_created','desc');
		$query   = $this->db->get();
        $result  = $query->result_array();
        // return $this->db->last_query();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function getUserCharacterReference($user_id){
		$this->db->select('*');
		$this->db->from('reference_character');
		$this->db->where('user_id',$user_id);
		$this->db->where('disabled',0);
		$this->db->order_by('date_created','desc');
		$query   = $this->db->get();
        $result  = $query->result_array();
        // return $this->db->last_query();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	public function getUserOffice($branch_id){
		$this->db->select("CONCAT(a.branch_name,' | ',b.company_name,' ',c.city_name) as office_name");
		$this->db->select("a.branch_id");
		$this->db->select("a.branch_name");
		$this->db->select("b.company_id");
		$this->db->select("b.company_name");
		$this->db->select("c.city_id");
		$this->db->select("c.city_name");
		$this->db->from('lookup_branch a');
    	$this->db->join('lookup_company b' , 'a.company_id  = b.company_id  and b.disabled=0', 'left');
    	$this->db->join('lookup_location c', 'a.city_id  	= c.city_id  	and c.disabled=0', 'left');
		$this->db->where('branch_id',$branch_id);
		$this->db->limit(1);
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0])){
            return $result[0];
        }else{
            return [];
        }
	}

	private function getUserLogs($uid){
		$this->db->select('*');
		$this->db->from('audit_logs');
		$this->db->where('user_id',$uid);
		$this->db->where('disabled',0);
		$this->db->order_by('date_created','desc');
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	

	public function validateregistration($data){
 		$has_error = '';
 		$return['error'] = 0;

 		if($data['password'] != $data['cpassword']){
 			$return['error_msg'] = "Password does not match";
 			$return['target_id'] = "cpassword";
 			$has_error = 1;
 		}

 		if($this->if_db($data['email'])){
 			$return['error'] = 1;

 			$return['error_msg'] = "Email already registered";
 			$return['target_id'] = "email";
 			$has_error = 1;
 		}

 		if(!empty($has_error)){
 			$return['error'] = 1;
 		} else {
 			$return = $this->register_user($data);
 		}

 		return $return;
	}

	private function getUID($username){
        $this->db->select('*');
        $this->db->from('user_account');
        $this->db->where("username",$username);
        $this->db->limit(1);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['uid'])){
            return $result[0]['uid'];
        }else{
            return 0;
        }
	}

	private function getpasssalt($uid){
        $this->db->select('*');
        $this->db->from('user_account');
        $this->db->where("uid",$uid);
        $this->db->where("disabled",0);
        $this->db->limit(1);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['salt'])){
            return $result[0]['salt'];
        }else{
            return 0;
        }
	}

	private function getpasshash($uid){
        $this->db->select('*');
        $this->db->from('user_account');
        $this->db->where("uid",$uid);
        $this->db->where("disabled",0);
        $this->db->limit(1);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['passhash'])){
            return $result[0]['passhash'];
        }else{
            return 0;
        }
	}

	private function getimg($uid){
        $this->db->select('*');
        $this->db->from('user_account');
        $this->db->where("uid",$uid);
        $this->db->where("disabled",0);
        $this->db->limit(1);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['img'])){
            return $result[0]['img'];
        }else{
            return 0;
        }
	}

	public function upload_file($id,$data){
		// die(var_dump($data));
		//transaction start
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

		//upload attachment first
		$upload = $data['upload']['attach_file'];
		$photo_dir = "";
		if(!empty($upload['tmp_name'])){
            $ext            = pathinfo(@$upload['name'], PATHINFO_EXTENSION);
		    $newfilename    = date('ymdhsi_').md5(@$upload['name'].rand(0,100)).".".$ext;
		    $output         = UPLOAD_DIR.$newfilename;

	    	if (move_uploaded_file($upload["tmp_name"], $output)){ 
		       $photo_dir = $newfilename;//pass the filename so we can save in db
		    }else{
		    	die("file upload in ins enroll form failed");
		    }

		    $register_attachment = array(
	            'user_id'				=> $id,
	            'filename'				=> $photo_dir,
	            'description'  			=> $data['file_description'],
	            'date_created'      	=> __datenow(),
	        );

	        //insert step 1
	        if(!$this->db->insert('reference_attachment', $register_attachment)){
	            $status['reference_attachment'] = "fail";
	            $has_error  = true;
	        }


	    }
	    else{
	    	die("you did not upload anything. <a href='".base_url("employee/view/".$id)."'>click here to return</a>");
	    }


	    //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
        	//mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
	}

	public function add_training($data){

        //transaction start
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

        if(!empty($data['submitraining'])){
            $new_entry = array(
                'user_id'            	=> $data['old_uid'],
                'training_name'			=> $data['training_name'],
                'training_location'		=> $data['training_location'],
                'training_period'    	=> $data['training_period'],
                'date_created'          => __datenow(),
            );

            //insert step 1
            if(!$this->db->insert('reference_trainings', $new_entry)){
                $status['reference_trainings'] = "fail";
                $has_error  = true;
            }
        }

        // //audit_logs
        // $audit = array(
        //     'patient_id'            => $data['old_patient_id'],
        //     'user_id'               => $this->session->uid,
        //     'description'           => "New emergency contact for Resident ".$this->getPatientName($data['old_patient_id'])." by ".$this->session->fullname.".",
        //     'mini_description'      => "Resident emergency contact update made by ".$this->session->fullname.".",
        //     'date_created'          => __datenow(),
        // );

        // //insert step 1
        // if(!$this->db->insert('audit_logs', $audit)){
        //     $status['audit_logs'] = "fail";
        //     $has_error  = true;
        // }

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
            //mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
    }

    public function add_workexperience($data){

        //transaction start
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

        if(!empty($data['submitworkexperience'])){
            $new_entry = array(
                'user_id'			=> $data['old_uid'],
                'company'			=> $data['company_name'],
                'salary'			=> $data['salary'],
                'reason_left'    	=> $data['reason_left'],
                'date_created'		=> __datenow(),
            );

            //insert step 1
            if(!$this->db->insert('reference_work_experience', $new_entry)){
                $status['reference_work_experience'] = "fail";
                $has_error  = true;
            }
        }

        // //audit_logs
        // $audit = array(
        //     'patient_id'            => $data['old_patient_id'],
        //     'user_id'               => $this->session->uid,
        //     'description'           => "New emergency contact for Resident ".$this->getPatientName($data['old_patient_id'])." by ".$this->session->fullname.".",
        //     'mini_description'      => "Resident emergency contact update made by ".$this->session->fullname.".",
        //     'date_created'          => __datenow(),
        // );

        // //insert step 1
        // if(!$this->db->insert('audit_logs', $audit)){
        //     $status['audit_logs'] = "fail";
        //     $has_error  = true;
        // }

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
            //mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
    }

	public function add_dependent($data){

        //transaction start
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

        if(!empty($data['submitdependent'])){
            $new_entry = array(
                'user_id'            	=> $data['old_uid'],
                'dependent_name'		=> $data['dependent_name'],
                'dependent_relation'	=> $data['dependent_relation'],
                'dependent_remarks'     => $data['dependent_remarks'],
                'date_created'          => __datenow(),
            );

            //insert step 1
            if(!$this->db->insert('reference_dependents', $new_entry)){
                $status['reference_dependents'] = "fail";
                $has_error  = true;
            }
        }

        // //audit_logs
        // $audit = array(
        //     'patient_id'            => $data['old_patient_id'],
        //     'user_id'               => $this->session->uid,
        //     'description'           => "New emergency contact for Resident ".$this->getPatientName($data['old_patient_id'])." by ".$this->session->fullname.".",
        //     'mini_description'      => "Resident emergency contact update made by ".$this->session->fullname.".",
        //     'date_created'          => __datenow(),
        // );

        // //insert step 1
        // if(!$this->db->insert('audit_logs', $audit)){
        //     $status['audit_logs'] = "fail";
        //     $has_error  = true;
        // }

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
            //mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
    }



	public function save_employeeinfo($data,$update=0){
		// die(var_dump($data));
        //transaction start
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

		//upload attachment first
		$upload = $data['upload']['img_preview'];
		$photo_dir = "";
		if(!empty($upload['tmp_name'])){
            $ext            = pathinfo(@$upload['name'], PATHINFO_EXTENSION);
		    $newfilename    = date('ymdhsi_').md5(@$upload['name'].rand(0,100)).".".$ext;
		    $output         = PHOTO_DIR.$newfilename;
		    //check if image
            // if(@getimagesize($upload['tmp_name']) !== false){
		    	if (move_uploaded_file($upload["tmp_name"], $output)){ 
			       $photo_dir = $newfilename;//pass the filename so we can save in db
			    }else{
			    	die("file upload in ins enroll form failed");
			    }
            // }
	    }

	    if($update == 0){
		    //if new register do this
	    	if($this->if_db($data['username'])){
	    		die("Username already exists");
	    	}
	    	if(empty($photo_dir)){
				$photo_dir = "img.png";
	    	}
	    }else{
	    	if(empty($photo_dir)){
	    		$photo_dir  = $this->getimg($data['id']);
	    	}
	    }

	    $admin = 0;
	    $staff = 0;
	    if($data['employee_type']==1){
	    	$staff = 1;

	    }elseif($data['employee_type']==2){
	    	$admin = 1;
	    	$staff = 1;
	    }


    	

	    if($update == 0){
			$pw = $this->passhash($data['password']);
	        $register_userdetails = array(
	            'username'				=> $data['username'],
	            'passhash'				=> $pw['password'],
	            'salt'            		=> $pw['salt'],
	            // 'email'					=> $data['email'],
	            'mobile'  				=> $data['contact'],
	            'fullname'				=> $data['first_name']." ".$data['last_name'],
	            'img'					=> $photo_dir,
	            'admin'					=> $admin,
	            'staff'					=> $staff,
	            'date_created'      	=> __datenow(),
	        );

	        //insert step 1
	        if(!$this->db->insert('user_account', $register_userdetails)){
	            $status['user_account'] = "fail";
	            $has_error  = true;
	        }

        	$new_uid = $this->db->insert_id();

	    }else{

	    	$this->db->set('fullname', $data['first_name']." ".$data['last_name']);
			$this->db->set('img', $photo_dir);
			$this->db->where('uid', $data['id']);
			$this->db->update('user_account'); 
        	$new_uid = $data['id'];


			$this->db->set('disabled', 1);
			$this->db->where('user_id', $data['id']);
			$this->db->update('employee_records'); 

	    }



        $register_employeedetails = array(
            'user_id'					=> $new_uid,
            'first_name'				=> $data['first_name'],
            'middle_name'				=> $data['middle_name'],
            'last_name'					=> $data['last_name'],
            'birth_date'				=> __date($data['birth_date'],"Y-m-d"),
            'birth_place'				=> $data['birth_place'],
            'height'					=> $data['height'],
            'weight'					=> $data['weight'],
            'complexion'				=> $data['complexion'],
            'home_address'				=> $data['home_address'],
            'contact'					=> $data['contact'],
            'gender'					=> $data['gender'],
            'civil_status'				=> $data['civil_status'],
            // 'spouse_name'				=> $data['spouse_name'],
            // 'spouse_address'			=> $data['spouse_address'],
            'tin_no'					=> $data['tin_no'],
            'nbi_no'					=> $data['nbi_no'],
            'police_clearance'			=> $data['police_clearance'],
            'med_cert'					=> $data['med_cert'],
            'elementary_name'			=> $data['elementary_name'],
            'elementary_period'			=> $data['elementary_period'],
            'elementary_location'		=> $data['elementary_location'],
            'highschool_name'			=> $data['highschool_name'],
            'highschool_period'			=> $data['highschool_period'],
            'highschool_location'		=> $data['highschool_location'],
            'college_name'				=> $data['college_name'],
            'college_course'			=> $data['college_course'],
            'college_location'			=> $data['college_location'],
            'date_created'      		=> __datenow(),
        );

        //insert step 1
        if(!$this->db->insert('employee_records', $register_employeedetails)){
            $status['employee_records'] = "fail";
            $has_error  = true;
        }

        $employee_id = $this->db->insert_id();

        if($update == 1){
        	$this->db->set('disabled', 1);
			$this->db->where('user_id', $data['id']);
			$this->db->update('employee_requirements'); 
        }

        if(empty($data['branch_assigned'])){
        	$data['branch_assigned'] = 0;
        }
        
        $register_employeerequirements = array(
            'user_id'				=> $new_uid,
            'employee_code'			=> $data['employee_code'],
            'employee_type'			=> $data['employee_type'],
            'company_assigned'		=> @$data['branch_assigned'],
            // 'company_branch'		=> $data['company_branch'],
            'bank_account'			=> $data['account_no'],
            'philhealth'			=> $data['philhealth_no'],
            'sss'					=> $data['sss_no'],
            'pagibig'				=> $data['pagibig_no'],
            'date_created'			=> __datenow(),
        );



        //insert step 1
        if(!$this->db->insert('employee_requirements', $register_employeerequirements)){
            $status['employee_requirements'] = "fail";
            $has_error  = true;
        }

        foreach (@$data['wx_company'] as $key => $value) {
        	if(empty($value)){
        		continue;
        	}

        	$work_experience = array(
	            'user_id'				=> $new_uid,
	            'company'				=> @$data['wx_company'][$key],
	            'salary'				=> @$data['wx_salary'][$key],
	            'reason_left'			=> @$data['wx_reason'][$key],
	            'date_created'			=> __datenow(),
	        );

	        //insert step 1
	        if(!$this->db->insert('reference_work_experience', $work_experience)){
	            $status['reference_work_experience'] = "fail";
	            $has_error  = true;
	        }
        }

        foreach (@$data['dep_name'] as $key => $value) {
        	if(empty($value)){
        		continue;
        	}

        	$reference_dependents = array(
	            'user_id'			=> $new_uid,
	            'dependent_name'		=> @$data['dep_name'][$key],
	            'dependent_relation'	=> @$data['dep_relation'][$key],
	            'dependent_remarks'		=> @$data['dep_remarks'][$key],
	            'date_created'			=> __datenow(),
	        );

	        //insert step 1
	        if(!$this->db->insert('reference_dependents', $reference_dependents)){
	            $status['reference_dependents'] = "fail";
	            $has_error  = true;
	        }
        }

        foreach (@$data['tr_name'] as $key => $value) {
        	if(empty($value)){
        		continue;
        	}

        	$reference_trainings = array(
	            'user_id'			=> $new_uid,
	            'training_name'			=> @$data['tr_name'][$key],
	            'training_location'		=> @$data['tr_location'][$key],
	            'training_period'  		=> @$data['tr_course'][$key],//name mistake but will not fix as too much work
	            'date_created'			=> __datenow(),
	        );

	        //insert step 1
	        if(!$this->db->insert('reference_trainings', $reference_trainings)){
	            $status['reference_trainings'] = "fail";
	            $has_error  = true;
	        }
        }

        foreach (@$data['ref_name'] as $key => $value) {
        	if(empty($value)){
        		continue;
        	}

        	$reference_character = array(
	            'user_id'				=> $new_uid,
	            'refererence_name'			=> @$data['ref_name'][$key],
	            'refererence_location'		=> @$data['ref_location'][$key],
	            'refererence_contact'  		=> @$data['ref_contact'][$key],
	            'date_created'				=> __datenow(),
	        );

	        //insert step 1
	        if(!$this->db->insert('reference_character', $reference_character)){
	            $status['reference_character'] = "fail";
	            $has_error  = true;
	        }
        }


        //audit_logs
        // $audit = array(
        //     'user_id'				=> $new_uid,
        //     'description'			=> "User ".$data['username']." created by ".$this->session->fullname.".",
        //     'mini_description'  	=> "User ".$data['username']." created by ".$this->session->fullname.".",
        //     'date_created'      	=> __datenow(),
        // );

        // //insert step 1
        // if(!$this->db->insert('audit_logs', $audit)){
        //     $status['audit_logs'] = "fail";
        //     $has_error  = true;
        // }
	    

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
        	//mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
	}

	public function disable_account($uid){
		$this->db->set('disabled', 1);
		$this->db->where('uid', $uid);
		$this->db->update('user_account'); 
	}

	public function delete_training($id){
		$this->db->set('disabled', 1);
		$this->db->where('tr_id', $id);
		$this->db->update('reference_trainings'); 
	}

	public function delete_dependent($id){
		$this->db->set('disabled', 1);
		$this->db->where('dep_id', $id);
		$this->db->update('reference_dependents'); 
	}

	public function delete_workexperience($id){
		$this->db->set('disabled', 1);
		$this->db->where('wx_id', $id);
		$this->db->update('reference_work_experience'); 
	}

	public function delete_city($id){
		$this->db->set('disabled', 1);
		$this->db->where('city_id', $id);
		$this->db->update('lookup_location'); 
	}

	public function delete_company($id){
		$this->db->set('disabled', 1);
		$this->db->where('company_id', $id);
		$this->db->update('lookup_company'); 
	}

	public function delete_branch($id){
		$this->db->set('disabled', 1);
		$this->db->where('branch_id', $id);
		$this->db->update('lookup_branch'); 
	}


	public function change_password($old_pass="",$new_pass,$confirm_pass,$target_uid=0){
		$uid = $this->session->uid;
		$adminmode=false;
		if(!empty($target_uid)){
			$uid = $target_uid;
			$adminmode=true;
		}

		if($new_pass != $confirm_pass){
			return 2;
		}
		$this->db->select('*');
		$this->db->from('user_account');
		$this->db->where('uid', $uid);
		$this->db->limit(1);
		$query 			= $this->db->get();  
		$result 		= $query->result_array();
		$passmatch 		= $this->passhash($old_pass,$result[0]['salt']);
		$new_salt		= $this->randomString();
		$newpassword 	= $this->passhash($new_pass,$new_salt);
		
		if($result[0]['passhash'] == $passmatch['password'] || $this->session->admin){
			
			if(!empty($newpassword)){
				$this->db->set('passhash', $newpassword['password']);
				$this->db->set('salt', $newpassword['salt']);
				$this->db->where('uid', $uid);
				$this->db->update('user_account'); 
				return  9;

				$description 		= "User [".$result[0]['fullname']."] changed their password";
				$mini_description 	= "[".$result[0]['fullname']."] changes password";
				if($adminmode){
					$description = "User [".$result[0]['fullname']."] password is changed by ".$this->session->fullname;
					$mini_description 	= "[".$result[0]['fullname']."] password changed by admin";
				}
				//audit_logs
		        $audit = array(
		            'user_id'				=> $result[0]['uid'],
		            'description'			=> $description,
		            'mini_description'  	=> $mini_description,
		            'date_created'      	=> __datenow(),
		        );

		        //insert step 1
		        // if(!$this->db->insert('audit_logs', $audit)){
		        //     $status['audit_logs'] = "fail";
		        //     $has_error  = true;
		        // }
			}
		}
		else{
			return 1;
		}

		return 0;
		
	}
	/*#####################
	0 - nothing happened
	1 - incorrect old password
	2 - confirm pass does not match
	9 - success
	#####################*/


	public function login($username,$password){
		$return = 0;
		if($this->if_db($username)){
			$this->db->select('*');
			$this->db->from('user_account a');
			$this->db->where('username',$username);
			$this->db->limit(1);

			$query = $this->db->get();  
			$result = $query->result_array();
			$passmatch = $this->passhash($password,$result[0]['salt']);
			if($result[0]['passhash'] == $passmatch['password']){

				if($result[0]['disabled'] == 1){
					return 3;
				}
				else{
					$this->set_session($result[0]); //start session
					return 9;
				}				
			}
			else{return 2;}
		}
		else{return 1;}
		/*#######################
		Return States
			0 - Nothing Happened
			1 - Username does not exist
			2 - Password is Incorrect
			3 - Account is Disabled
			9 - Login Successful
		#######################*/
	}


}