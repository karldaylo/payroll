<?php
class Generate_db extends CI_Model {
	//this model is about all CRUD access in account DB
	public function __construct(){
        parent::__construct();
        $this->load->database('default');
    }

	public function getPagIbig($y,$m,$d){
        $this->db->select('*');
        $this->db->from('attendance_table a');
        $this->db->join('employee_records b', 'b.user_id = a.user_id and b.disabled=0', 'left');
        $this->db->join('employee_requirements c', 'c.employee_id = b.employee_id and c.disabled=0', 'left');
        $this->db->where('att_month',$m);
        $this->db->where('att_year',$y);
        $this->db->where('a.disabled',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        $return  = "";
        foreach ($result as $key => $row) {
            $return .= str_pad($row['pagibig']          , 29  , " ");
            $return .= str_pad($row['first_name']       , 30  , " ");
            $return .= str_pad($row['middle_name']      , 30  , " ");
            $return .= str_pad($row['last_name']        , 30  , " ");
            $return .= str_pad(number_format($row['pagibig_deduction'],2)        , 13  , " ", STR_PAD_LEFT);
            $return .= str_pad(number_format($row['pagibig_deduction'],2)        , 13  , " ", STR_PAD_LEFT);
            $return .= str_pad(__date($row['birth_date'],"Ymd")   , 23  , " ", STR_PAD_LEFT)."\r\n";
        }
        return $return;
    }

    public function getPayroll($y,$m,$d){
        $this->db->select('*');
        $this->db->from('attendance_table a');
        $this->db->join('employee_records b', 'b.user_id = a.user_id and b.disabled=0', 'left');
        $this->db->join('employee_requirements c', 'c.employee_id = b.employee_id and c.disabled=0', 'left');
        $this->db->where('att_date',$d);
        $this->db->where('att_month',$m);
        $this->db->where('att_year',$y);
        $this->db->where('a.disabled',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        $return  = "";
        foreach ($result as $key => $row) {
            //D|100058022320|1|000000492158|EMPLOYEE NAME:Parado John Rey
            $newformat = number_format($row['total_wage'],2,".","") *100;
            $return .= "D|".$row['bank_account']."|1|".$newformat."|EMPLOYEE NAME:".$row['first_name']." ".$row['middle_name']." ".$row['last_name']."\n";
            
        }
        return $return;
    }

    public function getPhilHealth($y,$m,$d){
        $this->db->select('*');
        $this->db->from('attendance_table a');
        $this->db->join('employee_records b', 'b.user_id = a.user_id and b.disabled=0', 'left');
        $this->db->join('employee_requirements c', 'c.employee_id = b.employee_id and c.disabled=0', 'left');
        $this->db->where('att_month',$m);
        $this->db->where('att_year',$y);
        $this->db->where('a.disabled',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        $return  = "";
        foreach ($result as $key => $row) {
            $return .= str_pad($row['philhealth']       , 29  , " ");
            $return .= str_pad($row['first_name']       , 30  , " ");
            $return .= str_pad($row['middle_name']      , 30  , " ");
            $return .= str_pad($row['last_name']        , 30  , " ");
            $return .= str_pad(number_format($row['philhealth_deduction'],2)        , 13  , " ", STR_PAD_LEFT);
            $return .= str_pad(number_format($row['philhealth_deduction'],2)        , 13  , " ", STR_PAD_LEFT);
            $return .= str_pad(__date($row['birth_date'],"Ymd")   , 23  , " ", STR_PAD_LEFT)."\r\n";
        }
        return $return;
    }

    public function getSSS($y,$m,$d){
        $this->db->select('*');
        $this->db->from('attendance_table a');
        $this->db->join('employee_records b', 'b.user_id = a.user_id and b.disabled=0', 'left');
        $this->db->join('employee_requirements c', 'c.employee_id = b.employee_id and c.disabled=0', 'left');
        $this->db->where('att_month',$m);
        $this->db->where('att_year',$y);
        $this->db->where('a.disabled',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        $return  = "";
        foreach ($result as $key => $row) {
            $return .= str_pad($row['sss']              , 29  , " ");
            $return .= str_pad($row['first_name']       , 30  , " ");
            $return .= str_pad($row['middle_name']      , 30  , " ");
            $return .= str_pad($row['last_name']        , 30  , " ");
            $return .= str_pad(number_format($row['sss_deduction'],2)        , 13  , " ", STR_PAD_LEFT);
            $return .= str_pad(number_format($row['sss_deduction'],2)        , 13  , " ", STR_PAD_LEFT);
            $return .= str_pad(__date($row['birth_date'],"Ymd")   , 23  , " ", STR_PAD_LEFT)."\r\n";
        }
        return $return;
    }



   


}