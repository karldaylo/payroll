<?php
class Attendance_db extends CI_Model {
	//this model is about all CRUD access in account DB
	
	public function __construct(){
		parent::__construct();
		$this->load->database('default');
	}


    public function save_attendance($data){
        // die(var_dump($data));
        //transaction start
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;
         
        $this->load->model('account_db');

        $uid        = $data["user_id"];
        $att_month  = $data["target_month"];
        $att_date   = $data["target_semi"];
        $att_year   = $data["target_year"];
            
        $reference = $this->account_db->get_user_details_calc($uid);

        if(!empty($reference)){

            $attendance = $this->account_db->getUserAttendance($uid,$att_month,$att_date,$att_year);
            
            if(!empty($attendance[0])){
                $disable_id=$attendance[0]['attend_id']; 
                //disable now
                $this->db->set('disabled', 1);
                $this->db->where('attend_id', $disable_id);
                $this->db->update('attendance_table'); 

                $this->db->set('disabled', 1);
                $this->db->where('attend_id', $disable_id);
                $this->db->update('attendance_others'); 
            }

            if($att_date == 2){
                $previous_attendance = $this->account_db->getUserAttendance($uid,$att_month,1,$att_year);
                $reference['previous_attendance']= false;
                if(!empty($previous_attendance)){
                    $reference['previous_attendance'] = $previous_attendance[0];
                }
            }
            
            if($reference['employee_type'] == 313){
                $reference['rate'] = rate_313();
            }elseif($reference['employee_type'] == 310){
                $reference['rate'] = rate_310();
            }elseif($reference['employee_type'] == 258){
                $reference['rate'] = rate_258();
            }
        }

        //set zero if empty
        foreach ($reference['rate'] as $key => $val) {
            if(empty($val)){
                $reference['rate'][$key] = $val;
            }
        }

        $semi_month                = $reference['rate']['semi_month'];
        $daily_wage                = $reference['rate']['daily_wage'];
        $hourly_rate               = $reference['rate']['hourly_rate'];
        $legal_holiday             = $reference['rate']['legal_holiday'];
        $ot_legal_holiday          = $reference['rate']['ot_legal_holiday'];
        $ot_rate                   = $reference['rate']['ot_rate'];
        $ot_restday_regular_holiday= $reference['rate']['ot_restday_regular_holiday'];
        $ot_special_holiday        = $reference['rate']['ot_special_holiday'];
        $restday_regular_holiday   = $reference['rate']['restday_regular_holiday'];
        $special_holiday           = $reference['rate']['special_holiday'];
        $ut_rate                   = $reference['rate']['ut_rate'];
        $vl_rate                   = $reference['rate']['vl_rate'];
        $nd_rate                   = $reference['rate']['nd_rate'];

        //set zero if empty
        $target_zero = ['total_days','total_ut','total_ot','legal_holiday','legal_holiday_ot','special_holiday','special_holiday_ot','rest_days','rest_days_ot','pagibig_deduc','philhealth_deduc','sss_deduc','salary_adjustment','night_diff'];
        foreach($target_zero as $key){
            if(empty($data[$key])){
                $data[$key] = 0;
            }
        }

        $total_days       = $data["total_days"];
        $total_ut         = $data["total_ut"];
        $total_ot         = $data["total_ot"];
        $total_legal_hd   = $data["legal_holiday"];
        $total_legal_hd_ot= $data["legal_holiday_ot"];
        $total_special_hd = $data["special_holiday"];
        $total_special_hd_ot = $data["special_holiday_ot"];
        $total_rest_hd    = $data["rest_days"];
        $total_rest_hd_ot = $data["rest_days_ot"];
        $pagibig_deduc    = $data["pagibig_deduc"];
        $philhealth_deduc = $data["philhealth_deduc"];
        $sss_deduc        = $data["sss_deduc"];
        $salary_adjustment= $data["salary_adjustment"];
        $night_diff       = $data["night_diff"];

        $display_regular  = $semi_month;
        $display_ot       = $total_ot * $ot_rate;
        $display_legal_hd = ($legal_holiday * $total_legal_hd) + ($ot_legal_holiday * $total_legal_hd_ot);
        $display_rest_hd  = ($restday_regular_holiday * $total_rest_hd) + ($ot_restday_regular_holiday * $total_rest_hd_ot);
        $display_special_hd = ($special_holiday * $total_special_hd) + ($ot_special_holiday * $total_special_hd_ot);
        $display_night_diff = $night_diff * $nd_rate;


        $display_gross    = $display_regular + $display_ot + $display_legal_hd + $display_rest_hd + $display_special_hd + $display_night_diff;

        //deduction
        $display_absent   = $total_days * $daily_wage;
        $display_ut       = $total_ut * $ut_rate;

        $display_deduction = $display_absent + $display_ut + $sss_deduc + $pagibig_deduc + $philhealth_deduc;

        //others
        $display_etc=0;

        if(!empty($data['other_price'])){
            foreach ($data['other_price'] as $val) {
                $display_etc=$display_etc+$val; 
            }
        }
        
        $display_others   = $salary_adjustment+$display_etc;

        $display_total    = $display_gross-$display_deduction+$display_others;

        $register_attendance_date = array(
            'user_id'               => $data['user_id'],
            'work_factor'           => $reference['employee_type'],
            'att_month'             => $data['target_month'],
            'att_date'              => $data['target_semi'],
            'att_year'              => $data['target_year'],
            'total_absent'          => $data['total_days'],
            'total_ut'              => $data['total_ut'],
            'total_ot'              => $data['total_ot'],
            'vl_incentive'          => $data['total_vl'],
            'total_night_diff'      => $night_diff,
            // 'total_night_diff_ot'   => $data['night_diff_OT'],
            'total_legal_holiday'   => $data['legal_holiday'],
            'total_legal_holiday_ot'=> $data['legal_holiday_ot'],
            'total_special_holiday' => $data['special_holiday'],
            'total_special_holiday_ot' => $data['special_holiday_ot'],
            'total_rest_holiday'    => $data['rest_days'],
            'total_rest_holiday_ot' => $data['rest_days_ot'],
            'pagibig_deduction'     => $data['pagibig_deduc'],
            'philhealth_deduction'  => $data['philhealth_deduc'],
            'sss_deduction'         => $data['sss_deduc'],
            'salary_adjustment'     => $data['salary_adjustment'],
            'total_wage'            => number_format($display_total,4),
            'created_by'            => $this->session->username,
            'date_created'          => __datenow(),
        );

        $attendance_id = 0;

        //insert step 1
        if(!$this->db->insert('attendance_table', $register_attendance_date)){
            $status['attendance_table'] = "fail";
            $has_error  = true;
        }else{
            $attendance_id = $this->db->insert_id();
        }

        if(!empty($data['other_price'])){
            foreach ($data['other_price'] as $key => $value) {
                if(empty($value)){
                    continue;
                }
                if(empty($data['other_description'][$key])){
                    $data['other_description'][$key] = "No description";
                }
                $register_attendance_others = array(
                    'attend_id'               => $attendance_id,
                    'amount'        => $value,
                    'description'   => $data['other_description'][$key],
                    'date_created'  => __datenow(),
                );

                //insert step 1
                if(!$this->db->insert('attendance_others', $register_attendance_others)){
                    $status['attendance_others'] = "fail";
                }
            }
        }
        
        // $new_uid = $this->db->insert_id();


        // //audit_logs
        // $audit = array(
        //     'user_id'               => $new_uid,
        //     'description'           => "User ".$data['username']." created by ".$this->session->fullname.".",
        //     'mini_description'      => "User ".$data['username']." created by ".$this->session->fullname.".",
        //     'date_created'          => __datenow(),
        // );

        // //insert step 1
        // if(!$this->db->insert('audit_logs', $audit)){
        //     $status['audit_logs'] = "fail";
        //     $has_error  = true;
        // }
        
        

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
            //mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
    }

    public function get_attendance_list($id,$start_date,$end_date){
        $this->db->select('*');
        $this->db->from('attendance_table');
        $this->db->where('disabled',0);
        $this->db->where('att_date >=',$start_date);
        $this->db->where('att_date <=',$end_date);
        $this->db->order_by('att_date','asc');
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
    }

    public function get_attendance_detail($attendance_id){
        $this->db->select('*');
        $this->db->from('attendance_table');
        $this->db->where('disabled',0);
        $this->db->where('att_date >=',$start_date);
        $this->db->where('att_date <=',$end_date);
        $this->db->order_by('att_date','asc');
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
    }

    private function if_db($value,$table = 'attendance_table',$field = 'att_date'){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($field,$value);
        $this->db->limit(1);
        return ($this->db->count_all_results() > 0)?true:false;
    }


}