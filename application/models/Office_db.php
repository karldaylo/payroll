<?php
class Office_db extends CI_Model {
	//this model is about all CRUD access in account DB
	public function __construct(){
        parent::__construct();
        $this->load->database('default');
    }

    

    public function get_company_list(){

        $result = [];
        $this->db->select("*");
        $this->db->where("disabled",0);
        $this->db->from('lookup_company');
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            return $result;
        }
        return $result;
    }

    public function get_location_list(){

        $result = [];
        $this->db->distinct();
        $this->db->select("*");
        $this->db->where("disabled",0);
        $this->db->from('lookup_location');
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            return $result;
        }
        return $result;
    }

    public function get_branch_list(){

        $result = [];
        $this->db->select("*");
        $this->db->from('lookup_branch a');
        $this->db->join('lookup_location b','b.city_id = a.city_id and b.disabled=0', 'left');
        $this->db->join('lookup_company c','c.company_id = a.company_id and b.disabled=0', 'left');
        $this->db->where("a.disabled",0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            return $result;
        }
        return $result;
    }

    public function create_company($data){
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

        $register_company = array(
            'company_name'    => $data['company_name'],
            'disabled'       => 0,
        );

        //insert step 1
        if(!$this->db->insert('lookup_company', $register_company)){
            $status['reference_attachment'] = "fail";
            $has_error  = true;
        }

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
            //mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
    }

    public function create_city($data){
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

        $register_city = array(
            'city_name'    => $data['city_name'],
            'disabled'       => 0,
        );

        //insert step 1
        if(!$this->db->insert(' lookup_location', $register_city)){
            $status['reference_attachment'] = "fail";
            $has_error  = true;
        }

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
            //mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
    }

    public function create_office($data){
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

        $register_city = array(
            'branch_name'   => $data['office_name'],
            'company_id'    => $data['company_assigned'],
            'city_id'       => $data['city_assigned'],
            'disabled'      => 0,
        );

        //insert step 1
        if(!$this->db->insert('lookup_branch', $register_city)){
            $status['reference_attachment'] = "fail";
            $has_error  = true;
        }

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
            //mailme newly registered user
            $return['error']         = 0;  
            $return['status']       = 'success';  

        }
        return $return;
    }


}