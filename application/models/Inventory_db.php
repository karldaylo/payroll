<?php
class Inventory_db extends CI_Model {
	//this model is about all CRUD access in account DB
	
	public function __construct(){
		parent::__construct();
		$this->load->database('default');
	}

	public function get_inventory_list($limit=0,$page=0){
		$this->db->select('a.item_id');
        $this->db->select('b.unit_name');
        $this->db->select('b.unit_brand');
        $this->db->select('b.unit_description');
        $this->db->select('b.unit_price');
		$this->db->from('item_list a');
    	$this->db->join('item_description b', 'b.item_id = a.item_id and b.disabled=0', 'left');
    	$this->db->order_by('b.last_modified', 'DESC');
		if($limit>0){
			// if($page == 0){
			// 	$page = 1;
			// }
            // $offset = ($page - 1) * $limit;
			$offset = $page * $limit;
			$this->db->limit($limit,$offset);
		}
		$query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
        	foreach ($result as $key => $row) {
                $result[$key]['unit_code']          = __itemid($result[$key]['item_id']);
	        	$result[$key]['unit_total'] 		= $this->getItemTotal($result[$key]['item_id']);
        	}

        }

        return $result;
	}

    public function search_inventory($filter){

        $this->db->select('a.item_id');
        $this->db->select('b.unit_name');
        $this->db->select('b.unit_brand');
        $this->db->select('b.unit_price as price');
        $this->db->from('item_list a');
        $this->db->join('item_description b', 'b.item_id = a.item_id and b.disabled=0', 'left');
        $this->db->group_start();
        $this->db->like('unit_name',$filter,"BOTH");
        $this->db->or_like('unit_brand',$filter,"BOTH");
        $this->db->group_end();
        $this->db->where('a.archived',0);
        $this->db->limit("10");
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            foreach ($result as $key => $row) {
                $brand = "";
                if($row['unit_brand']){
                    $brand = " (".$row['unit_brand'].")";
                }
                $result[$key]['label']          = $row['unit_name'].$brand;
                $result[$key]['id']             = $row['item_id'];
            }

        }
        return $result;
    }

    public function search_autocomplete($filter){

        $this->db->distinct();
        $this->db->select("unit_name as label");
        $this->db->from('item_description');
        $this->db->like('unit_name',$filter,"BOTH");
        $this->db->limit("10");
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            return $result;

        }
        return $result;
    }

    public function omni_item_search($filter=0){
        $this->db->select('a.item_id');
        $this->db->select('b.unit_name');
        $this->db->select('b.unit_brand');
        $this->db->select('b.unit_description');
        $this->db->select('b.unit_price');
        $this->db->from('item_list a');
        $this->db->join('item_description b', 'b.item_id = a.item_id and b.disabled=0', 'left');

        $this->db->like('b.unit_name', $filter);
        $this->db->or_like('b.unit_description', $filter);
        $this->db->or_like('b.unit_brand', $filter);
        $this->db->or_like('b.unit_usage', $filter);
        $this->db->or_like('b.unit_category', $filter);

        $this->db->order_by('b.last_modified', 'DESC');
       
        $this->db->limit(100);

        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
            foreach ($result as $key => $row) {
                $result[$key]['unit_code']          = __itemid($result[$key]['item_id']);
                $result[$key]['unit_total']         = $this->getItemTotal($result[$key]['item_id']);
            }

        }

        return $result;
    }


	public function get_inventory_details($item_id){
		$this->db->select('*');
		$this->db->from('item_list a');
    	$this->db->join('item_description b', 'b.item_id = a.item_id and b.disabled=0', 'left');
		$this->db->where('a.item_id',$item_id);
		$this->db->limit(1);
		$query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty($result[0])){
        	$result = $result[0];
            $result['unit_code']        = __itemid($result['item_id']);
        	$result['item_total'] 		= $this->getItemTotal($result['item_id']);
        	$result['logs'] 			= $this->getItemLogs($result['item_id']);
        }
        // die(var_dump($result));
        return $result;
	}

    public function get_dashboardinventory(){
        $this->db->select('COUNT(item_id) as total');
        $this->db->from('item_list');
        $this->db->where('archived',0);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['total'])){
            return $result[0]['total'];
        }else{
            return 0;
        }
    }


	private function getItemTotal($item_id){
		$this->db->select('SUM(quantity) as total');
		$this->db->from('item_count');
		$this->db->where('item_id',$item_id);
		$this->db->where('disabled',0);
		// $this->db->group_start();
		// $this->db->where('method','ADD');
		// $this->db->or_where('method','RELEASED');
		// $this->db->group_end();
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0]['total'])){
            return $result[0]['total'];
        }else{
            return 0;
        }
	}

	private function getItemLogs($item_id){
		$this->db->select('*');
		$this->db->from('audit_logs');
		$this->db->where('item_id',$item_id);
		$this->db->where('disabled',0);
		$this->db->order_by('date_created','desc');
		$query   = $this->db->get();
        $result  = $query->result_array();
		if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
	}

	private function if_db($value,$table = 'item_list',$field = 'item_id'){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where($field,$value);
		$this->db->limit(1);
		return ($this->db->count_all_results() > 0)?true:false;
	}

	public function increase_inventory($data,$item_id){
		//transaction start
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

		$register_count = array(
            'item_id'          		=> $item_id,
            'quantity'				=> $data['add_qty'],
            'method'            	=> "ADD",
            'origin'            	=> $data['add_origin'],
            'entry_remark'          => $data['add_remarks'],
            'created_by'           	=> $this->session->uid,
            'date_created'      	=> __datenow(),
        );

        //insert step 1
        if(!$this->db->insert('item_count', $register_count)){
            $status['item_count'] = "fail";
            $has_error  = true;
        }

        //audit_logs
        $audit = array(
            'item_id'          		=> $item_id,
            'user_id'				=> $this->session->uid,
            'description'			=> "Inventory [<b>".__itemid($item_id)."</b>] stock increased by ".$this->session->fullname.".",
            'mini_description'  	=> "Inventory [<b>".__itemid($item_id)."</b>] restocked",
            'date_created'      	=> __datenow(),
        );

        //insert step 1
        if(!$this->db->insert('audit_logs', $audit)){
            $status['audit_logs'] = "fail";
            $has_error  = true;
        }

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
        	//mailme newly registered user
            $return['error']         = 0;  
            $return['status']        = 'success';  

        }
        return $return;
	} 


	public function release_inventory($data,$item_id){
		//transaction start
        $this->db->trans_start();
        

        //set value to 1 or true if force to error
        $has_error = false;

		$register_count = array(
            'item_id'          		=> $item_id,
            'quantity'				=> "-".$data['release_qty'],
            'method'            	=> "RELEASED",
            'entry_remark'          => $data['release_remarks'],
            'created_by'           	=> $this->session->uid,
            'date_created'      	=> __datenow(),
        );

        //insert step 1
        if(!$this->db->insert('item_count', $register_count)){
            $status['item_count'] = "fail";
            $has_error  = true;
        }

        $new_auditid = $this->db->insert_id();

        

        //audit_logs
        $audit = array(
            'item_id'          		=> $item_id,
            'user_id'				=> $this->session->uid,
            'description'			=> "Inventory [<b>".__itemid($item_id)."</b>] QTY ".$data['release_qty']." released by ".$this->session->fullname.".",
            'mini_description'  	=> "Inventory [<b>".__itemid($item_id)."</b>] released to ".$temp_name,
            'date_created'      	=> __datenow(),
        );

        //insert step 1
        if(!$this->db->insert('audit_logs', $audit)){
            $status['audit_logs'] = "fail";
            $has_error  = true;
        }

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
        	//mailme newly registered user
            $return['error']         = 0;  
            $return['status']        = 'success';  

        }
        return $return;
	} 

	public function save_item($data,$update=0){
        //transaction start
        $this->db->trans_start();

        //set value to 1 or true if force to error
        $has_error = false;

		//upload attachment first
		$upload = $data['upload']['img_preview'];
		$photo_dir = "";
		if(!empty($upload['tmp_name'])){
            $ext            = pathinfo(@$upload['name'], PATHINFO_EXTENSION);
		    $newfilename    = date('ymdhsi_').md5(@$upload['name'].rand(0,100)).".".$ext;
		    $output         = UPLOAD_DIR.$newfilename;
		    //check if image
            // if(@getimagesize($upload['tmp_name']) !== false){
		    	if (move_uploaded_file($upload["tmp_name"], $output)){ 
			       $photo_dir = $newfilename;//pass the filename so we can save in db
			    }else{
			    	die("file upload in ins enroll form failed");
			    }
            // }
	    }

	    //if new register do this
	    if($update == 0){
	    	
            //placeholder if no image
            if(empty($photo_dir)){
                $photo_dir = "img.png";
            }

	    	$register_item = array(
	            'created_by'			=> $this->session->username,
	            'date_created'      	=> __datenow(),
	        );

	        //insert step 1
	        if(!$this->db->insert('item_list', $register_item)){
	            $status['item_list'] = "fail";
	            $has_error  = true;
	        }

	        $new_itemid = $this->db->insert_id();

	        $register_itemdetails = array(
	            'item_id'          		=> $new_itemid,
	            'unit_name'				=> $data['unit_name'],
                'unit_brand'            => @$data['unit_brand'],
                'unit_description'      => @$data['unit_desc'],
	            'unit_usage'            => @$data['unit_usage'],
	            'unit_category'			=> @$data['unit_category'],
	            'unit_price'			=> @$data['unit_price'],
                'unit_img'              => @$photo_dir,
	            'modified_by'		    => $this->session->username,
	            'last_modified'      	=> __datenow(),
	        );

	        //insert step 
	        if(!$this->db->insert('item_description', $register_itemdetails)){
	            $status['item_description'] = "fail";
	            $has_error  = true;
	        }

            if($data['unit_qty']>0){
                $register_count = array(
                    'item_id'               => $new_itemid,
                    'quantity'              => $data['unit_qty'],
                    'method'                => "ADD",
                    'origin'                => $data['unit_origin'],
                    'entry_remark'          => $data['unit_remarks'],
                    'created_by'            => $this->session->uid,
                    'date_created'          => __datenow(),
                );

                //insert step 1
                if(!$this->db->insert('item_count', $register_count)){
                    $status['item_count'] = "fail";
                    $has_error  = true;
                }  
            }
	        

	        //audit_logs
	        $audit = array(
	            'item_id'          		=> $new_itemid,
	            'user_id'				=> $this->session->uid,
	            'description'			=> "Created inventory [<b>".$data['unit_name']."</b>-".__itemid($new_itemid)."] by ".$this->session->fullname.".",
	            'mini_description'  	=> "Created inventory [<b>".$data['unit_name']."</b>].",
	            'date_created'      	=> __datenow(),
	        );

	        //insert step 1
	        if(!$this->db->insert('audit_logs', $audit)){
	            $status['audit_logs'] = "fail";
	            $has_error  = true;
	        }

	    }
	    else{
	    	if(!$this->if_db($data['old_id'])){
                die("ERROR: inventory code does not exists ".__itemid($data['old_id']));
            }

            $new_itemid = $data['old_id'];

            if(empty($photo_dir)){
                $photo_dir  = $this->getimg($new_itemid);

            }

            //update disabled item_description
            $this->db->set('disabled', 1);
            $this->db->where('item_id', $new_itemid);
            if(!$this->db->update('item_description')){
                $status['item_description'] = "fail";
                $has_error  = true;
            }

            $register_itemdetails = array(
                'item_id'               => $new_itemid,
                'unit_name'             => $data['unit_name'],
                'unit_brand'            => @$data['unit_brand'],
                'unit_description'      => @$data['unit_desc'],
                'unit_usage'            => @$data['unit_usage'],
                'unit_category'         => @$data['unit_category'],
                'unit_price'            => @$data['unit_price'],
                'unit_img'              => @$photo_dir,
                'modified_by'           => $this->session->username,
                'last_modified'         => __datenow(),
            );

            //insert step 1
            if(!$this->db->insert('item_description', $register_itemdetails)){
                $status['item_description'] = "fail";
                $has_error  = true;
            }


            //audit_logs
            $audit = array(
                'item_id'               => $new_itemid,
                'user_id'               => $this->session->uid,
                'description'           => "Updated inventory description for [".__itemid($new_itemid)."] <b>".$data['unit_name']." ".__brand($data['unit_brand'])."</b> by ".$this->session->fullname.".",
                'mini_description'      => "Edited inventory [<b>".$data['unit_name']."</b>].",
                'date_created'          => __datenow(),
            );

            //insert step 
            if(!$this->db->insert('audit_logs', $audit)){
                $status['audit_logs'] = "fail";
                $has_error  = true;
            }
	    }
        

        //transaction end
        if($has_error){
            $this->db->trans_rollback();
        }else{
            $this->db->trans_complete();
        }

        //error handling
        if ($has_error) {
            $return['error']        = 1;
            $return['status']       = "error";
            $return['err_msg']      = "There was an error in the Database: ".$this->db->_error_message(); 
            $return['status_array'] = json_encode($status); 
            $return['err_no']       = $this->db->_error_number(); 
        }
        else {
        	//mailme newly registered user
            $return['error']         = 0;  
            $return['item_id']       = $new_itemid;  
            $return['status']        = 'success';  

        }
        return $return;
	}

    private function checkstatus($appoint_id){
        $this->db->select('*');
        $this->db->from('item_appointment');
        $this->db->where('appoint_id',$appoint_id);
        $this->db->limit(1);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['status'])){
            return strtolower($result[0]['status']);
        }else{
            die('status error');
        }
    }

    public function get_logs_list($limit=0,$page=0){
        $this->db->select('*');
        $this->db->from('audit_logs');
        $this->db->where('disabled',0);
        $this->db->order_by('date_created','desc');
        $offset = 0;
        if($page>0){
            $offset = $page * $limit;
        }
        $this->db->limit($limit,$offset);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0])){
            return $result;
        }else{
            return [];
        }
    }

    private function getimg($item_id){
        $this->db->select('*');
        $this->db->from('item_description');
        $this->db->where("item_id",$item_id);
        $this->db->where("disabled",0);
        $this->db->limit(1);
        $query   = $this->db->get();
        $result  = $query->result_array();
        if(!empty(@$result[0]['unit_img'])){
            return $result[0]['unit_img'];
        }else{
            return 0;
        }
    }

    public function enable_inventory($id){

        $this->db->set('archived'        , 0);
        $this->db->where('item_id'       , $id);
        if(!$this->db->update('item_list')){
            $status['item_list'] = "fail";
            $status['error'] = true;
        }else{
            $status['error'] = false;
        }
        return $status;

    }

    public function disable_inventory($id){

        $this->db->set('archived'        , 1);
        $this->db->where('item_id'       , $id);
        if(!$this->db->update('item_list')){
            $status['item_list'] = "fail";
            $status['error'] = true;
        }else{
            $status['error'] = false;
        }
        return $status;

    }


}