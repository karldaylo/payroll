<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form method="POST" action="" enctype="multipart/form-data">
<div class="container  dashboard-container">
  <div class="text-center" style="width:100%;"><h2>Employee Record</h2></div>
  
  <fieldset>
    <legend><h3>Login Information</h3></legend>

    <div class="form-group col-md-6">
      <div class="form-group col-md-12">
        <label for="employee_type" class="control-label">Employee Type</label> 
        <select required class="form-control "  name="employee_type" id="employee_type">
          <option value="313">Employee: 313 days factor</option>
          <option value="310">Employee: 310 days factor</option>
          <option value="258">Employee: 258 days factor</option>
          <?php
          if($this->session->admin){
            echo '<option value="1">Staff</option>
            <option value="2">Administration</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <?php
      echo bootstrap_form("employee_code",      "Employee ID",                 "",array("req"=>true,"class"=>"col-md-6"));
      echo bootstrap_form("username",           "Username",                    "",array("req"=>true,"class"=>"col-md-12"));
      echo bootstrap_form("password",           "Password (default at 1234)",  "1234",array("req"=>true,"class"=>"col-md-12","type"=>"password"));
      echo bootstrap_form("confirm_password",   "Confirm Password",            "1234",array("req"=>true,"class"=>"col-md-12","type"=>"password"));
    ?>
     
    <script type="text/javascript">
      function validate_username(){
        $has_error = 0;
        $("#username").parent().parent().removeClass('has-error');
        $("#username_extra").text("");
        $str = $("#username").val();
        $str = $str.replace(/[^a-zA-Z0-9-._@]/g, "");
        $("#username").val($str);
        // $exists = check_exist();
        // if($exists == 1){
        //   $has_error++;
        //   $("#username").parent().parent().addClass('has-error');
        //   $("#username_extra").text("(username already exists)");
        // }
        if($("#username").val().length < 5){
          $has_error++;
          $("#username").parent().parent().addClass('has-error');
          $("#username_extra").text("(minimum of 5 characters)");
        }

        return $has_error;
      }
      function validate_password(){
        $has_error = 0;
        $("#confirm_password").parent().parent().removeClass('has-error');
        $("#confirm_password_extra").text("");
        if($("#confirm_password").val()!= $("#password").val()){
          $has_error++;
          $("#confirm_password").parent().parent().addClass('has-error');
          $("#confirm_password_extra").text("(password not match)");
        }

        return $has_error;
      }

      $("#confirm_password").blur(function(){
        validate_password();
      });

      $("#username").blur(function(){
        validate_username();
      });
      $("#username").change(function(){
        validate_username();
      });

    </script>
  </fieldset>
</div>

<div class="container  dashboard-container">
  <fieldset>
    <legend><h3>Employee Personal Information</h3></legend>
    <div class="col-sm-2">
      <img src="<?php echo base_url("/assets/img/img.png")?>" style="width:100%" id="output"/>
    </div>
    <div class="col-sm-10">
      <div class="form-group">
        <label for="attach_insenrollform" class="control-label">Photo (optional)</label> 
        <input  class="form-control padless attach-group" type="file" name="img_preview" id="img_preview" style="height:28px" accept="image/*" onchange="loadFile(event)">
      </div>
    </div>
    <div class="col-sm-12">
    </div>
    

    <?php
      echo bootstrap_form("first_name",        "First Name",    "",array("req"=>true,"class"=>"col-md-4","req"=>1));
      echo bootstrap_form("middle_name",       "Middle Name",   "",array("class"=>"col-md-4"));
      echo bootstrap_form("last_name",         "Last Name",     "",array("req"=>true,"class"=>"col-md-4","req"=>1));
      echo bootstrap_form("birth_date",        "Birth Date",    "",array("class"=>"col-md-6 dateonly"));
      echo bootstrap_form("birth_place",       "Birth Place",   "",array("class"=>"col-md-6"));
      
      ?>
      <div class="form-group col-md-6">
        <div class="form-group col-md-12">
          <label for="gender" class="control-label">Gender</label> 
          <select required class="form-control "  name="gender" id="gender">
            <option >Male</option>
            <option >Female</option>
          </select>
        </div>
      </div>
      <div class="form-group col-md-6">
        <div class="form-group col-md-12">
          <label for="civil_status" class="control-label">Civil Status</label> 
          <select required class="form-control "  name="civil_status" id="civil_status">
            <option >Single</option>
            <option >Married</option>
            <option >Divorced</option>
            <option >Separated</option>
            <option >Widowed</option>
            
          </select>
        </div>
      </div>
      <?php
      echo bootstrap_form("height",            "Height",        "",array("class"=>"col-md-6"));
      echo bootstrap_form("weight",            "Weight",        "",array("class"=>"col-md-6"));
      echo bootstrap_form("complexion",        "Complexion",    "",array("class"=>"col-md-6"));
      echo bootstrap_form("contact",  "Contact Number", "",array("class"=>"col-md-6"));
      echo bootstrap_form_textarea("home_address",        "Home Address",         "",array("class"=>"col-md-12"));

    ?>

  </fieldset>
</div>

<div class="container  dashboard-container">
  <fieldset>
    <legend><h3>Employment Required Information</h3></legend>
    <div class="col-md-12" id="hide-in-edit">
    <div class="alert alert-info">You can leave these fields blank for the meantime, but this will be required in the future for file generation use. Otherwise if left blank, this employee will not be included in the future file generations</div><br>
  </div>

    <div class="form-group col-md-12">
      <div class="form-group col-md-6">
        <label for="company_assigned" class="control-label">Company Assigned<b class="req">*</b></label> 
        <select required class="form-control "  name="company_assigned" id="company_assigned">
          <option value="0">-None-</option>
          <?php
          if(!empty($company_list)){
            foreach($company_list as $row){
              echo "<option value='".$row['company_id']."''>".$row['company_name']."</option>";
            }
          }
          ?>
        </select>
      </div>
      <div class="form-group col-md-6">
        <label for="company_assigned" class="control-label">Location Assigned<b class="req">*</b></label> 
        <select required class="form-control "  name="city_assigned" id="city_assigned">
          <option value="0">-None-</option>
          <?php
          if(!empty($location_list)){
            foreach($location_list as $row){
              echo "<option value='".$row['city_id']."''>".$row['city_name']."</option>";
            }
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group col-md-12">
      <div class="form-group col-md-12">
        <label for="company_assigned" class="control-label">Branch Assigned<b class="req">*</b></label> 
        <select required class="form-control "  name="branch_assigned" id="branch_assigned">
          <option value="0">-Please select Location and Company-</option>
          <?php
            if(!empty($user['office']['branch_id'])){
              echo "<option value='".$user['office']['branch_id']."' checked>".$user['office']['branch_name']."</option>";
            }
          ?>
        </select>
      </div>
    </div>
    <script type="text/javascript">
      var complete_branches = '<?php echo json_encode($branch_list); ?>';
      const offices = JSON.parse(complete_branches);
      // $("#branch_assigned").val('');
      $("#branch_assigned").val(0);
      $("#branch_assigned").attr('disabled','disabled');
      function check_branch(){
        $city     = $("#city_assigned").val();
        $company  = $("#company_assigned").val();
        if($city > 0 && $company > 0){
          $("#branch_assigned").removeAttr('disabled');
          $("#branch_assigned").html("<option value='0'>-Please select Location and Company-</option>"); // reset it
          jQuery.each( offices, function( i, row ) {
            if(row.city_id == $city && row.company_id == $company){
              $("#branch_assigned").append("<option value='"+row.branch_id+"'>"+row.branch_name+"</option>");
            }
          });
        }else{
          $("#branch_assigned").val(0);
          $("#branch_assigned").attr('disabled','disabled');
        }
      }
      $("#city_assigned").change(function(){ check_branch(); });
      $("#company_assigned").change(function(){ check_branch(); });
    
    </script>

    <?php
      // echo bootstrap_form("company_assigned",  "Company Assigned", "",array("class"=>"col-md-6","mreq"=>1));
      // echo bootstrap_form("company_branch",  "Company Branch", "",array("class"=>"col-md-6","mreq"=>1));
      echo bootstrap_form("account_no",  "Salary Bank Account #", "",array("class"=>"col-md-6","mreq"=>1));
      echo bootstrap_form("philhealth_no",  "PhilHealth #", "",array("class"=>"col-md-6","mreq"=>1));
      echo bootstrap_form("sss_no",  "SSS #", "",array("class"=>"col-md-6","mreq"=>1));
      echo bootstrap_form("pagibig_no",  "Pag-Ibig MID #", "",array("class"=>"col-md-6","mreq"=>1));
    ?>
  </fieldset>
</div>

<div class="container handle-container dynamic-form">
    <div class="col-md-12 padless table-responsive">
      <table class="col-md-12 table table-condensed marginless ">
        <thead>
          <tr>
            <th class="text-center tbhead" colspan="4">Dependents <a class="btn btn-xs btn-success pull-right" id="add-dependent"><span class="glyphicon glyphicon-plus"></span> Add Dependents</a></th>
          </tr>
          <tr>
            <th >Name</th>         
            <th >Relation</th>
            <th >Remarks</th>
          </tr>
        </thead>
        <tbody id="dependents-content">
          
        </tbody>
      </table>
    </div>
</div>

<script type="text/javascript">
  var dep_count = 0;
  $("#add-dependent").click(function(){
    dep_count++;
    var nextdep="<tr id='dep"+ref_count+"'> <td><input name='dep_name["+ref_count+"]' class='form-control'></td> <td><input name='dep_relation["+ref_count+"]'  class='form-control'></td><td><input name='dep_remarks["+ref_count+"]'  class='form-control'></td> <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash '></span></a></td></tr>"
    $("#dependents-content").append(nextdep);

  });

  $("#dependents-content").on('click',".delete-row",function(){
    $(this).parent().parent().remove();
  });
</script>



<div class="container  dashboard-container">
  <fieldset>
    <legend><h3>Other Personal Information</h3></legend>
    <?php
      echo bootstrap_form("tin_no",  "TIN", "",array("class"=>"col-md-6"));
      echo bootstrap_form("nbi_no",  "NBI Clearance", "",array("class"=>"col-md-6"));
      echo bootstrap_form("police_clearance",  "Police Clearance", "",array("class"=>"col-md-6"));
      echo bootstrap_form("med_cert",  "Medical Certificate", "",array("class"=>"col-md-6"));
    ?>
  </fieldset>
</div>

<div class="container  dashboard-container">
  <fieldset>
    <legend><h3>Educational Background</h3></legend>

    <?php
      echo bootstrap_form("elementary_name",    "Elementary School Name", "",array("class"=>"col-md-6"));
      echo bootstrap_form("elementary_period",  "Elementary Period", "",array("class"=>"col-md-6","placeholder"=>"ex. 1995-1999"));
      echo bootstrap_form_textarea("elementary_location","Elementary Location", "",array("class"=>"col-md-12"));
      echo bootstrap_form("highschool_name",    "Highschool Name", "",array("class"=>"col-md-6"));
      echo bootstrap_form("highschool_period",  "Highschool Period", "",array("class"=>"col-md-6","placeholder"=>"ex. 1995-1999"));
      echo bootstrap_form_textarea("highschool_location","Highschool Location", "",array("class"=>"col-md-12"));
      echo bootstrap_form("college_name",       "College Name", "",array("class"=>"col-md-6"));
      echo bootstrap_form("college_course",     "College Period and Course", "",array("class"=>"col-md-6","placeholder"=>"ex. 1995-1999 BS in Hotel and Restaurant Management"));
      echo bootstrap_form_textarea("college_location",   "College Location", "",array("class"=>"col-md-12"));
    ?>
  </fieldset>
</div>

<div class="container handle-container dynamic-form" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
      <thead>
        <tr>
          <th class="text-center tbhead" colspan="4">Work Experience <a class="btn btn-xs btn-success pull-right" id="add-workex"><span class="glyphicon glyphicon-plus"></span> Add Work Experience</a></th>
        </tr>
        <tr>
          <th >Company</th>         
          <th >Salary</th>
          <th >Reason for Leaving</th>
        </tr>
      </thead>
      <tbody id="work-content">
        <tr id="wx0">
          <td><input name='wx_company[0]' class='form-control'></td>
          <td><input name='wx_salary[0]'  class='form-control'></td>
          <td><input name='wx_reason[0]' class='form-control'></td>
          <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash '></span></a></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  var wx_count = 0;
  $("#add-workex").click(function(){
    wx_count++;
    var nextwx="<tr id='wx"+wx_count+"'> <td><input name='wx_company["+wx_count+"]' class='form-control'></td> <td><input name='wx_salary["+wx_count+"]'  class='form-control'></td> <td><input name='wx_reason["+wx_count+"]' class='form-control'></td> <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash '></span></a></td></tr>"
    $("#work-content").append(nextwx);

  });

  $("#work-content").on('click',".delete-row",function(){
    $(this).parent().parent().remove();
  });
</script>

<div class="container handle-container dynamic-form" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
      <thead>
        <tr>
          <th class="text-center tbhead" colspan="4">Training/Seminar<a class="btn btn-xs btn-success pull-right" id="add-traintb"><span class="glyphicon glyphicon-plus"></span> Add Training</a></th>
        </tr>
        <tr>
          <th >Training/Course</th>         
          <th >School and Location</th>
          <th >Period</th>
        </tr>
      </thead>
      <tbody id="train-content">
        <tr id="wx0">
          <td><input name='tr_name[0]' class='form-control'></td>
          <td><input name='tr_location[0]'  class='form-control'></td>
          <td><input name='tr_course[0]' class='form-control'></td>
          <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash '></span></a></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  var tr_count = 0;
  $("#add-traintb").click(function(){
    tr_count++;
    var nexttw="<tr id='tr"+tr_count+"'> <td><input name='tr_name["+tr_count+"]' class='form-control'></td> <td><input name='tr_location["+tr_count+"]'  class='form-control'></td> <td><input name='tr_course["+tr_count+"]' class='form-control'></td> <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash '></span></a></td></tr>"
    $("#train-content").append(nexttw);

  });

  $("#train-content").on('click',".delete-row",function(){
    $(this).parent().parent().remove();
  });
</script>

<div class="container handle-container dynamic-form" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
      <thead>
        <tr>
          <th class="text-center tbhead" colspan="4">Character Reference<a class="btn btn-xs btn-success pull-right" id="add-reference"><span class="glyphicon glyphicon-plus"></span> Add Reference</a></th>
        </tr>
        <tr>
          <th >Name</th>         
          <th >Location</th>
          <th >Contact No.</th>
        </tr>
      </thead>
      <tbody id="reference-content">
        <tr id="wx0">
          <td><input name='ref_name[0]' class='form-control'></td>
          <td><input name='ref_location[0]'  class='form-control'></td>
          <td><input name='ref_contact[0]' class='form-control'></td>
          <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash '></span></a></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  var ref_count = 0;
  $("#add-reference").click(function(){
    ref_count++;
    var nextref="<tr id='ref"+ref_count+"'> <td><input name='ref_name["+ref_count+"]' class='form-control'></td> <td><input name='ref_location["+tr_count+"]'  class='form-control'></td> <td><input name='ref_contact["+ref_count+"]' class='form-control'></td> <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash '></span></a></td></tr>"
    $("#reference-content").append(nextref);

  });

  $("#reference-content").on('click',".delete-row",function(){
    $(this).parent().parent().remove();
  });
</script>

<!--div class="container handle-container" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
      <thead>
        <tr>
          <th class="text-center tbhead" colspan="3">Character Reference</th>
        </tr>
        <tr>
          <th >Name</th>         
          <th >Location</th>
          <th >Contact No.</th>
        </tr>
      </thead>
      <tbody id="charref-content">
        
      </tbody>
    </table>
  </div>
</div-->

<div class="container  dashboard-container dynamic-form">
  <fieldset>
    <legend><h3>Employment Attachment</h3></legend>
    <div class="alert alert-info">File attachments will be available later after submitting this form</div>
  
    </fieldset>
  </div>

<div class="container  button-container">
  <div class="col-md-6">
    <a class="btn btn-lg btn-default btn-block" href="<?php echo base_url("/employee/")?>"> <span class="glyphicon glyphicon-share-alt"></span> Return to Employee List</a>
  </div>
  <div class="col-md-6">
    <button  class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="submit"> <span class="glyphicon glyphicon-ok"></span> Save Employee Record</button>
  </div>
</div>

</form>
<script type="text/javascript">
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  // $("#nav-employee").addClass("active");

  $(".dateonly").each(function (){
    $(this).find("input").datepicker();
  });

</script>
