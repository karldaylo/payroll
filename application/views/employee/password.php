<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// die(var_dump($user));
?>
<script type="text/javascript">
  $("#history-table").remove();
  $("#logs-table").remove();
</script>

<div class="container  dashboard-container">
    
    <div class="col-md-12 text-center">
      <img src="<?php echo base_url("/userimg/").$user['img']?>" style="width:200px" id="output"/>
    </div>

    <div class="col-md-12" >

      <div class="col-sm-12 padless text-center"><b style="font-size: 30px"><?php echo $user['first_name']." ".@$user['middle_name']." ".@$user['last_name']." (".$user['username'].")"; ?></b></div>
      <div class="col-sm-12 padless text-center"><?php echo $user['office']?></b></div>
      <div class="col-sm-12 padless text-center">EMPLOYEE TYPE: <?php echo $user['employee_type']?></b></div>
      <div class="col-sm-12 padless text-center"><?php echo $user['contact']." ".$user['email'] ?></b></div>
      <div class="col-sm-12 padless text-center"><?php echo $user['home_address'] ?></b></div>
       
      <div class="col-sm-12 padless text-center" id="edit-buttons">
        <a class="btn  btn-warning" href="<?php echo base_url('employee/edit/'.$id);?>">
          <span class="glyphicon glyphicon-edit"></span> Edit Employee Record
        </a>
        <a class="btn  btn-primary" href="<?php echo base_url('employee/settings/'.$id)?>">
          <span class="glyphicon glyphicon-lock"></span> Change Password
        </a>
      </div>
      
    </div>
</div>
<form method="POST" action="">
<div class="container dashboard-container" >
  <fieldset>
    <legend><h3>Change Password</h3></legend>
      <div class="col-md-12">
      <?php
        if(!empty($this->session->flashdata('display_result'))){
          echo '<div class="alert alert-warning" role="alert" ><span class="glyphicon glyphicon-exclamation-sign"> </span> ';
              echo $this->session->flashdata('display_result');         
            echo '</div>';
        }
      ?>
      <?php
        if(empty($this->session->admin) || $id == $this->session->uid){
          echo bootstrap_form("oldpass",      "Current Password",    "",array("mreq"=>true,"class"=>"col-md-12","type"=>"password"));
        }
        echo bootstrap_form("newpass",        "New Password",        "",array("mreq"=>true,"class"=>"col-md-12","type"=>"password"));
        echo bootstrap_form("confirm_newpass","Confirm New Password","",array("mreq"=>true,"class"=>"col-md-12","type"=>"password"));
      ?>
      </div>
      <br>
      <div class="col-md-3">
        
      </div>
      <div class="col-md-6">
        <button class="btn btn-lg btn-block btn-primary" type="submit" name="change_pass" value="1">Change Password</button>
      </div>
  </fieldset>
</div>
</form>