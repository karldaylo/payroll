<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// die(var_dump($user));

$office = "No office assigned.";
if(!empty($user['office']['office_name'])){
  $office = $user['office']['office_name'];
}
?>
<form method="POST" action="" id="modal-form">

<div id="training-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="" style="width:100%;max-width: 700px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">New Training/Seminar</h4>
      </div>
      <div class="modal-body" >
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-warning"> You are adding new training/course info.</div>
            <br>
          </div>

          <?php
          
            echo bootstrap_form("training_name",       "Training/Course Name",       "", array("mreq"=>true,"class"=>"col-md-12"));
            echo bootstrap_form("training_location",   "School/Facility held",  "", array("mreq"=>true,"class"=>"col-md-6"));
            echo bootstrap_form("training_period",     "Period of Training",    "", array("mreq"=>true,"class"=>"col-md-6"));
          ?>
          <div class="col-md-12">
            


            <div class="col-md-6">
              <button  class="btn btn-lg btn-primary btn-block" type="submit" name="submitraining" value="1"> <span class="glyphicon glyphicon-ok"></span> Save Training/Seminar</button>
            </div>
          </div>

        </div>         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>

<div id="dependents-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="" style="width:100%;max-width: 700px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">New Dependents</h4>
      </div>
      <div class="modal-body" >
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-warning"> You are adding new dependent info.</div>
            <br>
          </div>

          <?php
          
            echo bootstrap_form("dependent_name",       "Dependent Name",       "", array("mreq"=>true,"class"=>"col-md-12"));
            echo bootstrap_form("dependent_relation",   "Relation",  "", array("mreq"=>true,"class"=>"col-md-6"));
            echo bootstrap_form("dependent_remarks",    "Remarks",    "", array("mreq"=>true,"class"=>"col-md-6"));
          ?>
          <div class="col-md-12">
            


            <div class="col-md-6">
              <button  class="btn btn-lg btn-primary btn-block" type="submit" name="submitdependent" value="1"> <span class="glyphicon glyphicon-ok"></span> Save Dependent</button>
            </div>
          </div>

        </div>         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>

<div id="work-experience-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="" style="width:100%;max-width: 700px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">New Work Experience</h4>
      </div>
      <div class="modal-body" >
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-warning"> You are adding new work experience.</div>
            <br>
          </div>

          <?php
          
            echo bootstrap_form("company_name",       "Company Name",    "", array("mreq"=>true,"class"=>"col-md-6"));
            echo bootstrap_form("salary",       "Salary",    "", array("mreq"=>true,"class"=>"col-md-6"));
            echo bootstrap_form_textarea("reason_left",       "Reason for Leaving",    "", array("mreq"=>true,"class"=>"col-md-12"));
          ?>
          <div class="col-md-12">
            


            <div class="col-md-6">
              <button  class="btn btn-lg btn-primary btn-block" type="submit" name="submitworkexperience" value="1"> <span class="glyphicon glyphicon-ok"></span> Add Work Experience</button>
            </div>
          </div>

        </div>         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>

<div id="attachment-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="inspection_modal" style="width:100%;max-width: 700px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">New Attachment</h4>
      </div>
      <div class="modal-body" >
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-warning"> You are adding new file attachment.</div>
            <br>
          </div>

          <?php
          
            echo bootstrap_form("file_description",       "File Description",    "", array("mreq"=>true,"class"=>"col-md-12"));
          ?>
          <div class="col-md-12">
            <div class="col-md-12">
              <div class="form-group">
                <label for="attach_file" class="control-label">Attach File<b class="req">*</b></label> 
                <input  class="form-control padless attach-group" type="file" name="attach_file" id="attach_file" style="height:28px" >
              </div>
            </div>


            <div class="col-md-6">
              <button  class="btn btn-lg btn-primary btn-block" type="submit" name="fileupload" value="1"> <span class="glyphicon glyphicon-ok"></span> Upload Attachment</button>
            </div>
          </div>

        </div>         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>

</form>

<form method="POST" action="" id="delete-form">

<div class="container  dashboard-container">
    
    <div class="col-md-12 text-center">
      <img src="<?php echo base_url("/userimg/").$user['img']?>" style="width:200px" id="output"/>
    </div>

    <div class="col-md-12" >

      <div class="col-sm-12 padless text-center"><b style="font-size: 30px"><?php echo $user['first_name']." ".@$user['middle_name']." ".@$user['last_name']." (".$user['username'].")"; ?></b></div>
      <div class="col-sm-12 padless text-center"><?php echo $office?></b></div>
      <div class="col-sm-12 padless text-center">EMPLOYEE TYPE: <?php echo $user['employee_type']?></b></div>
      <div class="col-sm-12 padless text-center"><?php echo $user['contact']." ".$user['email'] ?></b></div>
      <div class="col-sm-12 padless text-center"><?php echo $user['home_address'] ?></b></div>
       
      <div class="col-sm-12 padless text-center" id="edit-buttons">
        <a class="btn  btn-warning" href="<?php echo base_url('employee/edit/'.$id);?>">
          <span class="glyphicon glyphicon-edit"></span> Edit Employee Record
        </a>
        <a class="btn  btn-primary" href="<?php echo base_url('employee/settings/'.$id)?>">
          <span class="glyphicon glyphicon-lock"></span> Change Password
        </a>
        <a class="btn  btn-success" target="popup" 
  onclick="window.open('<?php echo base_url('employee/printid/'.$id)?>','popup','height = 800, width = 500,resizable = 0'); return false;">
          <span class="glyphicon glyphicon-user"></span> View ID
        </a>
      </div>
      
    </div>
</div>
  <script type="text/javascript">
    <?php
      if(empty($this->session->staff)){//if user is not admin
        echo "$('#edit-buttons').remove();";
      }
    ?>
  </script>


<div class="container handle-container text-center" id="inspection-table">
  <table class="table table-condensed marginless ">
    <thead>
      <tr>
        <th class="text-center tbhead" colspan="1" >Personal Information </th>
      </tr>
    </thead>
    <tbody class="text-left" id="tbl-additional-info">
      <tr>
          <td>
              <div class="col-sm-12 padless">
                <?php
                    echo bootstrapalize("Date of Birth",     __date($user['birth_date'], "F j Y"),"col-md-6");
                    echo bootstrapalize("Place of Birth",    $user['birth_place'],"col-md-6");
                    echo bootstrapalize("Gender",            $user['gender'],"col-md-6");
                    echo bootstrapalize("Civil Status",      $user['civil_status'],"col-md-6");
                    echo bootstrapalize("Height and Weight",            $user['height']." ".$user['weight'],"col-md-6");
                    echo bootstrapalize("Complexion",        $user['complexion'],"col-md-6");
                    echo bootstrapalize("TIN",               $user['tin_no'],"col-md-6");
                    echo bootstrapalize("NBI Clearance",     $user['nbi_no'],"col-md-6");
                    echo bootstrapalize("Police Clearance",  $user['police_clearance'],"col-md-6");
                    echo bootstrapalize("Med Cert",          $user['med_cert'],"col-md-6");
                ?>
              </div>
          </td>
      </tr>
    </tbody>
  </table>
</div>

<div class="container handle-container text-center" >
  <table class="table table-condensed marginless ">
    <thead>
      <tr>
        <th class="text-center tbhead" colspan="1" >Credentials </th>
      </tr>
    </thead>
    <tbody class="text-left" id="tbl-required-info">
      <tr>
          <td>
              <div class="col-sm-12 padless">
                <?php
                    echo bootstrapalize("Employee ID",    $user['employee_code'],"col-md-6");
                    echo bootstrapalize("Employee Factor Type",    $user['employee_type'],"col-md-6");
                    echo bootstrapalize("Salary Bank Account No.",    $user['bank_account'],"col-md-6");
                    echo bootstrapalize("SSS No.",    $user['sss'],"col-md-6");
                    echo bootstrapalize("Philhealth No.",    $user['philhealth'],"col-md-6");
                    echo bootstrapalize("Pag-Ibig MID No.",    $user['pagibig'],"col-md-6");
                ?>
              </div>

                <div class="col-md-12 text-center">
                  
                </div>

          </td>
      </tr>
      
    </tbody>
  </table>
</div>


<div class="container handle-container" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless table-hover cursor">
      <thead>
        <tr>
          <th class="text-center tbhead" colspan="5">Attendance <a class="btn btn-xs btn-success pull-right"  href="#" onClick="MyWindow=window.open('<?php echo base_url('report/attendance/'.$id);?>','MyWindow','width=800,height=500'); return false;">View all Attendance</a></th>
        </tr>
        <tr>
          <th>Date</th>         
          <th>Days Absent</th>         
          <th>Total Wage</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody id="log-records">
        <?php
          if(empty($user['attendance'])){
            echo "<tr><td colspan='5' class='unavailable text-center'>No Records</td></tr>";
          }else{
            foreach ($user['attendance'] as $val) {
              $monthNum  = $val['att_month'];
              $dateObj   = DateTime::createFromFormat('!m', $monthNum);
              $monthName = $dateObj->format('F'); // March
              $semi = "1 to 15th";
              if($val['att_date']>1){
                $semi = "16 to 30/31th";
              }
              echo "<tr>
              <td> ".$monthName." ".$semi." ".$val['att_year']."</td>
              <td>".$val['total_absent']."</td>
              <td>".$val['total_wage']."</td>
              <td><a class='btn btn-sm btn-primary'><span class='glyphicon glyphicon-eye-open'></span> View</a>
              <a class='btn btn-sm btn-warning'><span class='glyphicon glyphicon-file'></span> PDF</a></td>
              </tr>";
            }
          }
        ?>
      </tbody>
    </table>
  </div>
</div>






<div class="container handle-container text-center" >
  <table class="table table-condensed marginless ">
    <thead>
      <tr>
        <th class="text-center tbhead" colspan="3" >Educational Background<a class='btn btn-primary btn-xs pull-right' id="toggle-education-info">Show/Hide</a></th>
      </tr>
    </thead>
    <tbody class="text-left" id="tbl-education-info">
      <tr>
          <td>
              <div class="col-sm-12 padless">
                <?php
                    echo bootstrapalize_2("Elementary"          , $user['elementary_name']." ".$user['elementary_period'] ,"col-md-12");
                    echo bootstrapalize_2("Elementary Address"  , $user['elementary_location'] ,"col-md-12");
                    echo bootstrapalize_2("Highschool"          , $user['highschool_name']." ".$user['highschool_period'] ,"col-md-12");
                    echo bootstrapalize_2("Highschool Address"  , $user['highschool_location'] ,"col-md-12");
                    echo bootstrapalize_2("College"             , $user['college_name']." ".$user['college_course'] ,"col-md-12");
                    echo bootstrapalize_2("College Address"     , $user['college_location'] ,"col-md-12");
                ?>
              </div>
          </td>
      </tr>
    </tbody>
  </table>
</div>
<script type="text/javascript">
  $("#tbl-education-info").hide();
  $("#toggle-education-info").click(function(){
    $("#tbl-education-info").toggle('display');
  });
</script>

<div class="container handle-container text-center" >
  <table class="table table-condensed marginless ">
    <thead>
      <tr>
        <th class="text-center tbhead" colspan="4" >Dependents<a class='btn btn-primary btn-xs pull-right' id="toggle-dependent-info">Show/Hide</a></th>
      </tr>
    </thead>
    <tbody class="text-left" id="tbl-dependent-info">
      <tr style="font-weight: bolder">
          <th>Name</th>         
          <th>Relation</th>         
          <th>Remarks</th>
          <th>Action</th>
      </tr>
      <?php
        if(!empty($user['dependent'])){
          foreach ($user['dependent'] as $key => $val) {
            echo "<tr>
            <td>".$val['dependent_name']."</td>
            <td>".$val['dependent_relation']."</td>
            <td>".$val['dependent_remarks']."</td>
            <td><button name='delete_dependent' value='".$val['dep_id']."' class='btn btn-danger btn-xs'>Delete</button></td>
            </tr>";
          }
        }else{
          echo "<tr class='text-center'><td class='unavailable' colspan='4'>No record found...</td></tr>";
        }
      ?>
      <tr>
        <td colspan="4">
          <div class="col-md-8"></div>
          <div class="col-md-4">
            <a class="btn btn-block btn-warning staff-button" data-toggle="modal" data-target="#dependents-modal"> <span class="glyphicon glyphicon-plus"> </span> Add New Dependents</a>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<script type="text/javascript">
  $("#tbl-dependent-info").hide();
  $("#toggle-dependent-info").click(function(){
    $("#tbl-dependent-info").toggle('display');
  });
</script>




<div class="container handle-container text-center" >
  <table class="table table-condensed marginless ">
    <thead>
      <tr>
        <th class="text-center tbhead" colspan="4" >Trainings<a class='btn btn-primary btn-xs pull-right' id="toggle-training-info">Show/Hide</a></th>
      </tr>
    </thead>
    <tbody class="text-left" id="tbl-training-info">
      <tr style="font-weight: bolder">
          <th>Name/Course</th>         
          <th>School/Facility Location</th>         
          <th>Period</th>
          <th>Action</th>
      </tr>
      <?php
        if(!empty($user['training'])){
          foreach ($user['training'] as $key => $val) {
            echo "<tr>
            <td>".$val['training_name']."</td>
            <td>".$val['training_location']."</td>
            <td>".$val['training_period']."</td>
            <td><button name='delete_training' value='".$val['tr_id']."' class='btn btn-danger btn-xs'>Delete</button></td>
            </tr>";
          }
        }else{
          echo "<tr class='text-center'><td class='unavailable' colspan='4'>No record found...</td></tr>";
        }
      ?>
      <tr>
        <td colspan="4">
          <div class="col-md-8"></div>
          <div class="col-md-4">
            <a class="btn btn-block btn-warning staff-button" data-toggle="modal" data-target="#training-modal"> <span class="glyphicon glyphicon-plus"> </span> Add New Training/Seminar Experience</a>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<script type="text/javascript">
  $("#tbl-training-info").hide();
  $("#toggle-training-info").click(function(){
    $("#tbl-training-info").toggle('display');
  });
</script>



<div class="container handle-container text-center" >
  <table class="table table-condensed marginless ">
    <thead>
      <tr>
        <th class="text-center tbhead" colspan="4" >Work Experience<a class='btn btn-primary btn-xs pull-right' id="toggle-work-info">Show/Hide</a></th>
      </tr>
    </thead>
    <tbody class="text-left" id="tbl-work-info">
      <tr style="font-weight: bolder">
          <th>Company</th>         
          <th>Previous Salary</th>         
          <th>Reason for Leaving</th>
          <th>Action</th>
      </tr>
      <?php
        if(!empty($user['work_experience'])){
          foreach ($user['work_experience'] as $key => $val) {
            if(!is_numeric($val['salary'])){
              $val['salary'] = 0;
            }
            echo "<tr>
            <td>".$val['company']."</td>
            <td>".number_format($val['salary'],2)."</td>
            <td>".$val['reason_left']."</td>
            <td><button name='delete_workexperience' value='".$val['wx_id']."' class='btn btn-danger btn-xs'>Delete</button></td>
            </tr>";
          }
        }else{
          echo "<tr class='text-center'><td class='unavailable' colspan='4'>No record found...</td></tr>";
        }
      ?>
      <tr>
        <td colspan="4">
          <div class="col-md-8"></div>
          <div class="col-md-4">
            <a class="btn btn-block btn-warning staff-button" data-toggle="modal" data-target="#work-experience-modal"> <span class="glyphicon glyphicon-plus"> </span> Add New Work Experience</a>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>



<div class="container handle-container text-center " >
  <table class="table table-condensed marginless ">
    <thead>
      <tr>
        <th class="text-center tbhead" colspan="3" >File Attachments<a class='btn btn-primary btn-xs pull-right' id="toggle-file-info">Show/Hide</a></th>
      </tr>
    </thead>
    <tbody class="text-left" id="tbl-file-info">
      <tr style="font-weight: bolder">
          <th>File Description</th>         
          <th>Action</th>         
      </tr>
      <?php
        if(!empty($user['attachment'])){
          foreach ($user['attachment'] as $key => $val) {
            echo "<tr>
            <td>
              <b>Filename: </b>".$val['filename']."
              <br><b>Description: </b>".$val['description']."
            </td>
            <td>
              <a href='".base_url("/attachment/".$val['filename'])."' target='__blank' class='btn btn-primary'>View/Download</a>
            </td>
            </tr>";
          }
        }else{
            echo "<tr><td colspan='2' class='unavailable text-center'> No attachment </td></tr>";

        }
      ?>
      <tr>
        <td colspan="2">
          <div class="col-md-8"></div>
          <div class="col-md-4">
            <a class="btn btn-block btn-warning staff-button" data-toggle="modal" data-target="#attachment-modal"> <span class="glyphicon glyphicon-plus"> </span> Add New Attachment</a>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</div>


<?php
if(!empty($this->session->admin)){//if user is admin
?>
<div class="container handle-container" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
      <thead>
        <tr>
          <th class="text-center tbhead" colspan="2">Logs <a class="btn btn-xs btn-success pull-right"  href="#" onClick="MyWindow3=window.open('<?php echo base_url('report/userlog/'.$id);?>','MyWindow3','width=800,height=500'); return false;">View all logs</a></th>
        </tr>
        <tr>
          <th class="datetable">Date</th>         
          <th >Description</th>
        </tr>
      </thead>
      <tbody id="log-records">
        <?php
          if(empty($user['logs'])){
            echo "<tr><td colspan='2' class='unavailable text-center'>No Records</td></tr>";
          }else{
            foreach ($user['logs'] as $val) {
              echo "<tr>
              <td>".__date($val['date_created'])."</td>
              <td>".$val['description']."</td>
              </tr>";
            }
          }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
}//end of logs
?>
</form>

<script type="text/javascript">
  $("#tbl-file-info").hide();
  $("#toggle-file-info").click(function(){
    $("#tbl-file-info").toggle('display');
  });
</script>

<script type="text/javascript">
  $("#tbl-work-info").hide();
  $("#toggle-work-info").click(function(){
    $("#tbl-work-info").toggle('display');
  });
</script>

<script type="text/javascript">
  $("#nav-employee").addClass("active");
  $(".dateonly").each(function (){
    $(this).find("input").datepicker({
      format:"YYYY-MM-DD",
    });
  });
  $(".timeonly").each(function (){
    $(this).find("input").datetimepicker({
      format:"LT",
    });
  });


  //delete prompt
  $("#delete-form").submit(function(event){
    var r=confirm("Are you sure to delete this entry?\nThis is irreversable.");
    if (r==false){
      console.log("canceled");
      event.preventDefault();
    }
  });
</script>