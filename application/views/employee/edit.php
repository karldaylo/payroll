<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
$(document).ready(function(){

$("#username").attr('readonly','readonly');
$("#password").attr('readonly','readonly');
$("#password").val('fakepassword');
$("#confirm_password").attr('readonly','readonly');
$("#confirm_password").val('fakepassword');
$("#hide-in-edit").hide();
<?php
  echo js_input('employee_code'       ,@$user['employee_code']);
  echo js_input('username'            ,@$user['username']);
  echo js_input('first_name'          ,@$user['first_name']);
  echo js_input('middle_name'         ,@$user['middle_name']);
  echo js_input('last_name'           ,@$user['last_name']);
  echo js_input('birth_date'          ,@__date($user['birth_date'],"m/d/Y"));
  echo js_input('birth_place'         ,@$user['birth_place']);
  echo js_input('height'              ,@$user['height']);
  echo js_input('weight'              ,@$user['weight']);
  echo js_input('complexion'          ,@$user['complexion']);
  echo js_input('home_address'        ,@$user['home_address']);
  echo js_input('contact'             ,@$user['contact']);
  echo js_input('gender'              ,@$user['gender']);
  echo js_input('account_no'          ,@$user['bank_account']);
  echo js_input('philhealth_no'       ,@$user['philhealth']);
  echo js_input('sss_no'              ,@$user['sss']);
  echo js_input('pagibig_no'          ,@$user['pagibig']);
  echo js_input('civil_status'        ,@$user['civil_status']);
  echo js_input('spouse_name'         ,@$user['spouse_name']);
  echo js_input('spouse_address'      ,@$user['spouse_address']);
  echo js_input('tin_no'              ,@$user['tin_no']);
  echo js_input('nbi_no'              ,@$user['nbi_no']);
  echo js_input('police_clearance'    ,@$user['police_clearance']);
  echo js_input('med_cert'            ,@$user['elementary_period']);
  echo js_input('elementary_name'     ,@$user['elementary_name']);
  echo js_input('elementary_period'   ,@$user['elementary_period']);
  echo js_input('elementary_location' ,@$user['elementary_location']);
  echo js_input('highschool_name'     ,@$user['highschool_name']);
  echo js_input('highschool_period'   ,@$user['highschool_period']);
  echo js_input('highschool_location' ,@$user['highschool_location']);
  echo js_input('college_name'        ,@$user['college_name']);
  echo js_input('college_course'      ,@$user['college_course']);
  echo js_input('college_location'    ,@$user['college_location']);
  
if(!empty($user['office']['branch_id'])){
  echo js_input('company_assigned'    ,@$user['office']['company_id']);
  echo js_input('city_assigned'       ,@$user['office']['city_id']);
  echo js_input('branch_assigned'     ,@$user['office']['branch_id']);
}

?>
$("#output").attr('src',"<?php echo base_url("/userimg/".$user["img"]);?>");

$(".dynamic-form").each(function(){
  $(this).remove();
});

});
</script>