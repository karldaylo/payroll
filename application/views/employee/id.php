<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// die(var_dump($user));
?>
<style type="text/css">
*{
  font-family: sans-serif;
}
#body{
  width: 450px;
  position: absolute;
}
.id-base{
  width: 450px;
}
#bg{
  position: absolute;
  z-index: 1;
}

.txt{
  position: absolute;
  z-index: 2;
}
#field_employee_number{
  /*border: solid 1px red;*/
  top: 185px;
  left: 213px;
  width: 190px;
  text-align: center;
}
#field_employee_name_container{
  /*border: solid 1px red;*/
  top: 225px;
  left: 213px;
  width: 190px;
  height: 45px;
  text-align: center;
}
#field_employee_name{
  position: absolute;
  bottom: 0;
  text-align: center;
  left: 0;
  right: 0;
}
#field_employee_company{
  /*border: solid 1px red;*/
  top: 315px;
  left: 213px;
  width: 190px;
  text-align: center;
}
#field_employee_address_container{
  /*border: solid 1px red;*/
  top: 715px;
  left: 50px;
  width: 350px;
  height: 125px;
  text-align: center;
}
#field_employee_address{
  position: absolute;
  bottom: 0;
  text-align: center;
  left: 0;
  right: 0;
}
#field_employee_dob{
  /*border: solid 1px red;*/
  top: 895px;
  left: 130px;
  width: 190px;
  text-align: center;
}
#field_employee_sss{
  /*border: solid 1px red;*/
  top: 955px;
  left: 130px;
  width: 190px;
  text-align: center;
}
#field_employee_tin{
  /*border: solid 1px red;*/
  top: 1015px;
  left: 130px;
  width: 190px;
  text-align: center;
}
#profilepic{
  /*border: solid 1px red;*/
  position: absolute;
  z-index: 99;
  max-width: 145px;
  max-height: 145px;
  left: 43px;
  background-color: white;
  top: 199px;
} 
</style>
<?php
    __js('assets/js/jquery.min.js');
  
?>
<div id="body">
  <div id="bg">
    <img src="<?php echo base_url('/assets/img/front.png')?>" class="id-base">
    <img src="<?php echo base_url('/assets/img/Back_new.png')?>" class="id-base">
  </div>
  <div id="content">
    <div id="field_employee_number" class="txt">{employee.number}</div>
    <div id="field_employee_name_container" class="txt">
      <div id="field_employee_name">{employee.name}</div>
    </div>
    <div id="field_employee_company" class="txt">{employee.company}</div>
    <img id="profilepic" class="txt" src="<?php echo base_url("/userimg/").$user['img']?>">
    <div id="field_employee_address_container" class="txt">
      <div id="field_employee_address">{employee.address}</div>
    </div>
    <div id="field_employee_dob" class="txt">{employee.dob}</div>
    <div id="field_employee_sss" class="txt">{employee.sss}</div>
    <!-- <div id="field_employee_tin" class="txt">{employee.tin}</div> -->
    <div id="field_employee_mobile" class="txt">{employee.mobile}</div>
  </div>
</div>

<script type="text/javascript">
  <?php
  echo js_html("field_employee_number",$user['employee_id']);
  echo js_html("field_employee_name",$user['first_name']." ".@$user['middle_name']." ".@$user['last_name']);
  echo js_html("field_employee_company",$user['branch_name']);
  echo js_html("field_employee_address",$user['home_address']);
  echo js_html("field_employee_dob",$user['birth_date']);
  echo js_html("field_employee_sss",$user['sss']);
  echo js_html("field_employee_mobile",$user['mobile']);
  // echo js_html("field_employee_tin",$user['tin_no']);
  ?>
</script>