<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$downloadbutton =  str_replace("/medical/","/",$_SERVER["REQUEST_URI"]);
$downloadbutton =  str_replace("/search/","/report/advance/",$downloadbutton);
?>

  <div class="container padless">
    <br>
    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=item'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-barcode"></span></div>
        <div class="dashboard-content"> Inventory</div>
        <div class="dashboard-sub">Search by Inventories</div>
      </a>
    </div>

    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=user'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-user"></span> </div>
        <div class="dashboard-content"> User Account</div>
        <div class="dashboard-sub">Search by User Accounts</div>
      </a>
    </div>

    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=patient'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-calendar"></span></div>
        <div class="dashboard-content"> Patient </div>
        <div class="dashboard-sub">Search by Patient</div>
      </a>
    </div>
  </div>

<form id="searchbox" method="GET" action="<?php echo base_url('/search/')?>">
  <div class="container button-container">      
      <div class="col-sm-12 padless ">
        <div class="input-group pull-right">
              <input type="hidden" name="method" value="item">
              <input id="search" type="text" name="q" class="form-control input-lg" placeholder="Search for Inventory..." value="<?php echo @$this->input->get('q'); ?>">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-default btn-lg" id="search_button"><span class="glyphicon glyphicon-search"></span> Search</button>
              </span>
          </div>
      </div>
  </div>
</form>

<form id="searchbox2" method="GET" action="<?php echo base_url('/search/')?>" style="display: none">
  <input type="hidden" name="method" value="item">
  <div class="container dashboard-container">  
    <h3 class="marginless">Advance Search</h3><hr class="marginless">

      <div class="col-md-12">
        <div class="col-md-12">
          <div class="form-group">
              <label class="form-label" class="">Search by detail contains:</label>
              <?php
              echo bootstrap_subform("inventory_name",           "Inventory Name",                 @$this->input->get('inventory_name'));
              echo bootstrap_subform("inventory_code",           "Inventory Code",                 @$this->input->get('inventory_code'));
              echo bootstrap_subform("inventory_desc",           "Description",                    @$this->input->get('inventory_desc'));
              echo bootstrap_subform("inventory_serial",         "Serial/Marks",                   @$this->input->get('inventory_serial'));
              echo bootstrap_subform("inventory_category",       "Category",                       @$this->input->get('inventory_category'));
              echo bootstrap_subform("inventory_location",       "Stored at ",                     @$this->input->get('inventory_location'));
              echo bootstrap_subform("inventory_brand",          "Item Brand",                     @$this->input->get('inventory_brand'));
              echo bootstrap_subform("inventory_tags",           "Inventory Tags",                 @$this->input->get('inventory_tags'));
              ?>
          </div>
        </div>
        <!-- <div class="col-md-6">
          <div class="form-group">
              <label class="form-label" class="">Price Range (or equal to)</label>
              <?php 
              echo bootstrap_subform("inventory_greater",        "Greater than",                  @$this->input->get('inventory_greater')  ,array("type"=>"number"));
              echo bootstrap_subform("inventory_less",           "Less than ",                    @$this->input->get('inventory_less')    ,array("type"=>"number"));
              ?>
          </div>
          <div class="form-group">
              <label class="form-label" class="">Date Range (or equal to) </label>
              <?php 
              echo bootstrap_subform("inventory_dtstart",        "From Date",                     @$this->input->get('inventory_dtstart'));
              echo bootstrap_subform("inventory_dtend",          "To Date",                       @$this->input->get('inventory_dtend'));
              ?>
          </div>
          <br>

        </div> -->
          <button class="btn btn-lg btn-block btn-primary" type="submit" name="advance" value="true"> Search with Filters</button>

      </div>
    </div>
</form>

<div class="container dashboard-container">  
  <?php 
    if(!empty($result_list)){
  ?>
  <a href="<?php echo base_url($downloadbutton) ?>" class="btn btn-warning  pull-right" target="_blank">Download Results</a>
  <?php
    }
  ?>
  <h3>Inventory Results</h3>
  <div class="table-responsive">
    <table class="table table-bordered table-hover dashboardTable">
      <thead>          
        <tr class="tbhead">
          <th>#</th>
          <th>Name</th>
          <th>Brand</th>
          <th class="hidden-sm hidden-xs" >Description</th>
          <th>Quantity</th>
        </tr>
      </thead>   
      <tbody  id="item-list-container">          
        <?php 
          if(!empty($result_list)){
            $page = 0;
            foreach ($result_list as $row) {
              $page++;
              $row_class    = "";
              echo"
              <tr class='clickable-tr pointer' data-target='$row[item_id]'>
                <td>$page</td>
                <td>$row[unit_name]</td>
                <td>".$row['unit_brand']."</td>
                <td>$row[unit_description]</td>
                <td>$row[unit_remaining] of $row[unit_total]</td>
              </tr>";
            }
          }
          else{
            echo"
              <tr class='text-center unavailable '>
                <td colspan='5'>No results found...</td>
              </tr>";
          }
        ?>
        </tbody>   
    </table>
  </div>    
</div>

<script type="text/javascript">

  $("#item-list-container").on("click",".clickable-tr",function (){
    $target_id = $(this).data("target");
    window.location.href='<?php echo base_url("/inventory/manage")?>/'+$target_id;
  });
  $("#nav-search").addClass("active");

  $(function () {
    $('#inventory_dtend').datetimepicker({
      format: 'YYYY-MM-DD',
      // sideBySide: true
    });
    $('#inventory_dtstart').datetimepicker({
      format: 'YYYY-MM-DD',
      // sideBySide: true
    });
  });
</script>

