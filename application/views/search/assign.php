<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$downloadbutton =  str_replace("/inventory/","/",$_SERVER["REQUEST_URI"]);
$downloadbutton =  str_replace("/search/","/report/advance/",$downloadbutton);
?>
  <div class="container padless">
    <br>
    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=item'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-barcode"></span></div>
        <div class="dashboard-content"> Inventory</div>
        <div class="dashboard-sub">Search by Inventories</div>
      </a>
    </div>

    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=user'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-user"></span> </div>
        <div class="dashboard-content"> User Account</div>
        <div class="dashboard-sub">Search by User Accounts</div>
      </a>
    </div>

    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=patient'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-calendar"></span></div>
        <div class="dashboard-content"> Patient </div>
        <div class="dashboard-sub">Search by Patient</div>
      </a>
    </div>
  </div>

<form id="searchbox" method="GET" action="<?php echo base_url('/search/')?>">
  <div class="container button-container">      
      <div class="col-sm-12 padless ">
        <div class="input-group pull-right">
              <input type="hidden" name="method" value="assign">
              <input id="search" type="text" name="q" class="form-control input-lg" placeholder="Search for Inventory..." value="<?php echo @$this->input->get('q'); ?>">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-default btn-lg" id="search_button"><span class="glyphicon glyphicon-search"></span> Search</button>
              </span>
          </div>
      </div>
  </div>
</form>

<form id="searchbox2" method="GET" action="<?php echo base_url('/search/')?>">
  <input type="hidden" name="method" value="assign">
  <div class="container dashboard-container">  
    <h3 class="marginless">Advance Search</h3><hr class="marginless">

      <div class="col-md-12">

        <div class="col-md-12">
          <div class="form-group">
              <label class="form-label" class="">Search by detail contains:</label>
              <?php
              echo bootstrap_subform("inventory_name",           "Inventory Name",                 @$this->input->get('inventory_name'));
              echo bootstrap_subform("inventory_code",           "Inventory Code",                 @$this->input->get('inventory_code'));
              echo bootstrap_subform("inventory_serial",         "Serial/Marks",                   @$this->input->get('inventory_serial'));
              echo bootstrap_subform("inventory_category",       "Category",                       @$this->input->get('inventory_category'));
              echo bootstrap_subform("inventory_tags",           "Inventory Tags",                 @$this->input->get('inventory_tags'));
              echo bootstrap_subform("inventory_location",       "Stored at",                      @$this->input->get('inventory_location'));
              ?>
          </div>
          <div class="form-group">
            <label for="release_to" class="control-label">Assigned to</label>
            <select class="form-control" id="assign_to" name="assign_to" >
              <option value=''>-Select User-</option>
              <?php
                foreach ($users as $key => $row) {
                  echo"<option value='".$row['uid']."'>".$row['last_name'].", ".$row['first_name']." ".$row['middle_name']."</option>";
                }
              ?>
            </select>
          </div>
        </div>
        <!-- <div class="col-md-6">
         <div class="form-group">
              <label class="form-label" class="">Price Range (or equal to)</label>
              <?php 
              echo bootstrap_subform("inventory_greater",        "Greater than",                  @$this->input->get('inventory_greater')  ,array("type"=>"number"));
              echo bootstrap_subform("inventory_less",           "Less than ",                    @$this->input->get('inventory_less')    ,array("type"=>"number"));
              ?>
          </div>
         <div class="form-group">
              <label class="form-label" class="">Date Range Assigned (or equal to) </label>
              <?php 
              echo bootstrap_subform("inventory_dtstart",        "From Date",                     @$this->input->get('inventory_dtstart'));
              echo bootstrap_subform("inventory_dtend",          "To Date",                       @$this->input->get('inventory_dtend'));
              ?>
          </div>
          <br>
          
        </div> -->
          <button class="btn btn-lg btn-block btn-primary" type="submit" name="advance" value="true"> Search with Filters</button>
      </div>
  </div>

</form>

<div class="container dashboard-container">  
  <?php 
    if(!empty($result_list)){
  ?>
  <a href="<?php echo base_url($downloadbutton) ?>" class="btn btn-warning  pull-right" target="_blank">Download Results</a>
  <?php
    }
  ?>
  <h3>Assigned Inventory Results</h3>
  <div class="table-responsive">
    <table class="table table-bordered table-hover dashboardTable">
      <thead>          
        <tr class="tbhead">
          <th>Item</th>
          <th>Name</th>
          <th>Status</th>
          <th>Office</th>
          <th>Destination</th>
          <th>Assigned Date</th>
          <th>Quantity</th>
        </tr>
      </thead>
      <tbody id="active-records">
        <?php
          if(empty($result_list)){
            echo "<tr><td colspan='7' class='unavailable text-center'>No results found...</td></tr>";
          }else{
            foreach ($result_list as $val) {
              //fix name
              $name = $val['display_name'];
              if(!empty($val['uid'])){
                $name   = "<a href='".base_url("/user/view/".$val['uid'])."'>".$val['first_name']." ".$val['last_name']."</a>";
              }

              //fix item
              $item = $val['unit_name'];
              if(!empty($val['uid'])){
                $item   = "<a href='".base_url("/inventory/manage/".$val['item_id'])."'>".$val['unit_name']."</a>";
              }

              echo "<tr>
              <td>".$item."</td>
              <td>".$name."</td>
              <td>".$val['status']."</td>
              <td>".$val['office']."</td>
              <td>".$val['destination']."</td>
              <td>".__date($val['start_date'],"M j, Y")."</td>
              <td>".$val['unit_qty']."</td>
              </tr>";
            }
          }
        ?>
      </tbody>
    </table>
  </div>
</div>

<script type="text/javascript">
  $("#nav-search").addClass("active");
</script>