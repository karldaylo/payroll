<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form id="searchbox" method="GET" action="<?php echo base_url('/search/')?>">
  <div class="container button-container">      
      <div class="col-sm-12 padless ">
        <div class="input-group pull-right">
              <input id="search" type="text" name="q" class="form-control input-lg" placeholder="Search for Inventory..." value="<?php echo @$this->input->get('q'); ?>">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-default btn-lg" id="search_button"><span class="glyphicon glyphicon-search"></span> Search</button>
              </span>
          </div>
      </div>
  </div>
</form>

<div class="container dashboard-container">  
  <h3>Item Results</h3>
  <div class="table-responsive">
    <table class="table table-bordered table-hover dashboardTable">
      <thead>          
        <tr class="tbhead">
          <th>#</th>
          <th>Name</th>
          <th>Code</th>
          <th class="hidden-sm hidden-xs" >Description</th>
          <th>Quantity</th>
        </tr>
      </thead>   
      <tbody  id="item-list-container">          
        <?php 
          if(!empty($inventory_list)){
            $page = 0;
            foreach ($inventory_list as $row) {
              $page++;
              $row_class    = "";
              echo"
              <tr class='clickable-tr pointer' data-target='$row[item_id]'>
                <td>$page</td>
                <td>$row[unit_name]</td>
                <td>".strtoupper($row['unit_code'])."</td>
                <td>$row[unit_description]</td>
                <td>$row[unit_remaining] of $row[unit_total]</td>
              </tr>";
            }
          }
          else{
            echo"
              <tr class='text-center unavailable '>
                <td colspan='5'>There are no inventory...</td>
              </tr>";
          }
        ?>
        </tbody>   
    </table>
  </div>    
</div>

<script type="text/javascript">

  $("#item-list-container").on("click",".clickable-tr",function (){
    $target_id = $(this).data("target");
    window.location.href='<?php echo base_url("/inventory/manage")?>/'+$target_id;
  });
</script>




<div class="container dashboard-container">  
  <h3>User Results</h3>
  <div class="table-responsive">
    <table class="table table-bordered table-hover dashboardTable">
      <thead>          
        <tr class="tbhead">
          <th>#</th>
          <th>Name</th>
          <th>Username</th>
          <th class="hidden-sm hidden-xs">Designation</th>
          <th>Department</th>
          <th>Base Office</th>
        </tr>
      </thead>   
      <tbody  id="user-list-container">          
        <?php 
          if(!empty($user_list)){
            $page = 0;
            foreach ($user_list as $row) {
              $page++;
              $row_class    = "";
              echo"
              <tr class='clickable-tr pointer' data-target='$row[uid]'>
                <td>$page</td>
                <td>$row[last_name] ,$row[first_name] $row[middle_name]</td>
                <td>$row[username]</td>
                <td>$row[designation]</td>
                <td>$row[department]</td>
                <td>$row[office]</td>
              </tr>";
            }
          }
          else{
            echo"
              <tr class='text-center unavailable '>
                <td colspan='6'>There are no inventory...</td>
              </tr>";
          }
        ?>
        </tbody>   
    </table>
  </div>    
</div>

<script type="text/javascript">
  $("#nav-search").addClass("active");

  $("#user-list-container").on("click",".clickable-tr",function (){
    $target_id = $(this).data("target");
    window.location.href='<?php echo base_url("/user/view")?>/'+$target_id;
  });
</script>