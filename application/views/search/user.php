<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$downloadbutton =  str_replace("/medical/","/",$_SERVER["REQUEST_URI"]);
$downloadbutton =  str_replace("/search/","/report/advance/",$downloadbutton);
?>

  <div class="container padless">
    <br>
    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=item'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-barcode"></span></div>
        <div class="dashboard-content"> Inventory</div>
        <div class="dashboard-sub">Search by Inventories</div>
      </a>
    </div>

    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=user'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-user"></span> </div>
        <div class="dashboard-content"> User Account</div>
        <div class="dashboard-sub">Search by User Accounts</div>
      </a>
    </div>

    <div class="col-sm-4  ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless " href="<?php echo base_url('search/?method=patient'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-calendar"></span></div>
        <div class="dashboard-content"> Patient </div>
        <div class="dashboard-sub">Search by Patient</div>
      </a>
    </div>
  </div>

<form id="searchbox" method="GET" action="<?php echo base_url('/search/')?>">
  <div class="container button-container">      
      <div class="col-sm-12 padless ">
        <div class="input-group pull-right">
              <input type="hidden" name="method" value="user">
              <input id="search" type="text" name="q" class="form-control input-lg" placeholder="Search for Inventory..." value="<?php echo @$this->input->get('q'); ?>">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-default btn-lg" id="search_button"><span class="glyphicon glyphicon-search"></span> Search</button>
              </span>
          </div>
      </div>
  </div>
</form>

<form id="searchbox2" method="GET" action="<?php echo base_url('/search/')?>" style="display:none">
  <input type="hidden" name="method" value="user">
  <div class="container dashboard-container">  
    <h3 class="marginless">Advance Search</h3><hr class="marginless">

      <div class="col-md-12">
        <div class="col-md-12">
          <div class="form-group">
              <label class="form-label" class="">Search by detail contains:</label>
              <?php
              echo bootstrap_subform("username",        "Username",           @$this->input->get('username'));
              echo bootstrap_subform("fname",           "First Name",         @$this->input->get('fname'));
              echo bootstrap_subform("mname",           "Middle Name",        @$this->input->get('mname'));
              echo bootstrap_subform("lname",           "Last Name",          @$this->input->get('lname'));
              echo bootstrap_subform("designation",     "Designation",        @$this->input->get('designation'));
              echo bootstrap_subform("department",      "Department",         @$this->input->get('department'));
              echo bootstrap_subform("office",          "Office",             @$this->input->get('office'));
              echo bootstrap_subform("email",           "Email",              @$this->input->get('email'));
              ?>
          </div>
        </div>
        <!-- <div class="col-md-6">
          
           <div class="form-group">
              <label class="form-label" class="">Date Registered (or equal to) </label>
              <?php 
              echo bootstrap_subform("dtstart",        "From Date",                     @$this->input->get('dtstart'));
              echo bootstrap_subform("dtend",          "To Date",                       @$this->input->get('dtend'));
              ?>
          </div>
          <br>

        </div> -->
          <button class="btn btn-lg btn-block btn-primary" type="submit" name="advance" value="true"> Search with Filters</button>
      </div>
    </div>

</form>


<div class="container dashboard-container">  
  <?php 
    if(!empty($result_list)){
  ?>
  <a href="<?php echo base_url($downloadbutton) ?>" class="btn btn-warning  pull-right" target="_blank">Download Results</a>
  <?php
    }
  ?>
  <h3>User Results</h3>
  <div class="table-responsive">
    <table class="table table-bordered table-hover dashboardTable">
      <thead>          
        <tr class="tbhead">
          <th>#</th>
          <th>Name</th>
          <th>Username</th>
          <th class="hidden-sm hidden-xs">Designation</th>
          <th>Base Office</th>
        </tr>
      </thead>   
      <tbody  id="user-list-container">          
        <?php 
          if(!empty($result_list)){
            $page = 0;
            foreach ($result_list as $row) {
              $page++;
              $row_class    = "";
              echo"
              <tr class='clickable-tr pointer' data-target='$row[uid]'>
                <td>$page</td>
                <td>$row[fullname]</td>
                <td>$row[username]</td>
                <td class='hidden-sm hidden-xs'>$row[designation]</td>
                <td>$row[office_code]</td>
              </tr>";
            }
          }
          else{
            echo"
              <tr class='text-center unavailable '>
                <td colspan='5'>No results found...</td>
              </tr>";
          }
        ?>
        </tbody>   
    </table>
  </div>    
</div>

<script type="text/javascript">
  $("#nav-search").addClass("active");

  $("#user-list-container").on("click",".clickable-tr",function (){
    $target_id = $(this).data("target");
    window.location.href='<?php echo base_url("/user/view")?>/'+$target_id;
  });

  $(function () {
    $('#dtend').datetimepicker({
      format: 'YYYY-MM-DD',
      // sideBySide: true
    });
    $('#dtstart').datetimepicker({
      format: 'YYYY-MM-DD',
      // sideBySide: true
    });
  });
</script>