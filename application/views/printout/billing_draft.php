<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// die(var_dump($this->input->get()));
$path = __dir__."/boh.jpg";
$type = pathinfo($path, PATHINFO_EXTENSION);
$imgdata = file_get_contents($path);
$img = 'data:image/' . $type . ';base64,' . base64_encode($imgdata);
$total_price = 0;

if($this->input->get("start_date")){
  $bill['start_date'] =  __date(@$this->input->get("start_date"),"d-M-y");
}
if($this->input->get("end_date")){
  $bill['end_date'] =  __date(@$this->input->get("end_date"),"d-M-y");
}

if($this->input->get("print_date")){
  $bill['print_date'] =  __date(@$this->input->get("print_date"),"d-M-y");
}
if($this->input->get("due_date")){
  $bill['due_date'] =  __date(@$this->input->get("due_date"),"d-M-y");
}
if($this->input->get("pb")){
  $bill['prepared_by'] =  $this->input->get("pb");
}
if($this->input->get("ab")){
  $bill['approved_by'] =  $this->input->get("ab");
}

  $bill['recur_p'] = 0;
  $bill['recur_t'] = 0;
  $bill['outstanding_t'] = 0;

if(!empty($this->input->get('recur_t'))){
  $bill['recur_t'] = $this->input->get('recur_t');
}
if(!empty($this->input->get('recur_p'))){
  $bill['recur_p'] = $this->input->get('recur_p');
}
if(!empty($this->input->get('outstanding_t'))){
  $bill['outstanding_t'] = $this->input->get('outstanding_t');
}

$path_draft     = __dir__."/watermark.png";
$type_draft     = pathinfo($path_draft, PATHINFO_EXTENSION);
$imgdata_draft  = file_get_contents($path_draft);
$img_draft      = 'data:image/' . $type_draft . ';base64,' . base64_encode($imgdata_draft);
?>
<html>
  <head>
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/normalize.css"); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/printout.css"); ?>"> -->
    <style>
      <?php include(__dir__."/../../../assets/css/printout.css");?>
    </style> 
  </head>
  <body>
    <div id="watermark">
        <img src="<?php echo $img_draft; ?>" height="100%" width="100%" />
    </div>
    <img src="<?php echo $img; ?>" style="position:fixed; width:100px" />
    <div id="head1">Bridge of Hope Quezon City Inc.</div>
    <div id="head2">Your Journey Starts Here</div>
    <br>
    <br>
    <br>
    <div id="head3">Billing Statement</div>
    <br>
    <table>
      <tr >
        <td>NAME:</td>
        <td><?php echo $bill['first_name']." ".$bill['middle_name']." ".$bill['last_name']." ".$bill['suffix_name']?></td>
      </tr>
      <tr>
        <td>CONTACT #:</td>
        <td><?php echo $bill['contact'] ?></td>
      </tr>
      <tr>
        <td>BIRTHDATE:</td>
        <td><?php echo __date($bill['birthdate'],"F j Y") ?></td>
      </tr>
      <tr>
        <td> &nbsp;</td>
        <td> &nbsp;</td>
      </tr>

      <tr>
        <td>INCASE OF EMERGENCY:</td>
        <td><?php echo $bill['emergency_contact_name']." ".$bill['emergency_contact'] ?></td>
      </tr>
      <tr>
        <td>ADMISSION DATE:</td>
        <td><?php echo __date($bill['date_admitted'],"F j Y") ?></td>
      </tr>
    </table>

    <br>
    <table style="width:100%;text-align: center">
      <tr style="font-weight: bold">
        <td style="width:25%">BILLING STATEMENT DATED</td>
        <td >ITEMS</td>
        <td >REMARKS</td>
        <td style="width:15%">AMOUNT</td>
        <td style="width:15%">TOTAL</td>
      </tr>
      <?php
      // if(!empty($bill['recurring_treatment']) ){
         echo "<tr class='text-center'>
            <td>".__date($bill['start_date'],"d-M-y")." to ".__date($bill['end_date'],"d-M-y")."</td>
            <td>Treatment Fee</td>
            <td> </td>
            <td> </td>
            <td>".__peso($bill['recur_t'])."</td>
            <td>".__peso($bill['recur_t'])."</td>
          </tr>";
          if(is_numeric($bill['recur_t'])){
            $total_price += $bill['recur_t'];
          }
      // }
       if(!empty($bill['recur_p']) ){
         echo "<tr class='text-center'>
            <td></td>
            <td>Medical Fee</td>
            <td> </td>
            <td> </td>
            <td>".__peso($bill['recur_p'])."</td>
            <td>".__peso($bill['recur_p'])."</td>
          </tr>";
          if(is_numeric($bill['recur_p'])){
            $total_price += $bill['recur_p'];
          }
      }
      if(!empty($bill['outstanding_t']) ){
         echo "<tr class='text-center'>
            <td></td>
            <td>Outstanding Balance </td>
            <td>Previous Bill </td>
            <td> </td>
            <td>".__peso($bill['outstanding_t'])."</td>
            <td>".__peso($bill['outstanding_t'])."</td>
          </tr>";
          if(is_numeric($bill['outstanding_t'])){
            $total_price += $bill['outstanding_t'];
          }
      }
      
      ?>
    </table>

    <br>

    OTHER CHARGES
    <table id="main-table">
      <tr class="tbl-header">
        <td style="width:10%">DATE</td>
        <td>PARTICULARS/ITEM</td>
        <td style="width:10%">QUANTITY</td>
        <td style="width:15%">UNIT PRICE</td>
        <td style="width:15%">AMOUNT</td>
        <td class="left-only" style="width:15%"></td>
      </tr>
      <tr>
        <td colspan="5">CHECK UP</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['CHECKUP'])){
      $total_checkup =0;
      foreach ($bill['billing']['CHECKUP'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td> </td>
            <td> </td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_checkup += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }

      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-only"><?php echo number_format(@$total_checkup,2) ?></td>
      </tr>
      <tr>
        <td colspan="5">CLINIC</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['CLINIC'])){
      $total_clinic = 0;
      foreach ($bill['billing']['CLINIC'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td>".$val['quantity']."</td>
            <td>".__peso($val['unit_price'])."</td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_clinic += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }
      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-only"><?php echo number_format(@$total_clinic,2) ?></td>
      </tr>
      <tr>
        <td colspan="5">DENTAL</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['DENTAL'])){
      $total_dental =0;
      foreach ($bill['billing']['DENTAL'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td> </td>
            <td> </td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_dental += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }

      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-only"><?php echo number_format(@$total_dental,2) ?></td>
      </tr>
      <tr>
        <td colspan="5">LAUNDRY</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['LAUNDRY'])){
      $total_laundry = 0;
        foreach ($bill['billing']['LAUNDRY'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td>".$val['quantity']."</td>
            <td>".__peso($val['unit_price'])."</td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_laundry += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }
      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-only"><?php echo number_format(@$total_laundry,2) ?></td>
      </tr>
      <tr>
        <td colspan="5">HAIRCUT</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['HAIRCUT'])){
      $total_haircut = 0;
        foreach ($bill['billing']['HAIRCUT'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td> </td>
            <td> </td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_haircut += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }
      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-only"><?php echo number_format(@$total_haircut,2) ?></td>
      </tr>
      <tr>
        <td colspan="5">SERVICE FEE</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['SERVICEFEE'])){
      $total_servicefee = 0;
        foreach ($bill['billing']['SERVICEFEE'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td> </td>
            <td> </td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_servicefee += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }
      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-only"><?php echo number_format(@$total_servicefee,2) ?></td>
      </tr>
      <tr>
        <td colspan="5">ASSERTION</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['ASSERTION'])){
      $total_assertion = 0;
        foreach ($bill['billing']['ASSERTION'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td> </td>
            <td> </td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_assertion += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }
      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-only"><?php echo number_format(@$total_assertion,2) ?></td>
      </tr>
      <tr>
        <td colspan="5">RETRIEVAL</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['RETRIEVAL'])){
      $total_retrieval = 0;
        foreach ($bill['billing']['RETRIEVAL'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td>".$val['quantity']."</td>
            <td>".__peso($val['unit_price'])."</td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_retrieval += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }
      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-only"><?php echo number_format(@$total_others,2) ?></td>
      </tr>
      <tr>
        <td colspan="5">OTHERS</td>
        <td class="left-only"></td>
      </tr>
      <?php
      if(!empty($bill['billing']['OTHERS'])){
      $total_others = 0;
        foreach ($bill['billing']['OTHERS'] as $val) {
          echo "<tr class='text-center'>
            <td>".__date($val['date_billed'],"d-M-y")."</td>
            <td>".$val['item_name']."</td>
            <td>".$val['quantity']."</td>
            <td>".__peso($val['unit_price'])."</td>
            <td>".__peso($val['total_price'])."</td>
            <td class='left-only'></td>
          </tr>";
          if(is_numeric($val['total_price'])){
            $total_others += $val['total_price'];
            $total_price += $val['total_price'];
          }
        }
      }
      ?>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: right">-</td>
        <td class="left-down"><?php echo number_format(@$total_others,2) ?></td>
      </tr>
      <tr class="no-border">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td class="text-center"><?php echo number_format(@$total_price,2) ?></td>
      </tr>
    </table>
    <br>
    <table style="width:100%; border-collapse: collapse;">
      <tr class="text-center">
        <td colspan="2">Prepared By:</td>
        <td colspan="2">Approved By:</td>
        <td style="width:15%"><b>TOTAL</b></td>
        <td class="text-center" style="width:15%; border-bottom: double 3px black;border-top: solid 1px black"><?php echo number_format(@$total_price,2) ?></td>
      </tr>
      <tr class="text-center">
        <td colspan="6">&nbsp;&nbsp;&nbsp;</td>
      </tr>
      <tr class="text-center">
        <td colspan="2"><?php echo strtoupper(@$bill['prepared_by'])?></td>
        <td colspan="2"><?php echo strtoupper(@$bill['approved_by'])?></td>
        <td ></td>
        <td ></td>
      </tr>
      <tr class="text-center">
        <td colspan="6">&nbsp;&nbsp;&nbsp;</td>
      </tr>
      <tr class="text-center">
        <td colspan="3"></td>
        <td >Due Date:</td>
        <td ><?php echo __date(@$bill['due_date'],"d-M-y")?></td>
        <td ></td>
      </tr>
       <tr class="text-center">
        <td colspan="3"></td>
        <td >Print Date:</td>
        <td ><?php echo __date(@$bill['print_date'],"d-M-y")?></td>
        <td ></td>
      </tr>
      
    </table>
    <br>
    <i>*Please be reminded that late payments will be charged with the appropriate penalty pursuant to the Admin contract.</i>
  </body>
</html>
