<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// die(var_dump($user_list));
?>


<form action="" method="POST">
<div id="corporate-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="inspection_modal" style="width:100%;max-width: 500px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">New Company</h4>
      </div>
      <div class="modal-body" >
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <?php
            echo bootstrap_form("company_name",       "Company/Corporate",    "", array("mreq"=>true,"class"=>"col-md-12"));
          ?>

          <div class="col-md-6"></div>
          <div class="col-md-6">
            <button  class="btn btn-lg btn-primary btn-block" type="submit" name="save_company" id="save_company" value="1" disabled> <span class="glyphicon glyphicon-ok"></span> Save</button>
          </div>
        </div>         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#company_name").keypress(function(event){
    if($(this).val()){
      $("#save_company").removeAttr("disabled");
    }else{
      $("#save_company").attr("disabled","disabled");
    }
  });
</script>
<div id="city-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="inspection_modal" style="width:100%;max-width: 500px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">New City</h4>
      </div>
      <div class="modal-body" >
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <?php
            echo bootstrap_form("city_name",       "City/Location",    "", array("mreq"=>true,"class"=>"col-md-12"));
          ?>

          <div class="col-md-6"></div>
          <div class="col-md-6">
            <button  class="btn btn-lg btn-primary btn-block" type="submit" name="save_city" id="save_city" value="1" disabled> <span class="glyphicon glyphicon-ok"></span> Save</button>
          </div>
        </div>         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>
<script type="text/javascript">
  $("#city_name").keypress(function(event){
    if($(this).val()){
      $("#save_city").removeAttr("disabled");
    }else{
      $("#save_city").attr("disabled","disabled");
    }
  });
</script>
<div id="office-modal" class="modal fade" role="dialog">
  <div class="modal-dialog" id="inspection_modal" style="width:100%;max-width: 500px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">New Branch/Office</h4>
      </div>
      <div class="modal-body" >
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <?php
            echo bootstrap_form("office_name",       "Branch/Office",    "", array("mreq"=>true,"class"=>"col-md-12"));
          ?>

          <div class="form-group col-md-12">
            <div class="form-group col-md-6">
              <label for="company_assigned" class="control-label">Company Assigned<b class="req">*</b></label> 
              <select  class="form-control "  name="company_assigned" id="company_assigned">
                <option value="">-None-</option>
                <?php
                if(!empty($company_list)){
                  foreach($company_list as $row){
                    echo "<option value='".$row['company_id']."''>".$row['company_name']."</option>";
                  }
                }
                ?>
              </select>
            </div>
            <div class="form-group col-md-6">
              <label for="company_assigned" class="control-label">Location Assigned<b class="req">*</b></label> 
              <select  class="form-control "  name="city_assigned" id="city_assigned">
                <option value="">-None-</option>
                <?php
                if(!empty($location_list)){
                  foreach($location_list as $row){
                    echo "<option value='".$row['city_id']."''>".$row['city_name']."</option>";
                  }
                }
                ?>
              </select>
            </div>
          </div>

          <div class="col-md-6"></div>
          <div class="col-md-6">
            <button  class="btn btn-lg btn-primary btn-block" type="submit" name="save_office" id="save_office" value="1" disabled> <span class="glyphicon glyphicon-ok"></span> Save</button>
          </div>
        </div>         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>
</form>
<script type="text/javascript">
  $("#company_assigned,#city_assigned,#office_name").change(function(){
    $val1 = $("#company_assigned").val();
    $val2 = $("#city_assigned").val();
    $val3 = $("#office_name").val();
    if($val1 && $val2 && $val3){
      $("#save_office").removeAttr("disabled");
    }else{
      $("#save_office").attr("disabled","disabled");
    }
  });

</script>

<div class="container dashboard-container">  
<h1 style="margin-top: 10px;">Branches and Offices</h1>
</div>

<form id="disableform" method="POST" action="">
<div class="container dashboard-container">  

  <div class="col-md-6">
    <div class="table-responsive">
      <table class="table table-bordered table-hover dashboardTable">
        <thead>          
          <tr class="tbhead">
            <th>#</th>
            <th>Company/Corporate Name</th>
            <th style="width: 50px;">Delete</th>
          </tr>
        </thead>   
        <tbody  id="company-list-container">          
          <?php 
            if(!empty($company_list)){
              $page = 0;
              foreach ($company_list as $row) {
                $page++;
                echo"
                <tr class='' data-target='$row[company_id]'>
                  <td>$page</td>
                  <td>$row[company_name]</td>
                  <td class='text-center'>
                    <button type='submit' name='delete_company' value='$row[company_id]' class='btn btn-danger btn-xs'>
                      <span class='glyphicon glyphicon-trash'></span>                       
                    </button>
                  </td>
                </tr>";
              }
            }
            else{
              echo"
                <tr class='text-center unavailable '>
                  <td colspan='3'>There are no records...</td>
                </tr>";
            }


          ?>

            <tr>
              <td colspan="3" class="padless">
                <div class="col-md-12 padless">
                  <a class="btn btn-block btn-warning staff-button" data-toggle="modal" data-target="#corporate-modal"> <span class="glyphicon glyphicon-plus"> </span> Add New Company</a>
                </div>
              </td>
            </tr>

          </tbody>   
      </table>
    </div>    
  </div>

  <div class="col-md-6">
    <div class="table-responsive">
      <table class="table table-bordered table-hover dashboardTable">
        <thead>          
          <tr class="tbhead" >
            <th>#</th>
            <th>City/Location</th>
            <th style="width: 30px;">Delete</th>
          </tr>
        </thead>   
        <tbody  id="city-list-container">          
          <?php 
            if(!empty($location_list)){
              $page = 0;
              foreach ($location_list as $row) {
                $page++;
                
                echo"
                <tr class='clickable-tr-disable pointer' data-target='$row[city_id]'>
                  <td>$page</td>
                  <td>$row[city_name]</td>
                  <td class='text-center'>
                    <button type='submit' name='delete_city' value='$row[city_id]' class='btn btn-danger btn-xs'>
                      <span class='glyphicon glyphicon-trash'></span>                       
                    </button>
                  </td>
                </tr>";
              }
            }
            else{
              echo"
                <tr class='text-center unavailable '>
                  <td colspan='3'>There are no records...</td>
                </tr>";
            }
          ?>

            <tr>
              <td colspan="3" class="padless">
                <div class="col-md-12 padless">
                  <a class="btn btn-block btn-warning staff-button" data-toggle="modal" data-target="#city-modal"> <span class="glyphicon glyphicon-plus"> </span> Add New City</a>
                </div>
              </td>
            </tr>
          </tbody>   
      </table>
    </div>    
  </div>
</div>

<div class="container dashboard-container">  

  <div class="col-md-12">
    <div class="alert alert-info col-md-12">
      <div class="form-group col-md-12">
        <b>Filter Branch/Office:</b><br>
      </div>

      <div class="form-group col-md-12 marginless">
        <div class="form-group col-md-4">
          <label for="company_assigned" class="control-label">Company Assigned<b class="req">*</b></label> 
          <select  class="form-control "  id="filter_company">
            <option value="">-None-</option>
            <?php
            if(!empty($company_list)){
              foreach($company_list as $row){
                echo "<option value='".$row['company_id']."''>".$row['company_name']."</option>";
              }
            }
            ?>
          </select>
        </div>
        <div class="form-group col-md-4">
          <label for="location_filter" class="control-label">Location Assigned<b class="req">*</b></label> 
          <select  class="form-control "   id="filter_city">
            <option value="">-None-</option>
            <?php
            if(!empty($location_list)){
              foreach($location_list as $row){
                echo "<option value='".$row['city_id']."''>".$row['city_name']."</option>";
              }
            }
            ?>
          </select>
        </div>
        <?php
          echo bootstrap_form("filter_branch", "Filter by Name", "",array("class"=>"col-md-4"));
        ?>
      </div>

    </div>

  </div>
  <div class="col-md-12">
    <div class="table-responsive">
      <table class="table table-bordered table-hover dashboardTable">
        <thead>          
          <tr class="tbhead">
            <th>#</th>
            <th>Company/Corporate </th>
            <th>City</th>
            <th>Branch/Office Name</th>
            <th style="width:50px">Delete</th>
          </tr>
        </thead>   
        <tbody  id="company-list-container">          
          <?php 
            if(!empty($branch_list)){
              $page = 0;
              foreach ($branch_list as $row) {
                $page++;
                echo"
                <tr class='clickable-tr-disable pointer' data-target='$row[branch_id]'>
                  <td>$page</td>
                  <td>$row[company_name]</td>
                  <td>$row[city_name]</td>
                  <td>$row[branch_name]</td>
                  <td class='text-center'>
                    <button type='submit' name='delete_branch' value='$row[branch_id]' class='btn btn-danger btn-xs'>
                      <span class='glyphicon glyphicon-trash'></span>                       
                    </button>
                  </td>
                </tr>";
              }
            }
            else{
              echo"
                <tr class='text-center unavailable '>
                  <td colspan='5'>There are no records...</td>
                </tr>";
            }
          ?>
          </tbody>   

          <tr>
            <td colspan="5" class="padless">
              <div class="col-md-12 padless">
                <a class="btn btn-block btn-warning staff-button" data-toggle="modal" data-target="#office-modal"> <span class="glyphicon glyphicon-plus"> </span> Add New Office</a>
              </div>
            </td>
          </tr>
      </table>
    </div>    
  </div>
</div>  
</form>

<script type="text/javascript">
  $("#nav-office").addClass("active");

  //delete prompt
  $("#disableform").submit(function(event){
    var r=confirm("Are you sure to delete this entry?\nThis is irreversable.");
    $target_id = $(this).data('id');

      if (r==false){
        console.log("canceled");
        event.preventDefault();
      }
  });
  


</script>