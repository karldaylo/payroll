<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
$(document).ready(function(){

$("#username").attr('readonly','readonly');
$("#password").attr('readonly','readonly');
$("#password").val('fakepassword');
$("#confirm_password").attr('readonly','readonly');
$("#confirm_password").val('fakepassword');
<?php
  echo js_input('username'          ,@$user['username']);
  echo js_input('email'             ,@$user['email']);
  echo js_input('contact'           ,@$user['mobile']);
  echo js_input('designation'       ,@$user['designation']);
  echo js_input('department'        ,@$user['department']);
  echo js_input('office'            ,@$user['office']);
  echo js_input('name'              ,$user['fullname']);
  echo js_input('office_code'       ,@$user['office_code']);
  
  if($user['admin']==1&&$user['staff']==1){
    echo js_input('is_admin'          ,2);
  }elseif($user['admin']==0&&$user['staff']==1){
    echo js_input('is_admin'          ,1);
  }else{
    echo js_input('is_admin'          ,0);
  }
?>
$("#output").attr('src',"<?php echo base_url("/userimg/".$user["img"]);?>");

});
</script>