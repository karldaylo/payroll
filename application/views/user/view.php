<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form method="POST" action="">


<div class="container  dashboard-container">
    
    <div class="col-md-12 text-center">
      <img src="<?php echo base_url("/userimg/").$user['img']?>" style="width:200px" id="output"/>
    </div>

    <div class="col-md-12" >

      <div class="col-sm-12 padless text-center"><b style="font-size: 30px"><?php echo $user['fullname'] ?></b></div>
      <div class="col-sm-12 padless text-center"><b style="font-size: 15px"><?php echo $user['company_assigned'] ?></b></div>
      <div class="col-sm-12 padless text-center"><?php echo $user['company_branch'] ?></div>
      <div class="col-sm-12 padless text-center"><?php echo $user['email'] ?></div>
       
      <div class="col-sm-12 padless text-center" id="edit-buttons">
        <a class="btn  btn-warning" href="<?php echo base_url('employee/edit/'.$id);?>">
          <span class="glyphicon glyphicon-edit"></span> Edit User Info
        </a>
        <a class="btn  btn-primary" href="<?php echo base_url('user/settings/'.$id)?>">
          <span class="glyphicon glyphicon-lock"></span> Change Password
        </a>
      </div>
      
    </div>

</div>
  <script type="text/javascript">
    <?php
      if(empty($this->session->admin)){//if user is not admin
        echo "$('#edit-buttons').remove();";
      }
    ?>
  </script>

<?php
if(!empty($this->session->admin)){//if user is admin
?>
<div class="container handle-container" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
      <thead>
        <tr>
          <th class="text-center tbhead" colspan="2">Logs <a class="btn btn-xs btn-primary pull-right"  href="#" onClick="MyWindow3=window.open('<?php echo base_url('report/userlog/'.$id);?>','MyWindow3','width=800,height=500'); return false;">View all logs</a></th>
        </tr>
        <tr>
          <th class="datetable">Date</th>         
          <th >Description</th>
        </tr>
      </thead>
      <tbody id="log-records">
        <?php
          if(empty($user['logs'])){
            echo "<tr><td colspan='2' class='unavailable text-center'>No Records</td></tr>";
          }else{
            foreach ($user['logs'] as $val) {
              echo "<tr>
              <td>".__date($val['date_created'])."</td>
              <td>".$val['description']."</td>
              </tr>";
            }
          }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
}//end of logs
?>

</form>
<script type="text/javascript">
  $("#nav-account").addClass("active");
</script>