<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form id="searchbox" method="GET" action="<?php echo base_url('user/search/')?>">
  <div class="container button-container">   

      <div class="col-sm-3 padless ">
        <a class="btn btn-success btn-lg btn-block" id="add-user" href="<?php echo base_url("user/register")?>"> <span class="glyphicon glyphicon-plus"></span> Register User</a>
      </div>
      <div class="col-sm-1 hidden-xs padless ">
      </div>
      <div class="col-sm-8 padless ">
        <div class="input-group pull-right">
              <input id="search" type="text" name="q" class="form-control input-lg" placeholder="Search for User...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-default btn-lg" id="search_button"><span class="glyphicon glyphicon-search"></span> Search</button>
              </span>
          </div>
      </div>
  </div>
  <script type="text/javascript">
    <?php
      if(empty($this->session->admin)){//if user is staff
        echo "$('#add-user').remove();";
        echo "$('#download-user').remove();";
      }
    ?>
  </script>
</form>

<div class="container dashboard-container">  
  <div class="table-responsive">
    <table class="table table-bordered table-hover dashboardTable">
      <thead>          
        <tr class="tbhead">
          <th>#</th>
          <th>Name</th>
          <th>Username</th>
          <th class="hidden-sm hidden-xs">Designation</th>
          <th>Base Office</th>
        </tr>
      </thead>   
      <tbody  id="user-list-container">          
        <?php 
          if(!empty($user_list)){
            $page = 0;
            foreach ($user_list as $row) {
              $page++;
              $row_class    = "";
              echo"
              <tr class='clickable-tr pointer' data-target='$row[uid]'>
                <td>$page</td>
                <td>$row[fullname]</td>
                <td>$row[username]</td>
                <td class='hidden-sm hidden-xs'>$row[designation]</td>
                <td>$row[office_code]</td>
              </tr>";
            }
          }
          else{
            echo"
              <tr class='text-center unavailable '>
                <td colspan='5'>There are no inventory...</td>
              </tr>";
          }
        ?>
        </tbody>   
    </table>
      <?php
        if(count($user_list)>=$limit && $limit != 0){
        echo '<div class="col-xs-12 text-center pagination marginless" >
          <a class="btn btn-primary" id="nextPage" role="button" >Next Page</a>
        </div>';}
      ?>
      <div class="col-xs-12 text-center pagination marginless" id="noMore"  style="display: none">
        <i id="lastPage">No more Results</i>
      </div>
      <div class="col-xs-12 text-center pagination marginless" id="loadMore" style="display: none">
        <img class="ajaxLoad" src="<?php echo base_url('assets/img/load.gif'); ?>"/>
      </div>
  </div>    
</div>

<script type="text/javascript">
  $("#nav-account").addClass("active");

  $("#user-list-container").on("click",".clickable-tr",function (){
    $target_id = $(this).data("target");
    window.location.href='<?php echo base_url("/user/view")?>/'+$target_id;
  });
  $(document).ready(function () {  

      var pagination = 1;
      var limit = <?php echo $limit; ?>;
      var page  = <?php echo $page; ?>;

      //highlight and notify settings
      var page_display = limit;
      $("#nextPage").click(function () {
        $("#nextPage").hide();
        $("#loadMore").show();
        $.getJSON('<?php echo base_url('ajax/user')?>/'+limit+'/'+pagination, function(data){
          console.log(data['user']);
          $append_data = "";
          data['user'].forEach(function(row,key){
            page++;

              $append_data = $append_data+"<tr class='clickable-tr pointer' data-target='"+row['uid']+"'><td>"+page+"</td><td>"+row['last_name']+", "+row['first_name']+" "+row['middle_name']+"</td><td>"+row['username']+"</td><td>"+row['designation']+"</td><td>"+row['department']+"</td><td>"+row['office']+"</td></tr>"
          });
          
          //config ends
          $("#user-list-container").append($append_data);
          $("#loadMore").hide();
          //if there are more to fetch, prepare for next page 
          if(data['count']>=limit){
            $("#nextPage").show();
            pagination++;
          }
          else{
            $("#nextPage").hide();
            $("#noMore").show();
          }
        });
      });
    });
</script>