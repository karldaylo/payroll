<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form method="POST" action="" enctype="multipart/form-data">
<div class="container  dashboard-container">
  <div class="text-center" style="width:100%;"><h2>Employee Record</h2></div>
  
  <fieldset>
    <legend><h3>Login Information</h3></legend>
    <?php
      echo bootstrap_form("username",           "Username",                 "",array("req"=>true,"class"=>"col-md-12"));
      echo bootstrap_form("password",           "Password",                 "1234",array("req"=>true,"class"=>"col-md-12","type"=>"password"));
      echo bootstrap_form("confirm_password",   "Confirm Password",         "1234",array("req"=>true,"class"=>"col-md-12","type"=>"password"));

    ?>
    <!-- <div class="col-sm-12">
      <div class="form-group col-md-12">
        <label for="is_admin" class="control-label">User Type</label> 
        <select class="form-control"  name="is_admin" id="is_admin">
          <option value="0">Medical Staff</option>
          <option value="1">Administration Staff</option>
          <option value="2">Chief / Director</option>

        </select>
      </div>
    </div> -->

  </fieldset>
</div>

<div class="container  dashboard-container">
  <fieldset>
    <legend><h3>User Information</h3></legend>
    <div class="col-sm-2">
      <img src="<?php echo base_url("/assets/img/img.png")?>" style="width:100%" id="output"/>
    </div>
    <div class="col-sm-10">
      <div class="form-group">
        <label for="attach_insenrollform" class="control-label">Photo (optional)</label> 
        <input  class="form-control padless attach-group" type="file" name="img_preview" id="img_preview" style="height:28px" accept="image/*" onchange="loadFile(event)">
      </div>
    </div>
    <div class="col-sm-12">
    </div>
    

    <?php
      echo bootstrap_form("name",        "Full Name",             "",array("req"=>true,"class"=>"col-md-12"));
      echo bootstrap_form("designation",  "Designation/Position", "",array("class"=>"col-md-6"));
      echo bootstrap_form("contact",      "Contact Number",       "",array("class"=>"col-md-6"));
      echo bootstrap_form("email",        "Email adress",         "",array("class"=>"col-md-6"));
    ?>

    <div class="col-sm-6">
      <div class="form-group col-md-12">
        <label for="is_admin" class="control-label">Branch Office <b class="req">*</b></label> 
        <select class="form-control"  name="office_code" id="office_code">
          <option value="">-select branch office-</option>
          <?php
            foreach ($branch as $value) {
          
              echo "<option value='".$value['office_code']."'>".$value['office_description']."</option>";
            }
          ?>
        </select>
      </div>
    </div>

  </fieldset>
</div>


<div class="container  button-container">
  <div class="col-md-6">
    <a class="btn btn-lg btn-default btn-block" href="<?php echo base_url("/user/")?>"> <span class="glyphicon glyphicon-share-alt"></span> Return to User List</a>
  </div>
  <div class="col-md-6">
    <button  class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="submit"> <span class="glyphicon glyphicon-ok"></span> Save Account</button>
  </div>
</div>

</form>
<script type="text/javascript">
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };

  $("#nav-employee").addClass("active");

</script>
