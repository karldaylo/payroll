<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
  $("#history-table").remove();
  $("#logs-table").remove();
</script>


<form method="POST" action="">
<div class="container dashboard-container" >
  <fieldset>
    <legend><h3>Change Password</h3></legend>
      <div class="col-md-12">
      <?php
        if(!empty($this->session->flashdata('display_result'))){
          echo '<div class="alert alert-warning" role="alert" ><span class="glyphicon glyphicon-exclamation-sign"> </span> ';
              echo $this->session->flashdata('display_result');         
            echo '</div>';
        }
      ?>
      <?php
        if(empty($this->session->admin) || $id == $this->session->uid){
          echo bootstrap_form("oldpass",      "Current Password",    "",array("mreq"=>true,"class"=>"col-md-12","type"=>"password"));
        }
        echo bootstrap_form("newpass",        "New Password",        "",array("mreq"=>true,"class"=>"col-md-12","type"=>"password"));
        echo bootstrap_form("confirm_newpass","Confirm New Password","",array("mreq"=>true,"class"=>"col-md-12","type"=>"password"));
      ?>
      </div>
      <br>
      <div class="col-md-3">
        
      </div>
      <div class="col-md-6">
        <button class="btn btn-lg btn-block btn-primary" type="submit" name="change_pass" value="1">Change Password</button>
      </div>
  </fieldset>
</div>
</form>