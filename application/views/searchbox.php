<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form id="searchbox" method="GET" action="<?php echo base_url('/search/')?>">
	<div class="container button-container">      
      <div class="col-sm-12 padless pull-right">
        <div class="input-group pull-right">
              <input id="search" type="text" name="q" class="form-control input-lg" placeholder="Search for Inventory...">
              <span class="input-group-btn">
                <button type="submit" class="btn btn-default btn-lg" id="search_button"><span class="glyphicon glyphicon-search"></span> Search</button>
              </span>
          </div>
      </div>
  </div>
</form>

