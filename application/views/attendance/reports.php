<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php
		__css('assets/css/normalize.css');
		__css('assets/css/bootstrap.min.css');
		__css('assets/css/jquery-ui.css');

		__js('assets/js/jquery.min.js');
		__js('assets/js/jquery-ui.js');

	?>
	<style type="text/css">
		.infoLabels{
			font-weight: bold;
		}
		.padless,.nopadding{
			padding: 0px !important;
		}
		.marginless,.nomargin{
			margin:0px !important;
		}
	</style>
</head>
<form action="" method="GET">

<div class="col-md-12 row">
	<?php
	$name = "";
	
	// $name .= $data['first_name']." ".$data['middle_name']." ".$data['last_name'];
    echo bootstrapalize_3("Employee Name:"     	,$data['fullname']);
    echo bootstrapalize_3("Date Range:"     	,__date($start_date,"M j, Y")." - ".__date($end_date,"M j, Y"));
	?>
	<br>
</div>


<div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
    	<thead>
    		<tr>
    			<th>Date</th>
    			<th>Time In</th>
    			<th>Time Out</th>
    			<th>UT?</th>
    			<th>OT?</th>
                <th>Holiday</th>
    			<th>Rate</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    		$total = 0;
    		if(!empty($attendance_list)){
    			foreach ($attendance_list as $val) {
    				
                    $ot1 = strtotime( $val['overtime_in'] );
                    $ot2 = strtotime( $val['overtime_out'] );
                    $otdiff = $ot1 - $ot2;
                    $othours = $otdiff / ( 60 * 60 );
                    if($othours < 0){
                        $othours = $othours * -1;
                    }
                    $ot = "No";
                    if($othours > 0){
                        $ot = $othours."HR";
                    }
    				echo "
    				<tr>
    					<td>".__date($val['att_date'],"m/d/Y")."</td>
    					<td>".__date($val['time_in'],"h:i A")."</td>
    					<td>".__date($val['time_out'],"h:i A")."</td>
    					<td>".($val['is_ut']?"Yes":"No")."</td>
    					<td>".($ot)."</td>
                        <td>".$val['holiday']."</td>
    					<td>537</td>
    				</tr>";
    				$total +=537;
    			}
    		}
    		?>
    	</tbody>
    </table>


</div>
<div class="col-md-12 row">
<b style="font-size: 20px">TOTAL: <?php echo number_format($total,2)?></b>
</div>
</form>