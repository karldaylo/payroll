<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form method="POST" action="">




<div class="container  dashboard-container">
  <fieldset>
    <legend>Attendance </legend>
<div class="col-sm-12 alert alert-warning">

  <div class="form-group col-md-12">
    <label for="user_id" class="control-label">Employee</label> 
    <select required class="form-control select2"  name="user_id" id="user_id">
      <option value="">Select an Employee</option>
      <?php
        foreach ($employee_list as $key => $value) {
          echo "<option value='".$value['user_id']."'>".$value['last_name'].", ".$value['first_name']." ".$value['middle_name']."</option>";
        }
      ?>
    </select>
  </div>

  <div class="form-group col-md-4">
    <label for="target_month" class="control-label">Month</label> 
    <select required class="form-control "  name="target_month" id="target_month">
      <option value="01">January</option>
      <option value="02">February</option>
      <option value="03">March</option>
      <option value="04">April</option>
      <option value="05">May</option>
      <option value="06">June</option>
      <option value="07">July</option>
      <option value="08">August</option>
      <option value="09">September</option>
      <option value="10">October</option>
      <option value="11">November</option>
      <option value="12">December</option>
    </select>
  </div>

  <div class="form-group col-md-4">
    <label for="target_semi" class="control-label">Date</label> 
    <select required class="form-control "  name="target_semi" id="target_semi">
      <option value="1">Days 1 to 15</option>
      <option value="2">Days 15 to end of month</option>
    </select>
  </div>

  <div class="form-group col-md-4">
    <label for="target_year" class="control-label">Year</label> 
    <select required class="form-control "  name="target_year" id="target_year">
      <option>2020</option>
      <option>2021</option>
      <option>2022</option>
      <option>2023</option>
      <option>2024</option>
      <option>2025</option>
      <option>2026</option>
      <option>2027</option>
      <option>2028</option>
      <option>2029</option>
    </select>
  </div>
  
</div>

<script type="text/javascript">
  <?php
  $month_today = date("m");
  $year_today  = date("Y");
  $day_today  = date("d");
  $target_date=1;
  if($day_today>15){
    $target_date=2;
  }
  echo js_input("target_month",$month_today);
  echo js_input("target_semi",$target_date);
  echo js_input("target_year",$year_today);
  ?>
</script>



<div class="col-sm-12 alert alert-info">

  <?php
    echo bootstrap_form("total_days",  "Total Days Absent (required)"       ,"0"      ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.5",));
    echo bootstrap_form("night_diff",  "Total Night Differential(in hours)" ,"0"       ,array("class"=>"col-md-4 calc","type"=>"number"));
    echo bootstrap_form("total_vl",  "VL incentive remaining (Peso)"        ,"0" ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.01",));
    echo bootstrap_form("total_ut",  "Total UT (in hours)"   ,"0" ,array("class"=>"col-md-4 calc","type"=>"number"));
    echo bootstrap_form("total_ot",  "Total OT (in hours)"   ,"0" ,array("class"=>"col-md-4 calc","type"=>"number"));
    echo bootstrap_form("salary_adjustment",  "Salary Adjustment (negative if deduction)"          ,"0.00" ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.01"));
  ?>


  <div class="col-md-4 ">
    <div class="form-group col-md-12 ">
      <label for="rest_days" class="control-label">Restday(in hours)</label>
      <div class="input-group">
        <input type="number" class="form-control calc" id="rest_days" name="rest_days" placeholder="Total Hours" value="" >
        <input type="number" class="form-control calc" id="rest_days_ot" name="rest_days_ot" placeholder="Total OT Holiday(in hours)" value="" >
      </div>
    </div>
  </div>

  <div class="col-md-4 ">
    <div class="form-group col-md-12 ">
      <label for="legal_holiday" class="control-label">Legal Holiday(in hours)</label>
      <div class="input-group">
        <input type="number" class="form-control calc" id="legal_holiday" name="legal_holiday" placeholder="Total Hours" value="" >
        <input type="number" class="form-control calc" id="legal_holiday_ot" name="legal_holiday_ot" placeholder="Total OT Holiday(in hours)" value="" >
      </div>
    </div>
  </div>

  <div class="col-md-4 ">
    <div class="form-group col-md-12 ">
      <label for="special_holiday" class="control-label">Special Holiday(in hours)</label>
      <div class="input-group">
        <input type="number" class="form-control calc" id="special_holiday" name="special_holiday" placeholder="Total Hours" value="" >
        <input type="number" class="form-control calc" id="special_holiday_ot" name="special_holiday_ot" placeholder="Total OT Holiday(in hours)" value="" >
      </div>
    </div>
  </div>

</div>


 

<div class="col-sm-12 alert alert-danger">
  <?php
  echo bootstrap_form("pagibig_deduc",      "Pag-ibig Donation (in Peso)"   ,"0" ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.01"));
  echo bootstrap_form("philhealth_deduc",   "Philhealth Deduction (in Peso)" ,"0" ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.01"));
  echo bootstrap_form("sss_deduc",          "SSS Deduction (in Peso)"        ,"0" ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.01"));

  //loans
  echo bootstrap_form("pagibig_loan",      "Pag-ibig Loans (in Peso)"   ,"0" ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.01"));
  echo bootstrap_form("philhealth_loan",   "Philhealth Loans (in Peso)" ,"0" ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.01"));
  echo bootstrap_form("sss_loan",          "SSS Loans (in Peso)"        ,"0" ,array("class"=>"col-md-4 calc","type"=>"number","step"=>"0.01"));
?>
</div>


</div>


<div class="container handle-container" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
      <thead>
        <tr>
          <th class="text-center tbhead" style="background-color:goldenrod !important" colspan="3">Others<a class="btn btn-xs btn-success pull-right" id="add-traintb"><span class="glyphicon glyphicon-plus"></span> Add Others</a></th>
        </tr>
        <tr>
          <th >Amount in Peso(negative if deduction)</th>         
          <th >Description</th>
          <th ></th>
        </tr>
      </thead>
      <tbody id="train-content">
        <tr id="wx0">
          <td><input name='other_price[0]' class='form-control calc_other' type='number' step='0.01' value='0'></td>
          <td><input name='other_description[0]'  class='form-control'></td>
          <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash'></span></a></td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  var tr_count = 0;
  $("#add-traintb").click(function(){
    tr_count++;
    var nexttw="<tr id='tr"+tr_count+"'> <td><input name='other_price["+tr_count+"]' class='form-control calc_other' type='number' step='0.01' value='0'></td> <td><input name='other_description["+tr_count+"]' class='form-control'></td> <td><a class='btn btn-danger delete-row'><span class='glyphicon glyphicon-trash '></span></a></td></tr>"
    $("#train-content").append(nexttw);

  });

  $("#train-content").on('click',".delete-row",function(){
    $(this).parent().parent().remove();
  });
</script>

<div class="container  button-container">
  <div class="col-md-6">
    <a class="btn btn-lg btn-default btn-block" href="<?php echo base_url("/")?>"> <span class="glyphicon glyphicon-share-alt"></span> Return to Dashboard</a>
  </div>
  <div class="col-md-6">
    <button  class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="submit"> <span class="glyphicon glyphicon-ok"></span> Save Attendance</button>
  </div>
</div>

<div class="container handle-container" id="logs-table">
  <div class="col-sm-12 alert alert-success">
    <b style="font-size: 20px"><u>Realtime Calculations:</u></b><br>
    <p id="calcdescription" style="padding-left:50px"></p>
    <p id="calcval" style="padding-left:50px">
      <table style="width:100%">
        <tr style="font-weight: bold;">
          <td>Gross Earnings </td>
          <td id="display_gross"></td>
          <td>Total Deductions</td>
          <td id="display_deduction"></td>
          <td>Total Others</td>
          <td id="display_others"></td>
          <td>Net Earnings</td>
        </tr>
        <tr>
          <td>Regular Pay </td>
          <td id="display_semi"></td>
          <td>Absences </td>
          <td id="display_absent"></td>
          <td>Salary Adjustment</td>
          <td id="display_adjustment"></td>
          <td><b  style="color:red">Php <span id="display_total">0.00</span></b></td>
        </tr>
        <tr>
          <td>Overtime </td>
          <td id="display_ot"></td>
          <td>Undertime/Tardiness </td>
          <td id="display_ut"></td>
          <td>VL Incentive</td>
          <td id="display_vl"></td>
          <td></td>
        </tr>
        <tr>
          <td>Legal Holiday </td>
          <td id="display_legal_hd"></td>
          <td>SSS Contribution </td>
          <td id="display_sss"></td>
          <td>Others</td>
          <td id="display_etc"></td>
          <td></td>
        </tr>
        <tr>
          <td>Special Holiday</td>
          <td id="display_special_hd"></td>
          <td>PHIC Contribution </td>
          <td id="display_philhealth"></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>Restday + Holiday</td>
          <td id="display_rest_hd"></td>
          <td>HMDF Contribution </td>
          <td id="display_pagibig"></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td>Night Differential</td>
          <td id="display_night_diff"></td>
          <td>Loans</td>
          <td id="display_loan"></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
      </table>

    </p>
  </div>

  </fieldset>
</div>  


</form>
<script type="text/javascript">
  $("#nav-attendance").addClass("active");
  $(".dateonly").each(function (){
    $(this).find("input").datepicker({
      format:"YYYY-MM-DD",
    });
  });
  $(".timeonly").each(function (){
    $(this).find("input").datetimepicker({
      format:"LT",
    });
  });
    $("#user_id").select2();
</script>


<script type="text/javascript">
//declare placeholder
$employee = "";
$check_exist_attendance = "";

$semi_month                = 0;
$daily_wage                = 0;
$hourly_rate               = 0;
$legal_holiday             = 0;
$ot_legal_holiday          = 0;
$ot_rate                   = 0;
$ot_restday_regular_holiday = 0;
$ot_special_holiday        = 0;
$restday_regular_holiday   = 0;
$special_holiday           = 0;
$ut_rate                   = 0;
$nd_rate                   = 0;
$vl_rate                   = 0;
$previous_absent           = 0;
$sss_deduction             = [];
$pagibig_deduction         = [];
$philhealth_deduction      = [];
function refresh_employee(){
  $uid        = $("#user_id").val();
  $att_month  = $("#target_month").val();
  $att_date   = $("#target_semi").val();
  $att_year   = $("#target_year").val();

  //Get user's Data
  $.get({
    type: 'POST',
    url: '<?php echo base_url("ajax/getemployee/")?>',
    data: {att_month:$att_month,att_date:$att_date,att_year:$att_year,uid:$uid},
    dataType: 'json',
    async: false, 
    success: function(data){

      $employee=data;
      console.log(data);
      if($employee['attendance'] == 1){
        alert("This employee already have records of the selected dates. Saving this will overwrite the attandance.");
      }
    }
  });

  if($employee['uid']){
    if($att_date == 2 && !$employee['previous_attendance']){
      alert("The employee has not record of the date 1-15 of the selected month year.\nSSS,Philhealth, and HMDF will not be able to get its deduction rates accurately.\nPlease edit if incorrect.");
    }

    $("#calcdescription").html("");

    //
    $semi_month                = $employee['rate']['semi_month'];
    $daily_wage                = $employee['rate']['daily_wage'];
    $hourly_rate               = $employee['rate']['hourly_rate'];
    $legal_holiday             = $employee['rate']['legal_holiday'];
    $ot_legal_holiday          = $employee['rate']['ot_legal_holiday'];
    $ot_rate                   = $employee['rate']['ot_rate'];
    $ot_restday_regular_holiday= $employee['rate']['ot_restday_regular_holiday'];
    $ot_special_holiday        = $employee['rate']['ot_special_holiday'];
    $restday_regular_holiday   = $employee['rate']['restday_regular_holiday'];
    $special_holiday           = $employee['rate']['special_holiday'];
    $ut_rate                   = $employee['rate']['ut_rate'];
    $nd_rate                   = $employee['rate']['nd_rate'];
    $vl_rate                   = $employee['rate']['vl_rate'];
    //$nd_ot_rate                = $employee['rate']['ut_rate'];

    $sss_deduction             = $employee['rate']['sss_deduction'];
    $pagibig_deduction         = $employee['rate']['pagibig_deduction'];
    $philhealth_deduction      = $employee['rate']['philhealth_deduction'];

    if($employee['previous_attendance']){
      $previous_absent              = $employee['previous_attendance']['total_absent'];
    }
  }
}

function update_deduction(){
  $total_days       = $("#total_days").val();
  if($previous_absent>0){
    $total_days       = parseFloat($total_days) + parseFloat($previous_absent);
  }
  $att_date   = $("#target_semi").val();
  if($sss_deduction[$total_days]){
    $("#sss_deduc").val($sss_deduction[$total_days]);
  }else{
    $("#sss_deduc").val(0);
  }

  if($pagibig_deduction[$total_days]){
    $("#pagibig_deduc").val($pagibig_deduction[$total_days]);
  }else{
    $("#pagibig_deduc").val(0);
  }

  if($philhealth_deduction[$total_days]){
    $("#philhealth_deduc").val($philhealth_deduction[$total_days]);
  }else{
    $("#philhealth_deduc").val(0);
  }

  if($att_date==1){
    $("#sss_deduc").val(0);
    $("#pagibig_deduc").val(0);
    $("#philhealth_deduc").val(0);
  }

  $vl_price = 111.88 - ($total_days * $vl_rate);
  $("#total_vl").val($vl_price.toFixed(2));

}

//calculations
function update_calc(){
  $total_days       = $("#total_days").val();
  $total_ut         = $("#total_ut").val();
  $total_ot         = $("#total_ot").val();
  $total_legal_hd   = $("#legal_holiday").val();
  $total_legal_hd_ot= $("#legal_holiday_ot").val();
  $total_special_hd = $("#special_holiday").val();
  $total_special_hd_ot = $("#special_holiday_ot").val();
  $total_rest_hd    = $("#rest_days").val();
  $total_rest_hd_ot = $("#rest_days_ot").val();
  $pagibig_deduc    = $("#pagibig_deduc").val();
  $philhealth_deduc = $("#philhealth_deduc").val();
  $sss_deduc        = $("#sss_deduc").val();
  $salary_adjustment= $("#salary_adjustment").val();
  $night_diff       = $("#night_diff").val();
  $vl_incentive     = $("#total_vl").val();
  $sss_loan         = $("#sss_loan").val();
  $pagibig_loan     = $("#pagibig_loan").val();
  $philhealth_loan  = $("#philhealth_loan").val();

  if($("#user_id").val()){
    //gross
    $display_regular  = parseFloat($semi_month);
    $display_ot       = parseFloat($total_ot * $ot_rate);
    $display_legal_hd = parseFloat($legal_holiday * $total_legal_hd) + parseFloat($ot_legal_holiday * $total_legal_hd_ot);
    $display_rest_hd  = parseFloat($restday_regular_holiday * $total_rest_hd) + parseFloat($ot_restday_regular_holiday * $total_rest_hd_ot);
    $display_special_hd = parseFloat($special_holiday * $total_special_hd) + parseFloat($ot_special_holiday * $total_special_hd_ot);
    $display_night_diff = parseFloat($night_diff * $nd_rate);

    $display_gross    = parseFloat($display_regular) + parseFloat($display_ot) + parseFloat($display_legal_hd) + parseFloat($display_rest_hd) + parseFloat($display_special_hd) + parseFloat($display_night_diff);

    //deduction
    $display_absent   = parseFloat($total_days * $daily_wage);
    $display_ut       = parseFloat($total_ut * $ut_rate);
    $display_loan     = parseFloat($sss_loan) + parseFloat($pagibig_loan) + parseFloat($philhealth_loan);

    $display_deduction = parseFloat($display_absent) + parseFloat($display_ut) + parseFloat($sss_deduc) + parseFloat($pagibig_deduc) + parseFloat($philhealth_deduc) + parseFloat($display_loan);

    //others
    $display_etc = 0;
    $("#train-content").find(".calc_other").each(function(){
      $display_etc = $display_etc + parseFloat($(this).val());
    });  

    $display_others = 0
    $display_others = parseFloat($display_etc) + parseFloat($salary_adjustment) + parseFloat($vl_incentive);

    $display_total = parseFloat($display_gross) - parseFloat($display_deduction) + parseFloat($display_others);

    $("#display_gross").html(parseFloat($display_gross).toFixed(2));
    $("#display_deduction").html(parseFloat($display_deduction).toFixed(2));
    $("#display_others").html(parseFloat($display_others).toFixed(2));

    $("#display_semi").html($display_regular);
    $("#display_absent").html($display_absent);
    $("#display_ot").html($display_ot);
    $("#display_ut").html($display_ut);
    $("#display_night_diff").html(parseFloat($display_night_diff).toFixed(2));

    $("#display_legal_hd").html($display_legal_hd);
    $("#display_rest_hd").html($display_rest_hd);
    $("#display_special_hd").html($display_special_hd);

    $("#display_sss").html($sss_deduc);
    $("#display_philhealth").html($philhealth_deduc);
    $("#display_pagibig").html($pagibig_deduc);

    $("#display_etc").html($display_etc);
    $("#display_adjustment").html($salary_adjustment);
    $("#display_vl").html($vl_incentive);
    $("#display_loan").html($display_loan);

    $("#display_total").html(parseFloat($display_total).toFixed(2));

    $absent_display = "";

    if($previous_absent){
      $absent_display = " <b>Days absent on dates 1-15</b>: "+$previous_absent;
    }
    $("#calcdescription").html("<b>Employee:</b> "+$employee['fullname']+"  <b>Code:</b> "+$employee['employee_type']+" <b>Office:</b> "+$employee['company_assigned']+" "+$employee['company_branch']+$absent_display);
    //check if ssnumber exists
    
    // console.log("gross:"+$display_gross + "  deduc:"+$display_deduction+"   TOTAL:"+$display_total);

  }else{
    //empty user do nothing
    $("#calcdescription").html("<b style='color:SALMON;font-size:20px'>Select a user first!</b>");
  }

}

$("#user_id,#target_month,#target_semi,#target_year").change(function(){
  refresh_employee();
  update_deduction();
  // update_calc();
});

$("#total_days").change(function(){
  update_deduction();
});
$("#total_days").keyup(function(){
  update_deduction();
});

$("input,select").change(function(){
  update_calc();
});
$("input,select").keyup(function(){
  update_calc();
});
$("#train-content").on("keyup",".calc_other",function (){
  update_calc();
});
</script>
