<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
	<title>Spic'n Span - Payroll System</title>
      <?php
		__css('assets/css/bootstrap.min.css');
      	__css("assets/css/login.css");

		__js('assets/js/jquery.min.js');
		__js('assets/js/bootstrap.min.js');

      ?>
</head>

<body>
  	<div class="login-container">
	  	<div class="login-form">
		    <form class="" method="POST">
			    <h3>Spic'n Span - Payroll System</h3>
			    <div class="form-group text-left">

			    <?php
			    if(@$error_display){
			    	echo '<div class="alert alert-danger" role="alert" ><span class="glyphicon glyphicon-exclamation-sign"> </span> ';
			        	echo @$error_display;			    
			        echo '</div>';
			    }
			    ?>

			    </div>
			    <div class="form-group text-left">
				    <label for="email">Username:</label>
			    	<input type="text" placeholder="Username" name="username" required class="form-control"/>
				</div>
				<div class="form-group text-left">
				    <label for="password">Password:</label>
			    	<input type="password" placeholder="Password" name="password" required class="form-control"/>
				</div>
			    <!-- <img src="<?php echo base_url('assets/img/captcha.gif');?>" class=" captcha"> -->

			    <button type="submit" class="btn btn-block btn-primary btn-lg" value="submit"  name="login">Log in</button>
			      
		    </form>
		    <div class="text-center row hidden">
		    	<hr>
		    	 Powered by<br> 
		    	 <img class="" href="#" style='width:30%;' src="<?php echo base_url('assets/img/boh.jpg');?>">
		    </div>
	  	</div>
	</div>

</body>
</html>