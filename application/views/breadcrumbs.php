<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<div class="container button-container padless">
	    <ol class="breadcrumb"> 
	    	<?php
	    	if(!empty(@$breadcrumbs)){
	    		foreach ($breadcrumbs as $name => $link) {
	    			if(strtolower(@$link) == "active"){
	      				echo '		<li class="active">'.ucfirst(@$name).'</li>';
	    			}else{
	      				echo '		<li><a href="'.base_url(@$link).'">'.ucfirst(@$name).'</a></li>';
	    			}
	    		}
	    	}
	    	?>
      	</ol>
    </div>