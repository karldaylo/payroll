<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php
		__css('assets/css/normalize.css');
		__css('assets/css/bootstrap.min.css');
		__css('assets/css/jquery-ui.css');
		__css('assets/css/nav.css');
		__css('assets/css/istayl.css');
		__css('assets/css/bootstrap-tagsinput.css');
		__css('assets/css/bootstrap-datetimepicker.min.css');
		__css('assets/css/select2.min.css');
		
		__js('assets/js/jquery.min.js');
		// __js('assets/css/iskrip.js');
		__js('assets/js/jquery-ui.js');
		__js('assets/js/bootstrap.min.js');
		__js('assets/js/moment.js');
		__js('assets/js/transition.js');
		__js('assets/js/collapse.js');
		__js('assets/js/bootstrap-tagsinput.js');
		__js('assets/js/bootstrap-datetimepicker.min.js');
		__js('assets/js/select2.min.js');
		// __js('assets/js/autocomplete.js');
	?>
	<title>Spic'n Span - Payroll System</title>
</head>
<body>
	<div id="page_header" >	  
		<div class="container">
			<a href="<?php echo base_url(); ?>"><img class="img-responsive img-center" href="#" style='height:100%;max-height: 85px;' src="<?php echo base_url('assets/img/sns.png');?>"></a>
			<div class="pull-right text-right">
				<div id="date"></div>
				<div id="time"></div>
			</div>
		</div>
	</div>
	<nav class="navbar navbar-static-top navbar-default" style="margin-bottom:0;">    
		<div class="container">
          	<div class="navbar-header">
            	 <!-- <a ><img class="navbar-brand" href="#" src="<?php echo base_url('assets/img/mcgpi1.png');?>" style="min-height:50px;padding:5px"> </a> -->
              	<a class="navbar-brand" href="<?php echo base_url();?>">Spic'n Span - <span class="glyphicon glyphicon-home"></span> Home</a>
          	</div> 
        	
        	
        	<ul class="nav navbar-nav">
		      <!-- <li id="nav-inventory">	<a href="<?php echo base_url('/inventory');?>"><span class="glyphicon glyphicon-th"></span> &nbsp;  Inventory</a></li> -->
		      <?php
		      	if($this->session->admin || $this->session->staff){
		      ?>
		      <li id="nav-employee">		<a href="<?php echo base_url('/employee');?>"> <span class="glyphicon glyphicon-user"></span> &nbsp; Employee Records</a></li>

		      <?php
		      	if($this->session->admin){
		      ?>
		      <li id="nav-officer">		<a href="<?php echo base_url('/officer');?>"> <span class="glyphicon glyphicon-list"></span> &nbsp; Officer Accounts</a></li>
		      <li id="nav-office">		<a href="<?php echo base_url('/office');?>"> <span class="glyphicon glyphicon-briefcase"></span> &nbsp; Branches and Offices</a></li>
		      <?php
		  		}//check if admin
		  		}//check if staff
		      ?>
		    </ul>
		    

          	<ul class="nav navbar-nav navbar-right">	        	
	        	<li class="dropdown">
	          		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
	          		<span class="glyphicon glyphicon-cog"></span> &nbsp;&nbsp;<?php echo @$this->session->fullname ?> <span class="caret"></span></a>
	          		<ul class="dropdown-menu">
		            	<li><a href="<?php echo base_url('employee/settings')?>" >Change Password</a></li>
		            	<li><a href="<?php echo base_url('logout')?>">Logout</a></li>
	          		</ul>
	        	</li>
	      	</ul>		      	
		</div>
    </nav>

