<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form method="POST" action="" enctype="multipart/form-data">
<div class="container  dashboard-container">
  <div class="text-center" style="width:100%;"><h2>Inventory Registration</h2></div>
  
  <fieldset>
    <legend><h3>Step 1 - Base Information</h3></legend>
    <?php
      echo bootstrap_form("inventory_name",           "Inventory Name",                 "",array("req"=>true,"class"=>"col-md-6"));
      echo bootstrap_form("inventory_code",           "Inventory Code (aplhanumeric only)","",array("req"=>true,"class"=>"col-md-6"));
      echo bootstrap_form_textarea("inventory_desc",  "Description",                    "",array("req"=>true,"class"=>"col-md-12"));
      echo bootstrap_form("inventory_serial",         "Serial Number/Marks and Numbers","",array("class"=>"col-md-6"));
      echo bootstrap_form("inventory_category",       "Category",                       "",array("class"=>"col-md-6","placeholder"=>"e.g Chair, Laptop, Table, PC Accessory, etc..."));
      echo bootstrap_form("inventory_location",       "Location Stored/Origin Location","",array("class"=>"col-md-6"));
      echo bootstrap_form("inventory_brand",          "Item Brand",                     "",array("class"=>"col-md-6"));
      echo bootstrap_form("unit_price",               "Per Unit Price (in PHP)",                 "",array("class"=>"col-md-6","placeholder"=>"Hit enter key or comma to separate..."));
      echo bootstrap_form("item_tags",                "Inventory Tags",                 "",array("class"=>"col-md-6","attrib"=>"data-role='tagsinput'","placeholder"=>"Hit enter key or comma to separate..."));

    ?>
  </fieldset>
</div>

<div class="container  dashboard-container">
  <fieldset>
    <legend><h3>Step 2 - Inventory Entry Count</h3></legend>
    <div class="col-md-12">
      <div class="alert alert-warning">This fields is for adding an entry count of the inventory.</div>
      <br>
    </div>
    <?php
      echo bootstrap_form("unit_qty",        "Unit Quantity",                  "",array("req"=>true,"type"=>"number","class"=>"col-md-6"));
      echo bootstrap_form("unit_origin",     "Purchased from/Acquired from",   "",array("class"=>"col-md-6"));
      echo bootstrap_form_textarea("unit_remarks",   "Aquisition Remarks (if any)", "",array("class"=>"col-md-12"));
    ?>
  </fieldset>
</div>

<div class="container  dashboard-container">
  <fieldset>
    <legend><h3>Step 3 - Image Preview</h3></legend>

      <div class="col-md-8">
        <div class="form-group">
          <label for="attach_insenrollform" class="control-label">Image Preview<b class="req">*</b></label> 
          <input required class="form-control padless attach-group" type="file" name="img_preview" id="img_preview" style="height:28px" accept="image/*" onchange="loadFile(event)">
        </div>
      </div>
      <div class="col-md-6">
        <img src="<?php echo base_url("/assets/img/img.png")?>" style="width:100%" id="output"/>
      </div>

  </fieldset>
</div>


<div class="container  button-container">
  <div class="col-md-6">
    <a class="btn btn-lg btn-default btn-block"> <span class="glyphicon glyphicon-share-alt"></span> Return to Inventory List</a>
  </div>
  <div class="col-md-6">
    <button  class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="submit"> <span class="glyphicon glyphicon-ok"></span> Save Inventory</button>
  </div>
  
</div>

</form>
<script type="text/javascript">
  $(document).keypress(
    function(event){
      if (event.which == '13') {
        event.preventDefault();
      }
  });
  $('#inventory_code').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
  });
  $('#item_tags').keypress(function (e) {
    var regex = new RegExp("^[a-zA-Z0-9_,]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
  });
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };
</script>