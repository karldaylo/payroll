<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form method="POST" action="" enctype="multipart/form-data">
<div class="container  button-container">
  <div class="col-sm-4 padless">
    <a  class="btn btn-lg btn-warning btn-block" data-toggle="modal" data-target="#invetory-add"><span class="glyphicon glyphicon-plus-sign"></span><br> Increase Inventory</a>
  </div>
  
  <div class="col-sm-4 padless">
    <a  class="btn btn-lg btn-danger btn-block" data-toggle="modal" data-target="#invetory-release"><span class="glyphicon glyphicon-info-sign"></span><br> Release Inventory</a>
  </div>
  
  <div class="col-sm-4 padless">
    <a  class="btn btn-lg btn-primary btn-block" data-toggle="modal" data-target="#invetory-appoint"><span class="glyphicon glyphicon-user"></span><br> Assign Inventory</a>
  </div>
</div>


<div id="invetory-add" class="modal fade" role="dialog">
  <div class="modal-dialog" id="inspection_modal" style="width:100%;max-width: 700px">
    <div class="modal-content">
      <div class="modal-header" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">Increase Inventory</h4>
      </div>
      <div class="modal-body" id="preview-inspection2">
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-info"> Increase inventory total stock.</div>
            <br>
          </div>

          <?php
            echo bootstrap_form("add_qty",        "Unit Quantity",                  "",array("req"=>true,"type"=>"number","class"=>"col-md-6"));
            echo bootstrap_form("add_origin",     "Purchased from/Acquired from",   "",array("class"=>"col-md-6"));
            echo bootstrap_form_textarea("add_remarks",   "Aquisition Remarks (if any)", "",array("class"=>"col-md-12"));
          ?>
        </div>

         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>

<div id="invetory-release" class="modal fade" role="dialog">
  <div class="modal-dialog" id="inspection_modal" style="width:100%;max-width: 700px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">Release Inventory</h4>
      </div>
      <div class="modal-body" id="preview-inspection2">
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-warning"> Permanently decrease stock count by releasing inventory/ies.</div>
            <br>
          </div>

          <?php
            echo bootstrap_form("release_qty",                "Release Quantity",                  "1",array("req"=>true,"type"=>"number","class"=>"col-md-12"));
            echo bootstrap_form("release_to",                 "Release to Person",                 "", array("req"=>true,"class"=>"col-md-6"));
            echo bootstrap_form("release_at",                 "Release Destination (optional)",    "", array("class"=>"col-md-6"));
            echo bootstrap_form_textarea("release_remarks",   "Release Remarks (if any)", "",array("class"=>"col-md-12"));
          ?>

        </div>

         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>

<div id="invetory-appoint" class="modal fade" role="dialog">
  <div class="modal-dialog" id="inspection_modal" style="width:100%;max-width: 700px">
    <div class="modal-content">
      <div class="modal-header modalHeader" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">Assign Inventory</h4>
      </div>
      <div class="modal-body" id="preview-inspection2">
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <div class="col-md-12"> 
            <div class="alert alert-success"> Appoint and inventory without decreasing total stocked count.</div>
            <br>
          </div>

          <?php
            echo bootstrap_form("assign_qty",                "Assign Quantity",                  "1",array("req"=>true,"type"=>"number","class"=>"col-md-12"));
            echo bootstrap_form("assign_to",                 "Assign to Person",                 "", array("req"=>true,"class"=>"col-md-12"));
            echo bootstrap_form("assign_at",                 "Assign Destination (optional)",    "", array("class"=>"col-md-6"));
            echo bootstrap_form("assign_at",                 "Return Date",                      "", array("class"=>"col-md-6"));
            echo bootstrap_form_textarea("assign_remarks",   "Assign Remarks (if any)", "",array("class"=>"col-md-12"));
          ?>

        </div>
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>


<div class="container  dashboard-container">
    
    <div class="col-md-6">
      <img src="<?php echo base_url("/assets/img/img.png")?>" style="width:100%" id="output"/>
    </div>

    <div class="col-md-6">
      <div class="col-sm-12 padless"><b style="font-size: 25px">{item.name}</b></div>
      <div class="col-sm-6 padless" >
        <div class="col-xs-4 padless"><b>Item Code:</b></div>
        <div class="col-xs-8 padless">{item.code}</div>
      </div>
      <div class="col-sm-6 padless" >
        <div class="col-xs-4 padless"><b>Stock:</b></div>
        <div class="col-xs-8 padless">{item.remaining} of {item.total}</div>
      </div>
      <div class="col-sm-12 padless">
        <div class="col-md-2 padless"><b>Description:</b></div>
        <div class="col-md-10 padless">{item.details}</div>
      </div>
      <div class="col-sm-6 padless">
        <div class="col-xs-4 padless"><b>Unit Price:</b></div>
        <div class="col-xs-8 padless">{item.price}</div>
      </div>
      <div class="col-sm-6 padless">
        <div class="col-xs-4 padless"><b>Category:</b></div>
        <div class="col-xs-8 padless">{item.category}</div>
      </div>
      <div class="col-sm-6 padless">
        <div class="col-xs-4 padless"><b>Serial:</b></div>
        <div class="col-xs-8 padless">{item.category}</div>
      </div>
      <div class="col-sm-6 padless">
        <div class="col-xs-4 padless"><b>Brand:</b></div>
        <div class="col-xs-8 padless">{item.category}</div>
      </div>
      <div class="col-sm-6 padless">
        <div class="col-xs-4 padless"><b>Stored at:</b></div>
        <div class="col-xs-8 padless">{item.category}</div>
      </div>

      <div class="col-sm-12 padless">
        <div class="col-md-2 padless"><b>Tags:</b></div>
        <div class="col-md-10 padless">{item.tags}</div>
      </div>
    </div>

</div>



<div class="container row  dashboard-container">
  Logs
</div>

<div class="container  dashboard-container">
  Current equipment
</div>

</form>