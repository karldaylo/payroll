<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form method="POST" action="">

  <div class="container breadcrumb-container">
    <div class="col-sm-6 padless ">
      <a class="dashboard-body dashboard-success btn btn-block marginless padless"  href="<?php echo base_url('attendance'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-calendar"></span></div>
        <div class="dashboard-content"> Attendance</div>
        <div class="dashboard-sub">Create Attendance Entry</div>
      </a>
    </div>

    <div class="col-sm-6 padless ">
      <a class="dashboard-body dashboard-pending btn btn-block marginless padless"  data-toggle="modal" data-target="#generate-file">
        <div class="dashboard-background"><span class="glyphicon glyphicon-list"></span></div>
        <div class="dashboard-content"> Generate Files </div>
        <div class="dashboard-sub">UnionBank SSS/Philhealth/Pagibig</div>
      </a>
    </div>

    <div class="col-sm-6 padless ">
      <a class="dashboard-body dashboard-primary btn btn-block marginless padless"  href="<?php echo base_url('employee'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-user"></span> </div>
        <div class="dashboard-content"> Employee Records</div>
        <div class="dashboard-sub">Manage Employee Records</div>
      </a>
    </div>

    <div class="col-sm-6 padless ">
      <a class="dashboard-body dashboard-danger btn btn-block marginless padless"  href="<?php echo base_url('reports'); ?>">
        <div class="dashboard-background"><span class="glyphicon glyphicon-list"></span></div>
        <div class="dashboard-content"> Reports</div>
        <div class="dashboard-sub">Salary/SSS/Philhealth/Pagibig</div>
      </a>
    </div>

    

  </div>

<!--## MODAL BEGIN ##-->
<div id="generate-file" class="modal fade" role="dialog">
  <div class="modal-dialog" id="inspection_modal" style="width:100%;max-width: 700px">
    <div class="modal-content">
      <div class="modal-header" >
          <div class="pull-right" data-dismiss="modal" style="cursor:pointer">
              <span class="glyphicon glyphicon-remove"></span>
          </div>
          <h4 class="modal-title">Generate File</h4>
      </div>
      <div class="modal-body" id="preview-inspection2">
        <!--##  MODAL BODY BEGIN ##-->
        <div class="row">
          <div class="col-md-12">
            <div class="alert alert-info"><b>Note:</b> This is not the final version, some may subject to change.</div>
            <br>
          </div>

          <div class="form-group col-md-12">
            <label for="target_type" class="control-label">Generate what file?.</label> 
            <select required class="form-control "  name="target_type" id="target_type">
              <option value="payroll">Payroll(UnionBank)</option>
              <option value="pagibig">Pag-Ibig</option>
              <option value="philhealth">Philhealth</option>
              <option value="sss">SSS</option>
            </select>
          </div>

          <div class="form-group col-md-12" id="date-display">
            <label for="target_date" class="control-label">Date</label> 
            <select required class="form-control "  name="target_date" id="target_date">
              <option value="1">Day 1 to 15</option>
              <option value="2">Day 16 to end of month</option>
            </select>
          </div>

          <div class="form-group col-md-6">
            <label for="target_month" class="control-label">Month</label> 
            <select required class="form-control "  name="target_month" id="target_month">
              <option value="01">January</option>
              <option value="02">February</option>
              <option value="03">March</option>
              <option value="04">April</option>
              <option value="05">May</option>
              <option value="06">June</option>
              <option value="07">July</option>
              <option value="08">August</option>
              <option value="09">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
          </div>

          <div class="form-group col-md-6">
            <label for="target_year" class="control-label">Year</label> 
            <select required class="form-control "  name="target_year" id="target_year">
              <option>2020</option>
              <option>2021</option>
              <option>2022</option>
              <option>2023</option>
              <option>2024</option>
              <option>2025</option>
              <option>2026</option>
              <option>2027</option>
              <option>2028</option>
              <option>2029</option>
            </select>
          </div>

          <div class="form-group col-md-12">
            <a class="btn btn-block btn-lg btn-primary" id="generate-button">Generate File</a>
          </div>
        </div>

         
        <!--##  MODAL BODY END ##-->
      </div> 
    </div>
  </div>
</div>
<!--## Modal End ##-->

<script type="text/javascript">
  <?php
  $month_today = date("m");
  $year_today  = date("Y");
  $day_today  = date("d");
  $target_date=1;
  if($day_today>15){
    $target_date=2;
  }
  echo js_input("target_month",$month_today);
  echo js_input("target_year",$year_today);
  ?>
  $("#target_type").change(function(){
    $("#date-display").fadeOut();
    if($("#target_type").val() == "payroll"){
      $("#date-display").fadeIn();
    }
  });

  $("#generate-button").click(function(){
    if($("#target_type").val() == "payroll"){
      window.location.href = "<?php echo base_url('generate');?>/"+$("#target_type").val()+"/"+$("#target_year").val()+"/"+$("#target_month").val()+"/"+$("#target_date").val();
    }
    else{
      window.location.href = "<?php echo base_url('generate');?>/"+$("#target_type").val()+"/"+$("#target_year").val()+"/"+$("#target_month").val();
    }
  });
</script>

<?php
if(!empty($this->session->admin)){//if user is admin
?>
<div class="container handle-container" id="logs-table">
  <div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
      <thead>
        <tr>
          <th class="text-center tbhead" colspan="2">Recent Logs <a class="btn btn-xs btn-primary pull-right"  href="#" onClick="MyWindow3=window.open('<?php echo base_url('report/log/');?>','MyWindow3','width=800,height=500'); return false;">View all logs</a></th>
        </tr>
        <tr>
          <th class="datetable">Date</th>         
          <th >Description</th>
        </tr>
      </thead>
      <tbody id="log-records">
        <?php
          if(empty($latest_logs)){
            echo "<tr><td colspan='2' class='unavailable text-center'>No Records</td></tr>";
          }else{
            foreach ($latest_logs as $val) {
              echo "<tr>
              <td>".__date($val['date_created'])."</td>
              <td>".$val['description']."</td>
              </tr>";
            }
          }
        ?>
      </tbody>
    </table>
  </div>
</div>
<?php
}//end of logs
?>

</form>
<script type="text/javascript">
  // $("#nav-account").addClass("active");
</script>