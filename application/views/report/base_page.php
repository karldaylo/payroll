<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// die(var_dump($transaction));
?>
<div class="container dashboard-container">
	<div class="row">
          <div class="form-group col-md-12 text-center" >
            <h3>Onepage Report Generation</h3>
          </div>

          <div class="col-md-8">
            <div class="form-group col-md-4" id="date-display">
              <label for="target_date" class="control-label">Date</label> 
              <select required class="form-control "  name="target_date" id="target_date">
                <option value="1">Day 1 to 15</option>
                <option value="2">Day 16 to end of month</option>
              </select>
            </div>

            <div class="form-group col-md-4">
              <label for="target_month" class="control-label">Month</label> 
              <select required class="form-control "  name="target_month" id="target_month">
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
              </select>
            </div>

            <div class="form-group col-md-4">
              <label for="target_year" class="control-label">Year</label> 
              <select required class="form-control "  name="target_year" id="target_year">
                <option>2020</option>
                <option>2021</option>
                <option>2022</option>
                <option>2023</option>
                <option>2024</option>
                <option>2025</option>
                <option>2026</option>
                <option>2027</option>
                <option>2028</option>
                <option>2029</option>
              </select>
            </div>
            
          </div>



        <div class="col-md-4 ">
          <button class="btn btn-lg btn-block btn-success" type="submit" name="download" value="salary"><span class="glyphicon glyphicon-credit-card"></span><br>Download Report</button>
        </div>
  </div>
</div>


<div class="container dashboard-container">
  <div class="row">
          <div class="form-group col-md-12 text-center" >
            <h3>Company Report Generation</h3>
          </div>

          <div class="col-md-8">
            <div class="form-group col-md-4" id="date-display">
              <label for="target_date2" class="control-label">Date</label> 
              <select required class="form-control "  name="target_date2" id="target_date2">
                <option value="1">Day 1 to 15</option>
                <option value="2">Day 16 to end of month</option>
              </select>
            </div>

            <div class="form-group col-md-4">
              <label for="target_month2" class="control-label">Month</label> 
              <select required class="form-control "  name="target_month2" id="target_month2">
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
              </select>
            </div>

            <div class="form-group col-md-4">
              <label for="target_year2" class="control-label">Year</label> 
              <select required class="form-control "  name="target_year2" id="target_year2">
                <option>2020</option>
                <option>2021</option>
                <option>2022</option>
                <option>2023</option>
                <option>2024</option>
                <option>2025</option>
                <option>2026</option>
                <option>2027</option>
                <option>2028</option>
                <option>2029</option>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label for="target_company2" class="control-label">Company</label> 
              <select required class="form-control "  name="target_company2" id="target_company2">
                <?php
                  if(!empty($company)){
                      foreach ($company as $val) {
                        echo "<option >".$val['label']."</options>";
                      }
                  }
                ?>
              </select>
            </div>
            
          </div>



        <div class="col-md-4 ">
          <button class="btn btn-lg btn-block btn-primary" type="submit" name="download" value="salary"><span class="glyphicon glyphicon-duplicate"></span><br>Download Report</button>
        </div>
  </div>
</div>

<div class="container dashboard-container">
  <div class="row">
          <div class="form-group col-md-12 text-center" >
            <h3>Specific Branch Report Generation</h3>
          </div>

          <div class="col-md-8">
            <div class="form-group col-md-4" id="date-display">
              <label for="target_date3" class="control-label">Date</label> 
              <select required class="form-control "  name="target_date3" id="target_date3">
                <option value="1">Day 1 to 15</option>
                <option value="2">Day 16 to end of month</option>
              </select>
            </div>

            <div class="form-group col-md-4">
              <label for="target_month3" class="control-label">Month</label> 
              <select required class="form-control "  name="target_month3" id="target_month3">
                <option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
              </select>
            </div>

            <div class="form-group col-md-4">
              <label for="target_year3" class="control-label">Year</label> 
              <select required class="form-control "  name="target_year3" id="target_year3">
                <option>2020</option>
                <option>2021</option>
                <option>2022</option>
                <option>2023</option>
                <option>2024</option>
                <option>2025</option>
                <option>2026</option>
                <option>2027</option>
                <option>2028</option>
                <option>2029</option>
              </select>
            </div>

            <div class="form-group col-md-12">
              <label for="target_company2" class="control-label">Company Branch</label> 
              <select required class="form-control "  name="target_company2" id="target_company2">
                <?php
                  if(!empty($company_branch)){
                      foreach ($company_branch as $val) {
                        echo "<option >".$val['label']."</options>";
                      }
                  }
                ?>
              </select>
            </div>
            
          </div>





        <div class="col-md-4 ">
          <button class="btn btn-lg btn-block btn-warning" type="submit" name="download" value="salary"><span class="glyphicon glyphicon-file"></span><br>Download Report</button>
        </div>
  </div>
</div>