<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// die(var_dump($transaction));
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php
		__css('assets/css/normalize.css');
		__css('assets/css/bootstrap.min.css');
		__css('assets/css/jquery-ui.css');

		__js('assets/js/jquery.min.js');
		__js('assets/js/jquery-ui.js');

	?>
	<style type="text/css">
		.infoLabels{
			font-weight: bold;
		}
		.padless,.nopadding{
			padding: 0px !important;
		}
		.marginless,.nomargin{
			margin:0px !important;
		}
	</style>
</head>
<form action="" method="GET">
<div class="col-md-12 text-center">
	<h3><?php
	$reportname["PATIENTPROGRESS"]="Patient Progress Report";
    $reportname["INCIDENT"]="Incident Report";
    $reportname["SOCIALCASE"]="Social Case Study Report";
    $reportname["PSYCEVAL"]="Psychiatric Evaluation Report";
    $reportname["PSYCTEST"]="Psychological Test Report";
    $reportname["INTAKESHEET"]="Intake Sheet Report";
    $reportname["NURSENOTES"]="Nurse's Notes";

    echo $reportname[$filter];
	?></h3>
</div>
<div class="col-md-12 row">
	<?php
	$name = "";
	if(!empty($patient['nick_name'])){
		$name = "(".$patient['nick_name'].") ";
	}
	$name .= $patient['first_name']." ".$patient['middle_name']." ".$patient['last_name']." ".$patient['suffix_name'];
    echo bootstrapalize("Patient Name:"     	,$name);
	?>
	<br><br>
</div>

<div class="col-md-12 row">
	<?php 
		if($page>0){
			echo "<a class='btn btn-primary pull-left'>&lt;&lt;Previous</a>";
		}
		if($limit == count($logs)){
			echo "<a class='btn btn-primary pull-right'>&gt;&gt;Next</a>";
		}
	?>
</div>
<div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
    	<thead>
    		<tr>
    			<th>Report Date</th>
    			<th>Downloads</th>
    			<th>Description</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    		if(!empty($logs)){
    			foreach ($logs as $val) {
    				$download = "";
    				if(!empty($val['report_file'])){
    					$download = '<a href="'.base_url('report/'.$val['report_file']).'" id="report-download" target="_blank" class="btn btn-primary btn-xs"> <span class="glyphicon glyphicon-download"></span> File</a>';
    				}
    				echo "<tr>
    					<td>".__date($val['report_date'],"m/d/Y")."</td>
    					<td>".$download."</td>
    					<td>".$val['report_description']."</td>
    				</tr>";
    			}
    		}
    		?>
    	</tbody>
    </table>
</div>
<div class="col-md-12 row">
	<?php 
		if($page>0){
			echo "<a class='btn btn-primary pull-left'>&lt;&lt;Previous</a>";
		}
		if($limit == count($logs)){
			echo "<a class='btn btn-primary pull-right'>&gt;&gt;Next</a>";
		}
	?>
</div>
</form>