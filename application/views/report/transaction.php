<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// die(var_dump($transaction));
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php
		__css('assets/css/normalize.css');
		__css('assets/css/bootstrap.min.css');
		__css('assets/css/jquery-ui.css');

		__js('assets/js/jquery.min.js');
		__js('assets/js/jquery-ui.js');

	?>
	<style type="text/css">
		.infoLabels{
			font-weight: bold;
		}
		.padless,.nopadding{
			padding: 0px !important;
		}
		.marginless,.nomargin{
			margin:0px !important;
		}
	</style>
</head>
<form action="" method="GET">
<div class="col-md-12 text-center">
	<h3>Transaction List</h3>
</div>
<div class="col-md-12 row">
	<?php
	$name = "";
	if(!empty($patient['nick_name'])){
		$name = "(".$patient['nick_name'].") ";
	}
	$name .= $patient['first_name']." ".$patient['middle_name']." ".$patient['last_name']." ".$patient['suffix_name'];
    echo bootstrapalize_4_sm("Patient Name:"     	,$name);
    echo bootstrapalize_4_sm("Balance: "     ,"Php ".number_format($balance,2));
	
	?>
	<br><br>
</div>

<div class="col-md-12 row">
	<?php 
		if($page>0){
			echo "<a class='btn btn-primary pull-left'>&lt;&lt;Previous</a>";
		}
		if($limit == count($transactions)){
			echo "<a class='btn btn-primary pull-right'>&gt;&gt;Next</a>";
		}
	?>
</div>
<div class="col-md-12 padless table-responsive">
    <table class="table table-condensed marginless ">
    	<thead>
    		<tr>
    			<th>Transaction Date</th>
    			<th>Transaction #</th>
    			<th>Amount</th>
    			<th>Type</th>
    			<th>Description</th>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    		if(!empty($transactions)){
    			foreach ($transactions as $val) {
    				echo "<tr>
    					<td>".__date($val['date_created'],"m/d/Y h:i:s")."</td>
    					<td>".__tranno($val['credit_id'])."</td>
    					<td>".number_format($val['amount'],2)."</td>
    					<td>".$val['type']."</td>
    					<td>".$val['description']."</td>
    				</tr>";
    			}
    		}
    		?>
    	</tbody>
    </table>
</div>
<div class="col-md-12 row">
	<?php 
		if($page>0){
			echo "<a class='btn btn-primary pull-left'>&lt;&lt;Previous</a>";
		}
		if($limit == count($transactions)){
			echo "<a class='btn btn-primary pull-right'>&gt;&gt;Next</a>";
		}
	?>
</div>
</form>