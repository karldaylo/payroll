<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

	public function __construct(){
            parent::__construct();
        	checksession($this->session);
    } 

	public function index(){
		$limit = 50;

		$this->load->model('inventory_db');

		$data['inventory_list'] = $this->inventory_db->get_inventory_list($limit);
		$data['limit'] = $limit;

		$this->load->view("header");
		$this->load->view("inventory/list",$data);
		$this->load->view("footer");
	}

	public function manage($id = 0){
		if($this->input->post("increase")){
			$this->submit_increase($id);
		}
		if($this->input->post("release")){
			$this->submit_release($id);
		}
		if($this->input->post("disable_inventory")){
			$this->disable_inventory($id);
		}
		if($this->input->post("enable_inventory")){
			$this->enable_inventory($id);
		}

		$this->load->model('inventory_db');
		$this->load->model('account_db');

		$data['item'] 	= $this->inventory_db->get_inventory_details($id);
		$data['id'] 	= $id;

		$this->load->view("header");
		$this->load->view("inventory/view",$data);
		$this->load->view("footer");
	}

	public function search(){
		//models
		$this->load->model('inventory_db');

		$viewdata["inventory_list"] = [];
		if(!empty($this->input->get("q"))){
			$viewdata["inventory_list"] = $this->inventory_db->omni_item_search(__dbtrim($this->input->get("q")));
		}
		$viewdata["limit"] = 0;

		$this->load->view("header");
		$this->load->view("inventory/list",$viewdata);
		$this->load->view("footer");
		
	}


	public function edit($id = 0){
		if(empty($this->session->staff)){
			redirect("/");
		}
		if($this->input->post("submit")){
			$this->submit_update($id);
		}

		$this->load->model('inventory_db');

		$data['item'] 	= $this->inventory_db->get_inventory_details($id);
		$data['id'] 	= $id;
		if(empty($data['item'])){
			die("no");
		}

		$this->load->view("header");
		$this->load->view("inventory/addform");
		$this->load->view("inventory/edit",$data);
		$this->load->view("footer");
	}


	public function addnew(){
		if($this->input->post("submit")){
			$this->addtoinventory();
		}

		$this->load->view("header");
		$this->load->view("inventory/addform");
		$this->load->view("footer");
	}

	private function submit_increase($id){
		//model
		$this->load->model('inventory_db');
		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}

		$status = $this->inventory_db->increase_inventory($data,$id);
		if (@$status['error']==0) {
			redirect("inventory/manage/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}
	}

	private function submit_release($id){
		//model
		$this->load->model('inventory_db');
		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}

		$status = $this->inventory_db->release_inventory($data,$id);
		if (@$status['error']==0) {
			redirect("inventory/manage/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}
	}

	private function addtoinventory(){
		//model
		$this->load->model('inventory_db');
		//post and file text cleanup
		$data = $this->input->post(); 
		// die(var_dump($data));
		
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}
		$data['upload'] 	=  $_FILES;

		$status = $this->inventory_db->save_item($data);

		if (@$status['error']==0) {
			redirect("inventory/manage/".$status['item_id']);
		}
		else{
			echo "error";
			die(var_dump($status));
		}
	}

	private function submit_update($id){
		//model
		$this->load->model('inventory_db');
		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}
		$data['upload'] 	=  $_FILES;
		$data['old_id'] 	=  $id;

		$status = $this->inventory_db->save_item($data,true);

		if (@$status['error']==0) {
			redirect("inventory/manage/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}
	}

	private function disable_inventory($id){
		//model
		$this->load->model('inventory_db');
		$data['old_id'] 	=  $id;

		$status = $this->inventory_db->disable_inventory($id);
		
		if (@$status['error']==0) {
			redirect("inventory/manage/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}
	}

	private function enable_inventory($id){
		//model
		$this->load->model('inventory_db');
		$data['old_id'] 	=  $id;

		$status = $this->inventory_db->enable_inventory($id);

		if (@$status['error']==0) {
			redirect("inventory/manage/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}
	}

	
}
