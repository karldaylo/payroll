<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Generate extends CI_Controller {

	public function employee(){
		//models
		$this->load->model('report_db');

		$this->report_db->download_employee();
	}

	public function payroll($y,$m,$d){
		// $y = 2020;
		// $m = 07;
		$date = 1;

		//models
		$this->load->model('generate_db');

		$bufferdata = "H|SPICNSPAN-ECRE|".date("mdY")."|1"."\n";


		$bufferdata .= $this->generate_db->getPayroll($y,$m,$d);

		$file = "SPICNSPANECRE_CRF_".date("mdY").".txt";
		$txt = fopen($file, "w") or die("Unable to open file!");
		fwrite($txt, $bufferdata);
		fclose($txt);

		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		header("Content-Type: text/plain");
		readfile($file);
	}

	public function pagibig($y,$m){
		// $y = 2020;
		// $m = 07;
		$date = 1;

		//models
		$this->load->model('generate_db');

		$bufferdata = "EH04202005201035820003   PMCSPIC N SPAN SERVICE CORPORATION                                                                     6TH FLOOR BERMA CENTER REDEMTORIST ROAD BACLARAN                                                    1702   8975474        "."\r\n";


		$bufferdata .= $this->generate_db->getPagIbig($y,$m,$date);

		$file = "PAGIBIGPREM_".$m."_".$y.".txt_201035820003.txt";
		$txt = fopen($file, "w") or die("Unable to open file!");
		// $txt = fopen(__dir__."/../../temp/".$file, "w") or die("Unable to open file!");
		// $txt = fopen($file, "w") or die("Unable to open file!");
		fwrite($txt, $bufferdata);
		fclose($txt);

		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		header("Content-Type: text/plain");
		readfile($file);
	}

	public function philhealth($y,$m){
		// $y = 2020;
		// $m = 07;
		$date = 1;

		//models
		$this->load->model('generate_db');

		$bufferdata = "EH04202005201035820003   PMCSPIC N SPAN SERVICE CORPORATION                                                                     6TH FLOOR BERMA CENTER REDEMTORIST ROAD BACLARAN                                                    1702   8975474        "."\r\n";


		$bufferdata .= $this->generate_db->getPhilHealth($y,$m,$date);

		$file = "PHILHEALTH_".$m."_".$y.".txt_201035820003.txt";
		$txt = fopen($file, "w") or die("Unable to open file!");
		// $txt = fopen(__dir__."/../../temp/".$file, "w") or die("Unable to open file!");
		fwrite($txt, $bufferdata);
		fclose($txt);

		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		header("Content-Type: text/plain");
		readfile($file);
	}

	public function sss($y,$m){
		// $y = 2020;
		// $m = 07;
		$date = 1;

		//models
		$this->load->model('generate_db');

		$bufferdata = "EH04202005201035820003   PMCSPIC N SPAN SERVICE CORPORATION                                                                     6TH FLOOR BERMA CENTER REDEMTORIST ROAD BACLARAN                                                    1702   8975474        "."\r\n";


		$bufferdata .= $this->generate_db->getSSS($y,$m,$date);

		$file = "SSS_".$m."_".$y.".txt_201035820003.txt";
		// $txt = fopen(__dir__."/../../temp/".$file, "w") or die("Unable to open file!");
		$txt = fopen($file, "w") or die("Unable to open file!");
		fwrite($txt, $bufferdata);
		fclose($txt);

		header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename='.basename($file));
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . filesize($file));
		header("Content-Type: text/plain");
		readfile($file);
	}

	private function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
	    header('Content-Type: application/csv');
	    header('Content-Disposition: attachment; filename="'.$filename.'";');

	    // open the "output" stream
	    // see http://www.php.net/manual/en/wrappers.php.php#refsect2-wrappers.php-unknown-unknown-unknown-descriptioq
	    $f = fopen('php://output', 'w');

	    foreach ($array as $line) {
	        fputcsv($f, $line, $delimiter);
    	}
    }


}  

