<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	public function __construct(){
            parent::__construct();
        	checksession($this->session);
    } 

	public function index(){
		if($this->input->get('method')=='item'){
			$this->item();
		}elseif($this->input->get('method')=='user'){
			$this->user();
		}elseif($this->input->get('method')=='patient'){
			$this->patient();
		}else{
			$this->item();
		}
	}

	public function user(){
		//models
		$this->load->model('report_db');

		$viewdata["result_list"] = [];
		if(!empty($this->input->get("q"))){
			$viewdata["result_list"] = $this->report_db->omni_user_search(__dbtrim($this->input->get("q")));
		}
		if(!empty($this->input->get("advance"))){
			$data = $this->input->get(); 
			foreach ($data as $key => $value){
			    $data[$key] 	= __dbtrim($value);
			}
			$viewdata["result_list"] = $this->report_db->advance_user_search($data);
		}
		$this->load->view("header");
		$this->load->view("search/user",$viewdata);
		$this->load->view("footer");
		
	}

	public function item(){
		//models
		$this->load->model('report_db');

		$viewdata["result_list"] = [];
		if(!empty($this->input->get("q"))){
			$viewdata["result_list"] = $this->report_db->omni_item_search(__dbtrim($this->input->get("q")));
		}
		if(!empty($this->input->get("advance"))){
			$data = $this->input->get(); 
			foreach ($data as $key => $value){
			    $data[$key] 	= __dbtrim($value);
			}
			
			$viewdata["result_list"] = $this->report_db->advance_item_search($data);
		}

		$this->load->view("header");
		$this->load->view("search/item",$viewdata);
		$this->load->view("footer");
		
	}

	public function assign(){
		//models
		$this->load->model('report_db');
		$this->load->model('account_db');

		$viewdata["result_list"] = [];
		if(!empty($this->input->get("q"))){
			$viewdata["result_list"] = $this->report_db->omni_assign_search(__dbtrim($this->input->get("q")));
		}
		if(!empty($this->input->get("advance"))){
			$data = $this->input->get(); 
			foreach ($data as $key => $value){
			    $data[$key] 	= __dbtrim($value);
			}
			$viewdata["result_list"] = $this->report_db->advance_assign_search($data);
		}

		$viewdata['users'] 	= $this->account_db->get_user_list(1000);

		$this->load->view("header");
		$this->load->view("search/assign",$viewdata);
		$this->load->view("footer");
		
	}
	

	
}
