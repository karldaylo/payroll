<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

	public function __construct(){
            parent::__construct();
        	// checksession($this->session);
    } 

	public function index(){


		if($this->input->post("deleteme")){
			$this->disable_account();
		}

		$limit = 50;
		//models
		$this->load->model('account_db');

		$data["user_list"] 	= $this->account_db->get_employee_list("",$limit);
		$data["limit"] 		= $limit;
		$this->load->view("header");
		$this->load->view("employee/list",$data);
		$this->load->view("footer");
		
	}

	public function search(){
		$limit = 50;
		//models
    	$this->load->model('account_db');
		$query = "";
    	if(!empty($_GET['q'])){
    		$query = $_GET['q'];
    	}
		
		$data["user_list"] 	= $this->account_db->get_employee_list($query,$limit);
		$data["limit"] 		= $limit;
		$this->load->view("header");
		$this->load->view("employee/list",$data);
		$this->load->view("footer");	
	}

	public function view($id = 0){

		if($this->input->post("fileupload")){
			$this->upload_attachment($id);
		}
		if($this->input->post("submitdependent")){
			$this->add_dependent($id);
		}
		if($this->input->post("submitraining")){
			$this->add_training($id);
		}
		if($this->input->post("submitworkexperience")){
			$this->add_workexperience($id);
		}

		if($this->input->post("delete_dependent")){
			$this->delete_dependent($id);
		}
		if($this->input->post("delete_training")){
			$this->delete_training($id);
		}
		if($this->input->post("delete_workexperience")){
			$this->delete_workexperience($id);
		}

		//models
		$this->load->model('account_db');

		$data["user"]  			= $this->account_db->get_employee_details($id);
		$data["id"]   = $id;

		if(!empty($data['user']['admin']) || !empty($data['user']['staff'])){
			redirect("officer/view/".$id);
		}
		
		$this->load->view("header");
		$this->load->view("employee/view",$data);
		$this->load->view("footer");
	}

	public function payroll($id = 0){
		//models
		$this->load->model('account_db');

		$data["payroll"]	= $this->account_db->get_payroll_details($id);
		$data["id"]   		= $id;
		
		$this->load->view("header");
		$this->load->view("employee/view",$data);
		$this->load->view("footer");
	}

	private function add_workexperience($id){
		//models
		$this->load->model('account_db');

		//post and file text cleanup
		$data = $this->input->post(); 
	    foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}
		$data['old_uid'] 		=  $id;

		$status = $this->account_db->add_workexperience($data);

		if (@$status['error']==0) {
			redirect("employee/view/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}

		//do nothing
	}

	private function add_training($id){
		//models
		$this->load->model('account_db');

		//post and file text cleanup
		$data = $this->input->post(); 
	    foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}
		$data['old_uid'] 		=  $id;

		$status = $this->account_db->add_training($data);

		if (@$status['error']==0) {
			redirect("employee/view/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}

		//do nothing
	}

	private function add_dependent($id){
		//models
		$this->load->model('account_db');

		//post and file text cleanup
		$data = $this->input->post(); 
	    foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}
		$data['old_uid'] 		=  $id;

		$status = $this->account_db->add_dependent($data);

		if (@$status['error']==0) {
			redirect("employee/view/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}

		//do nothing
	}

	public function delete_training($id){
		//models
		$this->load->model('account_db');
		$data = __dbtrim($this->input->post('delete_training')); 
		$status = $this->account_db->delete_training($data);
		redirect("employee/view/".$id);
	}

	public function delete_dependent($id){
		//models
		$this->load->model('account_db');
		$data = __dbtrim($this->input->post('delete_dependent')); 
		$status = $this->account_db->delete_dependent($data);
		redirect("employee/view/".$id);
	}

	public function delete_workexperience($id){
		//models
		$this->load->model('account_db');
		$data = __dbtrim($this->input->post('delete_workexperience')); 
		$status = $this->account_db->delete_workexperience($data);
		redirect("employee/view/".$id);
	}

	public function upload_attachment($id){
		//models
		$this->load->model('account_db');
		$data['file_description'] = __dbtrim($this->input->post('file_description')); 
		$data['upload'] = $_FILES; 
		$status = $this->account_db->upload_file($id,$data);
		redirect("employee/view/".$id);
	}

	public function disable_account(){
		//models
		$this->load->model('account_db');
		$data = __dbtrim($this->input->post('deleteme')); 
		$status = $this->account_db->disable_account($data);
		redirect("employee/");
	}

	public function register(){

		$this->load->model('office_db');
		$data["company_list"] 		= $this->office_db->get_company_list();
		$data["location_list"] 		= $this->office_db->get_location_list();
		$data["branch_list"] 		= $this->office_db->get_branch_list();
		if($this->input->post("submit")){
			$this->newemployee();
		}
		$this->load->view("header");
		$this->load->view("employee/register",$data);
		$this->load->view("footer");
	}

	public function edit($id){

		$this->load->model('account_db');
		$this->load->model('office_db');
		$data["company_list"] 		= $this->office_db->get_company_list();
		$data["location_list"] 		= $this->office_db->get_location_list();
		$data["branch_list"] 		= $this->office_db->get_branch_list();

		if($this->input->post("submit")){
			$this->newemployee(true,$id);
		}

		$data["user"]  			= $this->account_db->get_employee_details($id);
		$data["id"]   = $id;

		$this->load->view("header");
		$this->load->view("employee/register",$data);
		$this->load->view("employee/edit",$data);
		$this->load->view("footer");
	}

	public function newemployee($edit = false,$id = 0){

		//models
		$this->load->model('account_db');

		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
			if(is_array($value)){
		    	foreach ($value as $key2 => $value2) {
		    		$data[$key][$key2] = __dbtrim($value2);
		    	}
		    }else{
		    	$data[$key] = __dbtrim($value);
		    }
		}
		$data['upload'] 	=  $_FILES;
		$data["id"]   = $id;
		// die(var_dump($data));

		$status = $this->account_db->save_employeeinfo($data,$edit);

		if (@$status['error']==0) {
			if($edit){
				redirect("employee/view/".$id);
			}
			redirect("employee/");
		}
		else{
			echo "error";
			die(var_dump($status));
		}
	}

	public function settings($id = 0){
		//if you are staff and trying to change someone's pass, dont
		if(empty($this->session->staff) && $id > 0){
			redirect("user/settings");
		}

		if($this->input->post("change_pass")){
			$this->submit_changepass($id);
		}

		if($id == 0){
			$id = $this->session->uid;
		}

		//models
		$this->load->model('account_db');

		$data["user"] = $this->account_db->get_employee_details($id);
		// die(var_dump($data['user']));
		$data["id"]   = $id;
		if(empty($data['user']['uid'])){
			die("404");
		}

		$this->load->view("header");
		// $this->load->view("officer_manage/view",$data);
		$this->load->view("employee/password",$data);
		$this->load->view("footer");
	}

	public function printid($id = 0){
		if($id == 0){
			$id = $this->session->uid;
		}
		//models
		$this->load->model('account_db');

		$data["user"] = $this->account_db->get_employee_details($id);
		// die(var_dump($data['user']));
		$data["id"]   = $id;
		if(empty($data['user']['uid'])){
			die("404");
		}

		$this->load->view("employee/id",$data);
	}

	private function submit_changepass($id){
		//models
		$this->load->model('account_db');

		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}

		$status = $this->account_db->change_password(@$data['oldpass'],$data['newpass'],$data['confirm_newpass'],$id);

		$display_result= array(
			0 => "ERROR UNKNOWN", 
			1 => "Incorrect Current Password", 
			2 => "Confirm Password Does not Match", 
			9 => "Success", 
		);

		$this->session->set_flashdata('display_result',$display_result[$status]);
		redirect("employee/settings/".$id);
	}
	

	


	
}
