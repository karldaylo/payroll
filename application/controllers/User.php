<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
            parent::__construct();
        	checksession($this->session);
    } 

	public function index(){
		$limit = 50;
		//models
		$this->load->model('account_db');

		$data["user_list"] 	= $this->account_db->get_user_list($limit);
		$data["limit"] 		= $limit;
		$this->load->view("header");
		$this->load->view("user/list",$data);
		$this->load->view("footer");
		
	}

	public function view($id = 0){
		
		
		//models
		$this->load->model('account_db');

		$data["user"] = $this->account_db->get_user_details($id);
		$data["id"]   = $id;
		if(empty($data['user']['username'])){
			die("404");
		}

		
		$this->load->view("header");
		$this->load->view("employee/view",$data);
		$this->load->view("footer");
	}

	public function settings($id = 0){
		if(empty($this->session->staff) && $id > 0){
			redirect("user/settings");
		}
		if($this->input->post("change_pass")){
			$this->submit_changepass($id);
		}

		if($id == 0){
			$id = $this->session->uid;
		}
		// die(var_dump($id));

		//models
		$this->load->model('account_db');

		$data["user"] = $this->account_db->get_user_details($id);
		$data["id"]   = $id;
		// die(var_dump($data));

		if(empty($data['user']['username'])){
			die("404");
		}

		$this->load->view("header");
		$this->load->view("user/view",$data);
		$this->load->view("user/password");
		$this->load->view("footer");
	}

	public function register(){

		$this->load->model('account_db');
		$data['branch'] = $this->account_db->get_branches();

		if($this->input->post("submit")){
			$this->newuser();
		}

		$this->load->view("header");
		$this->load->view("user/register",$data);
		$this->load->view("footer");
	}

	public function search(){
		//models
		$this->load->model('account_db');

		$viewdata["user_list"] = [];
		if(!empty($this->input->get("q"))){
			$viewdata["user_list"] = $this->account_db->omni_user_search(__dbtrim($this->input->get("q")));
		}
		$viewdata["limit"] = 0;
		
		$this->load->view("header");
		$this->load->view("user/list",$viewdata);
		$this->load->view("footer");
		
	}

	public function edit($id){


		//models
		$this->load->model('account_db');

		$data["user"] = $this->account_db->get_user_details($id);
		$data["id"]   = $id;
		if(empty($data['user']['username'])){
			die("404");
		}

		
		$this->load->view("header");
		$this->load->view("employee/register",$viewdata);
		$this->load->view("employee/edit",$data);
		$this->load->view("footer");
	}

	public function newuser(){

		//models
		$this->load->model('account_db');

		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}
		$data['upload'] 	=  $_FILES;
		// die(var_dump($data));

		$status = $this->account_db->save_userinfo($data);

		if (@$status['error']==0) {
			redirect("user/");
		}
		else{
			echo "error";
			die(var_dump($status));
		}

	}

	public function edituser($id){
		
		//models
		$this->load->model('account_db');

		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}
		$data['upload'] 	=  $_FILES;
		$data['old_uid'] 	=  $id;

		$status = $this->account_db->save_userinfo($data,true);

		if (@$status['error']==0) {
			redirect("user/view/".$id);
		}
		else{
			echo "error";
			die(var_dump($status));
		}

	}

	public function submit_changepass($id){
		//models
		$this->load->model('account_db');

		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}

		$status = $this->account_db->change_password(@$data['oldpass'],$data['newpass'],$data['confirm_newpass'],$id);

		$display_result= array(
			0 => "ERROR UNKNOWN", 
			1 => "Incorrect Current Password", 
			2 => "Confirm Password Does not Match", 
			9 => "Success", 
		);

		$this->session->set_flashdata('display_result',$display_result[$status]);
		redirect("user/settings/".$id);
	}

	public function confirm_recieve(){
		//models
		$this->load->model('inventory_db');

		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}

		$status = $this->inventory_db->receive_inventory($data);

		//do nothing

	}

	public function return_inventory(){
		//models
		$this->load->model('inventory_db');

		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
		    $data[$key] 	= __dbtrim($value);
		}

		$status = $this->inventory_db->return_inventory($data);

		//do nothing

	}

	


	
}
