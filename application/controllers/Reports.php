<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct(){
            parent::__construct();
        	checksession($this->session);
    } 

    public function index(){

        $this->load->model('report_db');
        $data['company']        = $this->report_db->get_company_list();
        $data['company_branch'] = $this->report_db->get_company_branch_list();
    	$this->load->view("header");
		$this->load->view("report/base_page",$data);
		$this->load->view("footer");
    }
	


	
}
