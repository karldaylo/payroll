<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {


	public function index(){
		die("404");
	}

	public function item($limit = 50,$page = 0){

		$this->load->model('inventory_db');

		$data['item'] 	= $this->inventory_db->get_inventory_list($limit,$page);
		$data['count']	= count($data['item']);

		echo json_encode($data);
	}

	public function user($limit = 50,$page = 0){

		$this->load->model('account_db');

		$data['user'] 	= $this->account_db->get_user_list($limit,$page);
		$data['count']	= count($data['user']);
		// $data['count']	= 50;

		echo json_encode($data);
	}

	public function search($filter =""){

		$this->load->model('inventory_db');

		$data 	= $this->inventory_db->search_inventory($filter);

		echo json_encode($data);
	}

	public function autocomplete($filter =""){

		$this->load->model('inventory_db');

		$data 	= $this->inventory_db->search_autocomplete($filter);

		echo json_encode($data);
	}

	public function get313(){
		$data['rate'] = rate_313();
		$data['sss'] = sss_313();
		$data['pagibig'] = pagibig_313();
		$data['philhealth'] = philhealth_313();
		$data['philhealth_adjustment'] = philhealth_adjustment_313();
		echo json_encode($data);
	}

	public function get310(){
		$data['rate'] = rate_310();
		$data['sss'] = sss_310();
		$data['pagibig'] = pagibig_310();
		$data['philhealth'] = philhealth_310();
		$data['philhealth_adjustment'] = philhealth_adjustment_310();
		echo json_encode($data);
	}

	public function get258(){
		$data['rate'] = rate_258();
		$data['sss'] = sss_258();
		$data['pagibig'] = pagibig_258();
		$data['philhealth'] = philhealth_258();
		$data['philhealth_adjustment'] = philhealth_adjustment_258();
		echo json_encode($data);
	}

	public function getemployee(){
		// if(empty($this->session->staff)){
		// 	die("{}");
		// }
		if(!empty($this->input->post('uid'))){
			
			$uid = $this->input->post('uid');
			$att_date = $this->input->post('att_date');
			$att_month = $this->input->post('att_month');
			$att_year = $this->input->post('att_year');

			$this->load->model('account_db');
			
			$result = $this->account_db->get_user_details_calc($uid);

			if(!empty($result)){

				$attendance = $this->account_db->getUserAttendance($uid,$att_month,$att_date,$att_year);
				$result['attendance'] = 0;
				if(!empty($attendance)){
					$result['attendance'] = 1;
				}

				if($att_date == 2){
					$previous_attendance = $this->account_db->getUserAttendance($uid,$att_month,1,$att_year);
					$result['previous_attendance']= false;
					if(!empty($previous_attendance)){
						$result['previous_attendance'] = $previous_attendance[0];
					}
				}
				
				if($result['employee_type'] == 313){
					$result['rate'] = rate_313();
				}elseif($result['employee_type'] == 310){
					$result['rate'] = rate_310();
				}elseif($result['employee_type'] == 258){
					$result['rate'] = rate_258();
				}
			}

			echo json_encode($result);

		}else{
			echo "{}";
		}
	}

	// public previewAttendance()

	public function testemployee(){
		
			
			$uid = 9;
			$att_date = 1;
			$att_month = 9;
			$att_year = 2020;

			$this->load->model('account_db');
			
			$result = $this->account_db->get_user_details_calc($uid);

			if(!empty($result)){

				$attendance = $this->account_db->getUserAttendance($uid,$att_month,$att_date,$att_year);
				$result['attendance'] = 0;
				if(!empty($attendance)){
					$result['attendance'] = 1;
				}

				if($att_date == 2){
					$previous_attendance = $this->account_db->getUserAttendance($uid,$att_month,1,$att_year);
					$result['previous_attendance']= false;
					if(!empty($previous_attendance)){
						$result['previous_attendance'] = $previous_attendance[0];
					}
				}

				
				if($result['employee_type'] == 313){
					$result['rate'] = rate_313();
				}elseif($result['employee_type'] == 310){
					$result['rate'] = rate_310();
				}elseif($result['employee_type'] == 258){
					$result['rate'] = rate_258();
				}
			}

			echo json_encode($result);
	}



	

}
