<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public function __construct(){
        parent::__construct();
        checksession($this->session);
    }

	public function index(){
		
		if($this->session->admin || $this->session->staff){
			$this->admin_dashboard();
		}else{
			$this->user_dashboard();
		}
	}

	private function admin_dashboard(){
		

		$this->load->view("header");
		$this->load->view("dashboard/admin");
		$this->load->view("footer");
	}

	private function user_dashboard(){
		
		//models
		$this->load->model('account_db');

		$id   = $this->session->uid;
		$data["user"]  			= $this->account_db->get_employee_details($id);
		$data["id"]   = $id;
		
		$this->load->view("header");
		$this->load->view("employee/view",$data);
		$this->load->view("footer");
	}

	

}
