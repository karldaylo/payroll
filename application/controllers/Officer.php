<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Officer extends CI_Controller {

	public function __construct(){
            parent::__construct();
        	checksession($this->session);
    } 

	public function index(){

		if($this->input->post("deleteme")){
			$this->disable_account();
		}

		$limit = 50;
		//models
		$this->load->model('account_db');

		$data["user_list"] 	= $this->account_db->get_officer_list("",$limit);
		$data["limit"] 		= $limit;
		$data['officer']    = true;
		$this->load->view("header");
		$this->load->view("employee/officer_list",$data);
		$this->load->view("footer");
	}

	public function view($id = 0){
		
		
		//models
		$this->load->model('account_db');

		$data["user"] = $this->account_db->get_user_details($id);
		$data["id"]   = $id;
		if(empty($data['user']['username'])){
			die("404");
		}

		
		$this->load->view("header");
		$this->load->view("employee/view",$data);
		$this->load->view("footer");
	}


	public function search(){
		$limit = 50;
		//models
    	$this->load->model('account_db');
		$query = "";
    	if(!empty($_GET['q'])){
    		$query = $_GET['q'];
    	}

		
		$data["user_list"] 	= $this->account_db->get_officer_list($query,$limit);
		$data["limit"] 		= $limit;
		$this->load->view("header");
		$this->load->view("employee/officer_list",$data);
		$this->load->view("footer");
		
	}

	public function disable_account(){
		//models
		$this->load->model('account_db');
		$data = __dbtrim($this->input->post('deleteme')); 
		$status = $this->account_db->disable_account($data);
		redirect("officer/");
		
	}


	


	
}
