 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attendance extends CI_Controller {

	public function __construct(){
            parent::__construct();
        	checksession($this->session);
    } 

	public function index(){

		if($this->input->post("submit")){
			$this->new_attendance();
		}
		
		$this->load->model('account_db');

		
		$data["employee_list"] = $this->account_db->get_employee_list();

		
		$this->load->view("header");
		$this->load->view("attendance/semiave",$data);
		$this->load->view("footer");
		
	}

	public function view($id){
		
		$this->load->model('attendance_db');
		$this->load->model('account_db');
		$start_date = __date($_GET['start_date'],"Y-m-d");
		$end_date = __date($_GET['end_date'],"Y-m-d");
		$data["data"] = $this->account_db->get_user_details($id);
		$data["start_date"] = $start_date;
		$data["end_date"] = $end_date;
		$data["attendance_list"] = $this->attendance_db->get_attendance_list($id,$start_date,$end_date);
		
		$this->load->view("attendance/reports",$data);
		
	}

	

	public function new_attendance(){

		//models
		$this->load->model('attendance_db');

		//post and file text cleanup
		$data = $this->input->post(); 
		foreach ($data as $key => $value){
			if(is_array($value)){
		    	foreach ($value as $key2 => $value2) {
		    		$data[$key][$key2] = __dbtrim($value2);
		    	}
		    }else{
		    	$data[$key] = __dbtrim($value);
		    }
		}
		// $data['upload'] 	=  $_FILES;
		// die(var_dump($data));


		$status = $this->attendance_db->save_attendance($data);

		if (@$status['error']==0) {
			redirect("attendance/");
		}
		else{
			echo "error";
			die(var_dump($status));
		}

	}

	


	
}
