<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
        parent::__construct();
		if(!empty($this->session->uid)){
            redirect('/', 'refresh');
        }
	}

	public function index()
	{	
		//libraries
		$this->load->library('form_validation');

		$data["error_display"] = $this->session->flashdata('error');
		if($this->input->post("login")){
		//if page submited a POST Data
			$this->validateLogin();
		}else{						
			$this->load->view('login',$data);
		}
	}

	private function randomString($size=5){
	    $c = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $l = strlen($c);
	    $r = '';
	    for ($i = 0; $i < $size; $i++) {
	        $r .= $c[rand(0, $l - 1)];
	    }
	    return $r;
	}

	public function passhash($pass, $salt=''){
		htmlentities($pass);
		if(empty($salt))
		{$salt =  $this->randomString();}
		$return = array(
			"password" => md5(sha1(md5($pass.$salt))),
			"raw" => $pass,
			"salt" => $salt,
		);
		die(var_dump($return));		
		return $return;
	}

	private function login_error($code=0){
        $return = array(
            0 => 'Unknown Error Occured. <br> <b>Note:</b> This error is impossible to obtain unless intended.',
            1 => 'Username is not yet registered',
            2 => 'Password is Incorrect',
            3 => 'Account is Disabled by Administrator. For reference please contact an Administrator',
            //9 => "Success"
        );
        return $return[$code];
    }

	private function validateLogin(){
		//model
		$this->load->model('account_db');
		
		$result= $this->account_db->login($this->input->post('username'),$this->input->post('password'));
		if($result==9){
			// die(var_dump($this->session));
			redirect('/', 'refresh');
		}
		else{
			$this->session->set_flashdata('error', $this->login_error($result));
			redirect('/login', 'refresh');
		}

	}

	public function customInput($str) 
	{
	    if ( !preg_match('/^[A-Za-z0-9_@*.!]*$/',$str) )
	    {
	        return false;
	    }
	}
}
